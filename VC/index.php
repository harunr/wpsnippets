<?php

// Visual Composer Addons Creation


if(function_exists(vc_map)){
    vc_map(array(
        'name' => 'Welcome Section',
        'base' => 'shortcode_title',
        'params' => array(
            array(
                'param_name' => 'title',
                'heading' => 'Widget Title',
                'type' => 'textfield',
                'value' => 'Default value of Parameter',                
            ),
            array(
                'param_name' => 'subtitle',
                'heading' => 'Sub Title',
                'type' => 'textfield',
                'value' => 'Default value for sub title',                
                'desctiption' => 'Help text',                
            )
        ),
    ));
    
    // For remove any existing VC element
    vc_remove_element('massage');
}