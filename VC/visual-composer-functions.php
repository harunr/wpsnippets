<?php

// Advanced Function for VC
global $team_members;
global $animate_css;
global $font_awesome_icons;
global $entypo_icons;
global $all_categories;
global $days;
global $years;
global $hours;
global $minutes;

function team_array() {
	$output = array();
	$myposts = get_posts( array( 'post_type' => 'team' ) );

	
	foreach ( $myposts as $post ) {
		$output[$post->post_title] = $post->ID; 
	}
		
	wp_reset_postdata();
	
    return $output;
}
$team_members = team_array();

function categories_array() {
	$categories = get_categories( array('hide_empty' => false) );
	foreach ($categories as $category) { 
		$output[$category->name] = $category->slug;
	}
	return $output;
}

$all_categories = categories_array();

function days_array() {
	$days_array = array();
	for ($i=1; $i < 32; $i++) { 
		$days_array[$i] = $i;
	}
	return $days_array;
}

$days = days_array();

function years_array() {
	$years_array = array();
	for ($i=2014; $i < 2100; $i++) { 
		$years_array[$i] = $i;
	}
	return $years_array;
}

$years = array();
$years = years_array();

for($i = 0;$i<24;$i++) {
	if($i<10) {
		$hour = '0'.$i;
	}
	else {
		$hour = $i;
	}
	$hours[$hour] = $hour;
}

for($i = 0;$i<60;$i++) {
	if($i<10) {
		$minute = '0'.$i;
	}
	else {
		$minute = $i;
	}
	$minutes[$minute] = $minute;
}

$animate_css = array(
	"none" => "",
	"flash" => "flash", 
	"bounce" => "bounce", 
	"shake" => "shake", 
	"tada" => "tada", 
	"swing" => "swing", 
	"wobble" => "wobble", 
	"pulse" => "pulse", 
	"flip" => "flip", 
	"flipInX" => "flipInX", 
	"flipOutX" => "flipOutX", 
	"flipInY" => "flipInY", 
	"flipOutY" => "flipOutY", 
	"fadeIn" => "fadeIn", 
	"fadeInUp" => "fadeInUp", 
	"fadeInDown" => "fadeInDown", 
	"fadeInLeft" => "fadeInLeft", 
	"fadeInRight" => "fadeInRight", 
	"fadeInUpBig" => "fadeInUpBig", 
	"fadeInDownBig" => "fadeInDownBig", 
	"fadeInLeftBig" => "fadeInLeftBig", 
	"fadeInRightBig" => "fadeInRightBig", 
	"fadeOut" => "fadeOut", 
	"fadeOutUp" => "fadeOutUp", 
	"fadeOutDown" => "fadeOutDown", 
	"fadeOutLeft" => "fadeOutLeft", 
	"fadeOutRight" => "fadeOutRight", 
	"fadeOutUpBig" => "fadeOutUpBig", 
	"fadeOutDownBig" => "fadeOutDownBig", 
	"fadeOutLeftBig" => "fadeOutLeftBig", 
	"fadeOutRightBig" => "fadeOutRightBig", 
	"slideInDown" => "slideInDown", 
	"slideInLeft" => "slideInLeft", 
	"slideInRight" => "slideInRight", 
	"slideOutUp" => "slideOutUp", 
	"slideOutLeft" => "slideOutLeft", 
	"slideOutRight" => "slideOutRight", 
	"bounceIn" => "bounceIn", 
	"bounceInDown" => "bounceInDown", 
	"bounceInUp" => "bounceInUp", 
	"bounceInLeft" => "bounceInLeft", 
	"bounceInRight" => "bounceInRight", 
	"bounceOut" => "bounceOut", 
	"bounceOutDown" => "bounceOutDown", 
	"bounceOutUp" => "bounceOutUp", 
	"bounceOutLeft" => "bounceOutLeft", 
	"bounceOutRight" => "bounceOutRight", 
	"rotateIn" => "rotateIn", 
	"rotateInDownLeft" => "rotateInDownLeft", 
	"rotateInDownRight" => "rotateInDownRight", 
	"rotateInUpLeft" => "rotateInUpLeft", 
	"rotateInUpRight" => "rotateInUpRight", 
	"rotateOut" => "rotateOut", 
	"rotateOutDownLeft" => "rotateOutDownLeft", 
	"rotateOutDownRight" => "rotateOutDownRight", 
	"rotateOutUpLeft" => "rotateOutUpLeft", 
	"rotateOutUpRight" => "rotateOutUpRight", 
	"lightSpeedIn" => "lightSpeedIn", 
	"lightSpeedOut" => "lightSpeedOut", 
	"hinge" => "hinge", 
	"rollIn" => "rollIn", 
	"rollOut" => "rollOut", 
);
$font_awesome_icons = array(
	__("None", "js_composer") => "", 
	__("fa-bars", "js_composer") => "fa-bars", 
	__("fa-flag", "js_composer") => "fa-flag", 
	__("fa-caret-down", "js_composer") => "fa-caret-down", 
	__("fa-flag", "js_composer") => "fa-flag", 
	__("fa-rub", "js_composer") => "fa-rub", 
	__("fa-camera-retro", "js_composer") => "fa-camera-retro", 
	__("fa-check-square", "js_composer") => "fa-check-square", 
	__("fa-won", "js_composer") => "fa-won", 
	__("fa-file-text-o", "js_composer") => "fa-file-text-o", 
	__("fa-hand-o-right", "js_composer") => "fa-hand-o-right", 
	__("fa-play-circle", "js_composer") => "fa-play-circle", 
	__("fa-github", "js_composer") => "fa-github", 
	__("fa-medkit", "js_composer") => "fa-medkit", 
	__("fa-caret-down", "js_composer") => "fa-caret-down", 
	__("fa-flag", "js_composer") => "fa-flag", 
	__("fa-rub", "js_composer") => "fa-rub", 
	__("fa-ruble", "js_composer") => "fa-ruble", 
	__("fa-rouble", "js_composer") => "fa-rouble", 
	__("fa-pagelines", "js_composer") => "fa-pagelines", 
	__("fa-stack-exchange", "js_composer") => "fa-stack-exchange", 
	__("fa-arrow-circle-o-right", "js_composer") => "fa-arrow-circle-o-right", 
	__("fa-arrow-circle-o-left", "js_composer") => "fa-arrow-circle-o-left", 
	__("fa-caret-square-o-left", "js_composer") => "fa-caret-square-o-left", 
	__("fa-toggle-left", "js_composer") => "fa-toggle-left", 
	__("fa-dot-circle-o", "js_composer") => "fa-dot-circle-o", 
	__("fa-wheelchair", "js_composer") => "fa-wheelchair", 
	__("fa-vimeo-square", "js_composer") => "fa-vimeo-square", 
	__("fa-try", "js_composer") => "fa-try", 
	__("fa-turkish-lira", "js_composer") => "fa-turkish-lira", 
	__("fa-plus-square-o", "js_composer") => "fa-plus-square-o", 
	__("fa-adjust", "js_composer") => "fa-adjust", 
	__("fa-anchor", "js_composer") => "fa-anchor", 
	__("fa-archive", "js_composer") => "fa-archive", 
	__("fa-arrows", "js_composer") => "fa-arrows", 
	__("fa-arrows-h", "js_composer") => "fa-arrows-h", 
	__("fa-arrows-v", "js_composer") => "fa-arrows-v", 
	__("fa-asterisk", "js_composer") => "fa-asterisk", 
	__("fa-ban", "js_composer") => "fa-ban", 
	__("fa-bar-chart-o", "js_composer") => "fa-bar-chart-o", 
	__("fa-barcode", "js_composer") => "fa-barcode", 
	__("fa-bars", "js_composer") => "fa-bars", 
	__("fa-beer", "js_composer") => "fa-beer", 
	__("fa-bell", "js_composer") => "fa-bell", 
	__("fa-bell-o", "js_composer") => "fa-bell-o", 
	__("fa-bolt", "js_composer") => "fa-bolt", 
	__("fa-book", "js_composer") => "fa-book", 
	__("fa-bookmark", "js_composer") => "fa-bookmark", 
	__("fa-bookmark-o", "js_composer") => "fa-bookmark-o", 
	__("fa-briefcase", "js_composer") => "fa-briefcase", 
	__("fa-bug", "js_composer") => "fa-bug", 
	__("fa-building-o", "js_composer") => "fa-building-o", 
	__("fa-bullhorn", "js_composer") => "fa-bullhorn", 
	__("fa-bullseye", "js_composer") => "fa-bullseye", 
	__("fa-calendar", "js_composer") => "fa-calendar", 
	__("fa-calendar-o", "js_composer") => "fa-calendar-o", 
	__("fa-camera", "js_composer") => "fa-camera", 
	__("fa-camera-retro", "js_composer") => "fa-camera-retro", 
	__("fa-caret-square-o-down", "js_composer") => "fa-caret-square-o-down", 
	__("fa-caret-square-o-left", "js_composer") => "fa-caret-square-o-left", 
	__("fa-caret-square-o-right", "js_composer") => "fa-caret-square-o-right", 
	__("fa-caret-square-o-up", "js_composer") => "fa-caret-square-o-up", 
	__("fa-certificate", "js_composer") => "fa-certificate", 
	__("fa-check", "js_composer") => "fa-check", 
	__("fa-check-circle", "js_composer") => "fa-check-circle", 
	__("fa-check-circle-o", "js_composer") => "fa-check-circle-o", 
	__("fa-check-square", "js_composer") => "fa-check-square", 
	__("fa-check-square-o", "js_composer") => "fa-check-square-o", 
	__("fa-circle", "js_composer") => "fa-circle", 
	__("fa-circle-o", "js_composer") => "fa-circle-o", 
	__("fa-clock-o", "js_composer") => "fa-clock-o", 
	__("fa-cloud", "js_composer") => "fa-cloud", 
	__("fa-cloud-download", "js_composer") => "fa-cloud-download", 
	__("fa-cloud-upload", "js_composer") => "fa-cloud-upload", 
	__("fa-code", "js_composer") => "fa-code", 
	__("fa-code-fork", "js_composer") => "fa-code-fork", 
	__("fa-coffee", "js_composer") => "fa-coffee", 
	__("fa-cog", "js_composer") => "fa-cog", 
	__("fa-cogs", "js_composer") => "fa-cogs", 
	__("fa-comment", "js_composer") => "fa-comment", 
	__("fa-comment-o", "js_composer") => "fa-comment-o", 
	__("fa-comments", "js_composer") => "fa-comments", 
	__("fa-comments-o", "js_composer") => "fa-comments-o", 
	__("fa-compass", "js_composer") => "fa-compass", 
	__("fa-credit-card", "js_composer") => "fa-credit-card", 
	__("fa-crop", "js_composer") => "fa-crop", 
	__("fa-crosshairs", "js_composer") => "fa-crosshairs", 
	__("fa-cutlery", "js_composer") => "fa-cutlery", 
	__("fa-dashboard", "js_composer") => "fa-dashboard", 
	__("fa-desktop", "js_composer") => "fa-desktop", 
	__("fa-dot-circle-o", "js_composer") => "fa-dot-circle-o", 
	__("fa-download", "js_composer") => "fa-download", 
	__("fa-edit", "js_composer") => "fa-edit", 
	__("fa-ellipsis-h", "js_composer") => "fa-ellipsis-h", 
	__("fa-ellipsis-v", "js_composer") => "fa-ellipsis-v", 
	__("fa-envelope", "js_composer") => "fa-envelope", 
	__("fa-envelope-o", "js_composer") => "fa-envelope-o", 
	__("fa-eraser", "js_composer") => "fa-eraser", 
	__("fa-exchange", "js_composer") => "fa-exchange", 
	__("fa-exclamation", "js_composer") => "fa-exclamation", 
	__("fa-exclamation-circle", "js_composer") => "fa-exclamation-circle", 
	__("fa-exclamation-triangle", "js_composer") => "fa-exclamation-triangle", 
	__("fa-external-link", "js_composer") => "fa-external-link", 
	__("fa-external-link-square", "js_composer") => "fa-external-link-square", 
	__("fa-eye", "js_composer") => "fa-eye", 
	__("fa-eye-slash", "js_composer") => "fa-eye-slash", 
	__("fa-female", "js_composer") => "fa-female", 
	__("fa-fighter-jet", "js_composer") => "fa-fighter-jet", 
	__("fa-film", "js_composer") => "fa-film", 
	__("fa-filter", "js_composer") => "fa-filter", 
	__("fa-fire", "js_composer") => "fa-fire", 
	__("fa-fire-extinguisher", "js_composer") => "fa-fire-extinguisher", 
	__("fa-flag", "js_composer") => "fa-flag", 
	__("fa-flag-checkered", "js_composer") => "fa-flag-checkered", 
	__("fa-flag-o", "js_composer") => "fa-flag-o", 
	__("fa-flash", "js_composer") => "fa-flash", 
	__("fa-flask", "js_composer") => "fa-flask", 
	__("fa-folder", "js_composer") => "fa-folder", 
	__("fa-folder-o", "js_composer") => "fa-folder-o", 
	__("fa-folder-open", "js_composer") => "fa-folder-open", 
	__("fa-folder-open-o", "js_composer") => "fa-folder-open-o", 
	__("fa-frown-o", "js_composer") => "fa-frown-o", 
	__("fa-gamepad", "js_composer") => "fa-gamepad", 
	__("fa-gavel", "js_composer") => "fa-gavel", 
	__("fa-gear", "js_composer") => "fa-gear", 
	__("fa-gears", "js_composer") => "fa-gears", 
	__("fa-gift", "js_composer") => "fa-gift", 
	__("fa-glass", "js_composer") => "fa-glass", 
	__("fa-globe", "js_composer") => "fa-globe", 
	__("fa-group", "js_composer") => "fa-group", 
	__("fa-hdd-o", "js_composer") => "fa-hdd-o", 
	__("fa-headphones", "js_composer") => "fa-headphones", 
	__("fa-heart", "js_composer") => "fa-heart", 
	__("fa-heart-o", "js_composer") => "fa-heart-o", 
	__("fa-home", "js_composer") => "fa-home", 
	__("fa-inbox", "js_composer") => "fa-inbox", 
	__("fa-info", "js_composer") => "fa-info", 
	__("fa-info-circle", "js_composer") => "fa-info-circle", 
	__("fa-key", "js_composer") => "fa-key", 
	__("fa-keyboard-o", "js_composer") => "fa-keyboard-o", 
	__("fa-laptop", "js_composer") => "fa-laptop", 
	__("fa-leaf", "js_composer") => "fa-leaf", 
	__("fa-legal", "js_composer") => "fa-legal", 
	__("fa-lemon-o", "js_composer") => "fa-lemon-o", 
	__("fa-level-down", "js_composer") => "fa-level-down", 
	__("fa-level-up", "js_composer") => "fa-level-up", 
	__("fa-lightbulb-o", "js_composer") => "fa-lightbulb-o", 
	__("fa-location-arrow", "js_composer") => "fa-location-arrow", 
	__("fa-lock", "js_composer") => "fa-lock", 
	__("fa-magic", "js_composer") => "fa-magic", 
	__("fa-magnet", "js_composer") => "fa-magnet", 
	__("fa-mail-forward", "js_composer") => "fa-mail-forward", 
	__("fa-mail-reply", "js_composer") => "fa-mail-reply", 
	__("fa-mail-reply-all", "js_composer") => "fa-mail-reply-all", 
	__("fa-male", "js_composer") => "fa-male", 
	__("fa-map-marker", "js_composer") => "fa-map-marker", 
	__("fa-meh-o", "js_composer") => "fa-meh-o", 
	__("fa-microphone", "js_composer") => "fa-microphone", 
	__("fa-microphone-slash", "js_composer") => "fa-microphone-slash", 
	__("fa-minus", "js_composer") => "fa-minus", 
	__("fa-minus-circle", "js_composer") => "fa-minus-circle", 
	__("fa-minus-square", "js_composer") => "fa-minus-square", 
	__("fa-minus-square-o", "js_composer") => "fa-minus-square-o", 
	__("fa-mobile", "js_composer") => "fa-mobile", 
	__("fa-mobile-phone", "js_composer") => "fa-mobile-phone", 
	__("fa-money", "js_composer") => "fa-money", 
	__("fa-moon-o", "js_composer") => "fa-moon-o", 
	__("fa-music", "js_composer") => "fa-music", 
	__("fa-pencil", "js_composer") => "fa-pencil", 
	__("fa-pencil-square", "js_composer") => "fa-pencil-square", 
	__("fa-pencil-square-o", "js_composer") => "fa-pencil-square-o", 
	__("fa-phone", "js_composer") => "fa-phone", 
	__("fa-phone-square", "js_composer") => "fa-phone-square", 
	__("fa-picture-o", "js_composer") => "fa-picture-o", 
	__("fa-plane", "js_composer") => "fa-plane", 
	__("fa-plus", "js_composer") => "fa-plus", 
	__("fa-plus-circle", "js_composer") => "fa-plus-circle", 
	__("fa-plus-square", "js_composer") => "fa-plus-square", 
	__("fa-plus-square-o", "js_composer") => "fa-plus-square-o", 
	__("fa-power-off", "js_composer") => "fa-power-off", 
	__("fa-print", "js_composer") => "fa-print", 
	__("fa-puzzle-piece", "js_composer") => "fa-puzzle-piece", 
	__("fa-qrcode", "js_composer") => "fa-qrcode", 
	__("fa-question", "js_composer") => "fa-question", 
	__("fa-question-circle", "js_composer") => "fa-question-circle", 
	__("fa-quote-left", "js_composer") => "fa-quote-left", 
	__("fa-quote-right", "js_composer") => "fa-quote-right", 
	__("fa-random", "js_composer") => "fa-random", 
	__("fa-refresh", "js_composer") => "fa-refresh", 
	__("fa-reply", "js_composer") => "fa-reply", 
	__("fa-reply-all", "js_composer") => "fa-reply-all", 
	__("fa-retweet", "js_composer") => "fa-retweet", 
	__("fa-road", "js_composer") => "fa-road", 
	__("fa-rocket", "js_composer") => "fa-rocket", 
	__("fa-rss", "js_composer") => "fa-rss", 
	__("fa-rss-square", "js_composer") => "fa-rss-square", 
	__("fa-search", "js_composer") => "fa-search", 
	__("fa-search-minus", "js_composer") => "fa-search-minus", 
	__("fa-share", "js_composer") => "fa-share", 
	__("fa-share-square", "js_composer") => "fa-share-square", 
	__("fa-share-square-o", "js_composer") => "fa-share-square-o", 
	__("fa-shield", "js_composer") => "fa-shield", 
	__("fa-shopping-cart", "js_composer") => "fa-shopping-cart", 
	__("fa-sign-in", "js_composer") => "fa-sign-in", 
	__("fa-sign-out", "js_composer") => "fa-sign-out", 
	__("fa-signal", "js_composer") => "fa-signal", 
	__("fa-sitemap", "js_composer") => "fa-sitemap", 
	__("fa-smile-o", "js_composer") => "fa-smile-o", 
	__("fa-sort", "js_composer") => "fa-sort", 
	__("fa-sort-alpha-asc", "js_composer") => "fa-sort-alpha-asc", 
	__("fa-sort-alpha-desc", "js_composer") => "fa-sort-alpha-desc", 
	__("fa-sort-amount-asc", "js_composer") => "fa-sort-amount-asc", 
	__("fa-sort-amount-desc", "js_composer") => "fa-sort-amount-desc", 
	__("fa-sort-asc", "js_composer") => "fa-sort-asc", 
	__("fa-sort-desc", "js_composer") => "fa-sort-desc", 
	__("fa-sort-down", "js_composer") => "fa-sort-down", 
	__("fa-sort-numeric-asc", "js_composer") => "fa-sort-numeric-asc", 
	__("fa-sort-numeric-desc", "js_composer") => "fa-sort-numeric-desc", 
	__("fa-sort-up", "js_composer") => "fa-sort-up", 
	__("fa-spinner", "js_composer") => "fa-spinner", 
	__("fa-square", "js_composer") => "fa-square", 
	__("fa-square-o", "js_composer") => "fa-square-o", 
	__("fa-star", "js_composer") => "fa-star", 
	__("fa-star-half", "js_composer") => "fa-star-half", 
	__("fa-star-half-empty", "js_composer") => "fa-star-half-empty", 
	__("fa-star-half-full", "js_composer") => "fa-star-half-full", 
	__("fa-star-half-o", "js_composer") => "fa-star-half-o", 
	__("fa-star-o", "js_composer") => "fa-star-o", 
	__("fa-subscript", "js_composer") => "fa-subscript", 
	__("fa-suitcase", "js_composer") => "fa-suitcase", 
	__("fa-sun-o", "js_composer") => "fa-sun-o", 
	__("fa-superscript", "js_composer") => "fa-superscript", 
	__("fa-tablet", "js_composer") => "fa-tablet", 
	__("fa-tachometer", "js_composer") => "fa-tachometer", 
	__("fa-tag", "js_composer") => "fa-tag", 
	__("fa-tags", "js_composer") => "fa-tags", 
	__("fa-tasks", "js_composer") => "fa-tasks", 
	__("fa-terminal", "js_composer") => "fa-terminal", 
	__("fa-thumb-tack", "js_composer") => "fa-thumb-tack", 
	__("fa-thumbs-down", "js_composer") => "fa-thumbs-down", 
	__("fa-thumbs-o-down", "js_composer") => "fa-thumbs-o-down", 
	__("fa-thumbs-o-up", "js_composer") => "fa-thumbs-o-up", 
	__("fa-thumbs-up", "js_composer") => "fa-thumbs-up", 
	__("fa-ticket", "js_composer") => "fa-ticket", 
	__("fa-times", "js_composer") => "fa-times", 
	__("fa-times-circle", "js_composer") => "fa-times-circle", 
	__("fa-times-circle-o", "js_composer") => "fa-times-circle-o", 
	__("fa-tint", "js_composer") => "fa-tint", 
	__("fa-toggle-down", "js_composer") => "fa-toggle-down", 
	__("fa-toggle-left", "js_composer") => "fa-toggle-left", 
	__("fa-toggle-right", "js_composer") => "fa-toggle-right", 
	__("fa-toggle-up", "js_composer") => "fa-toggle-up", 
	__("fa-trash-o", "js_composer") => "fa-trash-o", 
	__("fa-trophy", "js_composer") => "fa-trophy", 
	__("fa-truck", "js_composer") => "fa-truck", 
	__("fa-umbrella", "js_composer") => "fa-umbrella", 
	__("fa-unlock", "js_composer") => "fa-unlock", 
	__("fa-unlock-alt", "js_composer") => "fa-unlock-alt", 
	__("fa-unsorted", "js_composer") => "fa-unsorted", 
	__("fa-upload", "js_composer") => "fa-upload", 
	__("fa-user", "js_composer") => "fa-user", 
	__("fa-users", "js_composer") => "fa-users", 
	__("fa-video-camera", "js_composer") => "fa-video-camera", 
	__("fa-volume-down", "js_composer") => "fa-volume-down", 
	__("fa-volume-off", "js_composer") => "fa-volume-off", 
	__("fa-volume-up", "js_composer") => "fa-volume-up", 
	__("fa-warning", "js_composer") => "fa-warning", 
	__("fa-wheelchair", "js_composer") => "fa-wheelchair", 
	__("fa-wrench", "js_composer") => "fa-wrench", 
	__("fa-check-square", "js_composer") => "fa-check-square", 
	__("fa-check-square-o", "js_composer") => "fa-check-square-o", 
	__("fa-circle", "js_composer") => "fa-circle", 
	__("fa-circle-o", "js_composer") => "fa-circle-o", 
	__("fa-dot-circle-o", "js_composer") => "fa-dot-circle-o", 
	__("fa-minus-square", "js_composer") => "fa-minus-square", 
	__("fa-minus-square-o", "js_composer") => "fa-minus-square-o", 
	__("fa-plus-square", "js_composer") => "fa-plus-square", 
	__("fa-plus-square-o", "js_composer") => "fa-plus-square-o", 
	__("fa-square", "js_composer") => "fa-square", 
	__("fa-square-o", "js_composer") => "fa-square-o", 
	__("fa-bitcoin", "js_composer") => "fa-bitcoin", 
	__("fa-btc", "js_composer") => "fa-btc", 
	__("fa-cny", "js_composer") => "fa-cny", 
	__("fa-dollar", "js_composer") => "fa-dollar", 
	__("fa-eur", "js_composer") => "fa-eur", 
	__("fa-euro", "js_composer") => "fa-euro", 
	__("fa-gbp", "js_composer") => "fa-gbp", 
	__("fa-inr", "js_composer") => "fa-inr", 
	__("fa-jpy", "js_composer") => "fa-jpy", 
	__("fa-krw", "js_composer") => "fa-krw", 
	__("fa-money", "js_composer") => "fa-money", 
	__("fa-rmb", "js_composer") => "fa-rmb", 
	__("fa-rouble", "js_composer") => "fa-rouble", 
	__("fa-rub", "js_composer") => "fa-rub", 
	__("fa-ruble", "js_composer") => "fa-ruble", 
	__("fa-rupee", "js_composer") => "fa-rupee", 
	__("fa-try", "js_composer") => "fa-try", 
	__("fa-turkish-lira", "js_composer") => "fa-turkish-lira", 
	__("fa-usd", "js_composer") => "fa-usd", 
	__("fa-won", "js_composer") => "fa-won", 
	__("fa-yen", "js_composer") => "fa-yen", 
	__("fa-align-center", "js_composer") => "fa-align-center", 
	__("fa-align-justify", "js_composer") => "fa-align-justify", 
	__("fa-align-left", "js_composer") => "fa-align-left", 
	__("fa-align-right", "js_composer") => "fa-align-right", 
	__("fa-bold", "js_composer") => "fa-bold", 
	__("fa-chain", "js_composer") => "fa-chain", 
	__("fa-chain-broken", "js_composer") => "fa-chain-broken", 
	__("fa-clipboard", "js_composer") => "fa-clipboard", 
	__("fa-columns", "js_composer") => "fa-columns", 
	__("fa-copy", "js_composer") => "fa-copy", 
	__("fa-cut", "js_composer") => "fa-cut", 
	__("fa-dedent", "js_composer") => "fa-dedent", 
	__("fa-eraser", "js_composer") => "fa-eraser", 
	__("fa-file", "js_composer") => "fa-file", 
	__("fa-file-o", "js_composer") => "fa-file-o", 
	__("fa-file-text", "js_composer") => "fa-file-text", 
	__("fa-file-text-o", "js_composer") => "fa-file-text-o", 
	__("fa-files-o", "js_composer") => "fa-files-o", 
	__("fa-floppy-o", "js_composer") => "fa-floppy-o", 
	__("fa-font", "js_composer") => "fa-font", 
	__("fa-indent", "js_composer") => "fa-indent", 
	__("fa-italic", "js_composer") => "fa-italic", 
	__("fa-link", "js_composer") => "fa-link", 
	__("fa-list", "js_composer") => "fa-list", 
	__("fa-list-alt", "js_composer") => "fa-list-alt", 
	__("fa-list-ol", "js_composer") => "fa-list-ol", 
	__("fa-list-ul", "js_composer") => "fa-list-ul", 
	__("fa-outdent", "js_composer") => "fa-outdent", 
	__("fa-paperclip", "js_composer") => "fa-paperclip", 
	__("fa-paste", "js_composer") => "fa-paste", 
	__("fa-repeat", "js_composer") => "fa-repeat", 
	__("fa-rotate-left", "js_composer") => "fa-rotate-left", 
	__("fa-rotate-right", "js_composer") => "fa-rotate-right", 
	__("fa-save", "js_composer") => "fa-save", 
	__("fa-scissors", "js_composer") => "fa-scissors", 
	__("fa-strikethrough", "js_composer") => "fa-strikethrough", 
	__("fa-table", "js_composer") => "fa-table", 
	__("fa-text-height", "js_composer") => "fa-text-height", 
	__("fa-text-width", "js_composer") => "fa-text-width", 
	__("fa-th", "js_composer") => "fa-th", 
	__("fa-th-large", "js_composer") => "fa-th-large", 
	__("fa-th-list", "js_composer") => "fa-th-list", 
	__("fa-underline", "js_composer") => "fa-underline", 
	__("fa-undo", "js_composer") => "fa-undo", 
	__("fa-unlink", "js_composer") => "fa-unlink", 
	__("fa-angle-double-down", "js_composer") => "fa-angle-double-down", 
	__("fa-angle-double-left", "js_composer") => "fa-angle-double-left", 
	__("fa-angle-double-right", "js_composer") => "fa-angle-double-right", 
	__("fa-angle-double-up", "js_composer") => "fa-angle-double-up", 
	__("fa-angle-down", "js_composer") => "fa-angle-down", 
	__("fa-angle-left", "js_composer") => "fa-angle-left", 
	__("fa-angle-right", "js_composer") => "fa-angle-right", 
	__("fa-angle-up", "js_composer") => "fa-angle-up", 
	__("fa-arrow-circle-down", "js_composer") => "fa-arrow-circle-down", 
	__("fa-arrow-circle-left", "js_composer") => "fa-arrow-circle-left", 
	__("fa-arrow-circle-o-down", "js_composer") => "fa-arrow-circle-o-down", 
	__("fa-arrow-circle-o-left", "js_composer") => "fa-arrow-circle-o-left", 
	__("fa-arrow-circle-o-right", "js_composer") => "fa-arrow-circle-o-right", 
	__("fa-arrow-circle-o-up", "js_composer") => "fa-arrow-circle-o-up", 
	__("fa-arrow-circle-right", "js_composer") => "fa-arrow-circle-right", 
	__("fa-arrow-circle-up", "js_composer") => "fa-arrow-circle-up", 
	__("fa-arrow-down", "js_composer") => "fa-arrow-down", 
	__("fa-arrow-left", "js_composer") => "fa-arrow-left", 
	__("fa-arrow-right", "js_composer") => "fa-arrow-right", 
	__("fa-arrow-up", "js_composer") => "fa-arrow-up", 
	__("fa-arrows", "js_composer") => "fa-arrows", 
	__("fa-arrows-alt", "js_composer") => "fa-arrows-alt", 
	__("fa-arrows-h", "js_composer") => "fa-arrows-h", 
	__("fa-arrows-v", "js_composer") => "fa-arrows-v", 
	__("fa-caret-down", "js_composer") => "fa-caret-down", 
	__("fa-caret-left", "js_composer") => "fa-caret-left", 
	__("fa-caret-right", "js_composer") => "fa-caret-right", 
	__("fa-caret-square-o-down", "js_composer") => "fa-caret-square-o-down", 
	__("fa-caret-square-o-left", "js_composer") => "fa-caret-square-o-left", 
	__("fa-caret-square-o-right", "js_composer") => "fa-caret-square-o-right", 
	__("fa-caret-square-o-up", "js_composer") => "fa-caret-square-o-up", 
	__("fa-caret-up", "js_composer") => "fa-caret-up", 
	__("fa-chevron-circle-down", "js_composer") => "fa-chevron-circle-down", 
	__("fa-chevron-circle-left", "js_composer") => "fa-chevron-circle-left", 
	__("fa-chevron-circle-right", "js_composer") => "fa-chevron-circle-right", 
	__("fa-chevron-circle-up", "js_composer") => "fa-chevron-circle-up", 
	__("fa-chevron-down", "js_composer") => "fa-chevron-down", 
	__("fa-chevron-left", "js_composer") => "fa-chevron-left", 
	__("fa-chevron-right", "js_composer") => "fa-chevron-right", 
	__("fa-chevron-up", "js_composer") => "fa-chevron-up", 
	__("fa-hand-o-down", "js_composer") => "fa-hand-o-down", 
	__("fa-hand-o-left", "js_composer") => "fa-hand-o-left", 
	__("fa-hand-o-right", "js_composer") => "fa-hand-o-right", 
	__("fa-hand-o-up", "js_composer") => "fa-hand-o-up", 
	__("fa-long-arrow-down", "js_composer") => "fa-long-arrow-down", 
	__("fa-long-arrow-left", "js_composer") => "fa-long-arrow-left", 
	__("fa-long-arrow-right", "js_composer") => "fa-long-arrow-right", 
	__("fa-long-arrow-up", "js_composer") => "fa-long-arrow-up", 
	__("fa-toggle-down", "js_composer") => "fa-toggle-down", 
	__("fa-toggle-left", "js_composer") => "fa-toggle-left", 
	__("fa-toggle-right", "js_composer") => "fa-toggle-right", 
	__("fa-toggle-up", "js_composer") => "fa-toggle-up", 
	__("fa-arrows-alt", "js_composer") => "fa-arrows-alt", 
	__("fa-backward", "js_composer") => "fa-backward", 
	__("fa-compress", "js_composer") => "fa-compress", 
	__("fa-eject", "js_composer") => "fa-eject", 
	__("fa-expand", "js_composer") => "fa-expand", 
	__("fa-fast-backward", "js_composer") => "fa-fast-backward", 
	__("fa-fast-forward", "js_composer") => "fa-fast-forward", 
	__("fa-forward", "js_composer") => "fa-forward", 
	__("fa-pause", "js_composer") => "fa-pause", 
	__("fa-play", "js_composer") => "fa-play", 
	__("fa-play-circle", "js_composer") => "fa-play-circle", 
	__("fa-play-circle-o", "js_composer") => "fa-play-circle-o", 
	__("fa-step-backward", "js_composer") => "fa-step-backward", 
	__("fa-step-forward", "js_composer") => "fa-step-forward", 
	__("fa-stop", "js_composer") => "fa-stop", 
	__("fa-youtube-play", "js_composer") => "fa-youtube-play", 
	__("fa-adn", "js_composer") => "fa-adn", 
	__("fa-android", "js_composer") => "fa-android", 
	__("fa-apple", "js_composer") => "fa-apple", 
	__("fa-bitbucket", "js_composer") => "fa-bitbucket", 
	__("fa-bitbucket-square", "js_composer") => "fa-bitbucket-square", 
	__("fa-bitcoin", "js_composer") => "fa-bitcoin", 
	__("fa-btc", "js_composer") => "fa-btc", 
	__("fa-css3", "js_composer") => "fa-css3", 
	__("fa-dribbble", "js_composer") => "fa-dribbble", 
	__("fa-dropbox", "js_composer") => "fa-dropbox", 
	__("fa-facebook", "js_composer") => "fa-facebook", 
	__("fa-facebook-square", "js_composer") => "fa-facebook-square", 
	__("fa-flickr", "js_composer") => "fa-flickr", 
	__("fa-foursquare", "js_composer") => "fa-foursquare", 
	__("fa-github", "js_composer") => "fa-github", 
	__("fa-github-alt", "js_composer") => "fa-github-alt", 
	__("fa-github-square", "js_composer") => "fa-github-square", 
	__("fa-gittip", "js_composer") => "fa-gittip", 
	__("fa-google-plus", "js_composer") => "fa-google-plus", 
	__("fa-google-plus-square", "js_composer") => "fa-google-plus-square", 
	__("fa-html5", "js_composer") => "fa-html5", 
	__("fa-instagram", "js_composer") => "fa-instagram", 
	__("fa-linkedin", "js_composer") => "fa-linkedin", 
	__("fa-linkedin-square", "js_composer") => "fa-linkedin-square", 
	__("fa-linux", "js_composer") => "fa-linux", 
	__("fa-maxcdn", "js_composer") => "fa-maxcdn", 
	__("fa-pagelines", "js_composer") => "fa-pagelines", 
	__("fa-pinterest", "js_composer") => "fa-pinterest", 
	__("fa-pinterest-square", "js_composer") => "fa-pinterest-square", 
	__("fa-renren", "js_composer") => "fa-renren", 
	__("fa-skype", "js_composer") => "fa-skype", 
	__("fa-stack-exchange", "js_composer") => "fa-stack-exchange", 
	__("fa-stack-overflow", "js_composer") => "fa-stack-overflow", 
	__("fa-trello", "js_composer") => "fa-trello", 
	__("fa-tumblr", "js_composer") => "fa-tumblr", 
	__("fa-tumblr-square", "js_composer") => "fa-tumblr-square", 
	__("fa-twitter", "js_composer") => "fa-twitter", 
	__("fa-twitter-square", "js_composer") => "fa-twitter-square", 
	__("fa-vimeo-square", "js_composer") => "fa-vimeo-square", 
	__("fa-vk", "js_composer") => "fa-vk", 
	__("fa-weibo", "js_composer") => "fa-weibo", 
	__("fa-windows", "js_composer") => "fa-windows", 
	__("fa-xing", "js_composer") => "fa-xing", 
	__("fa-xing-square", "js_composer") => "fa-xing-square", 
	__("fa-youtube", "js_composer") => "fa-youtube", 
	__("fa-youtube-play", "js_composer") => "fa-youtube-play", 
	__("fa-youtube-square", "js_composer") => "fa-youtube-square", 
	__("fa-ambulance", "js_composer") => "fa-ambulance", 
	__("fa-h-square", "js_composer") => "fa-h-square", 
	__("fa-hospital-o", "js_composer") => "fa-hospital-o", 
	__("fa-medkit", "js_composer") => "fa-medkit", 
	__("fa-plus-square", "js_composer") => "fa-plus-square", 
	__("fa-stethoscope", "js_composer") => "fa-stethoscope", 
	__("fa-user-md", "js_composer") => "fa-user-md", 
	__("fa-wheelchair", "js_composer") => "fa-wheelchair", 
	__("fa-flag", "js_composer") => "fa-flag", 
	__("fa-maxcdn", "js_composer") => "fa-maxcdn", 
);

$entypo_icons = array(
	__("phone", "js_composer") => "phone", 
	__("mobile", "js_composer") => "mobile", 
	__("mouse", "js_composer") => "mouse", 
	__("address", "js_composer") => "address", 
	__("mail", "js_composer") => "mail", 
	__("paper-plane", "js_composer") => "paper-plane", 
	__("pencil", "js_composer") => "pencil", 
	__("feather", "js_composer") => "feather", 
	__("attach", "js_composer") => "attach", 
	__("inbox", "js_composer") => "inbox", 
	__("reply", "js_composer") => "reply", 
	__("reply-all", "js_composer") => "reply-all", 
	__("forward", "js_composer") => "forward", 
	__("user", "js_composer") => "user", 
	__("users", "js_composer") => "users", 
	__("add-user", "js_composer") => "add-user", 
	__("vcard", "js_composer") => "vcard", 
	__("export", "js_composer") => "export", 
	__("location", "js_composer") => "location", 
	__("map", "js_composer") => "map", 
	__("compass", "js_composer") => "compass", 
	__("direction", "js_composer") => "direction", 
	__("hair-cross", "js_composer") => "hair-cross", 
	__("share", "js_composer") => "share", 
	__("shareable", "js_composer") => "shareable", 
	__("heart", "js_composer") => "heart", 
	__("heart-empty", "js_composer") => "heart-empty", 
	__("star", "js_composer") => "star", 
	__("star-empty", "js_composer") => "star-empty", 
	__("thumbs-up", "js_composer") => "thumbs-up", 
	__("thumbs-down", "js_composer") => "thumbs-down", 
	__("chat", "js_composer") => "chat", 
	__("ecomment", "js_composer") => "ecomment", 
	__("quote", "js_composer") => "quote", 
	__("home", "js_composer") => "home", 
	__("popup", "js_composer") => "popup", 
	__("search", "js_composer") => "search", 
	__("flashlight", "js_composer") => "flashlight", 
	__("print", "js_composer") => "print", 
	__("bell", "js_composer") => "bell", 
	__("link", "js_composer") => "link", 
	__("flag", "js_composer") => "flag", 
	__("cog", "js_composer") => "cog", 
	__("tools", "js_composer") => "tools", 
	__("trophy", "js_composer") => "trophy", 
	__("etag", "js_composer") => "etag", 
	__("camera", "js_composer") => "camera", 
	__("megaphone", "js_composer") => "megaphone", 
	__("moon", "js_composer") => "moon", 
	__("palette", "js_composer") => "palette", 
	__("leaf", "js_composer") => "leaf", 
	__("note", "js_composer") => "note", 
	__("beamed-note", "js_composer") => "beamed-note", 
	__("new", "js_composer") => "new", 
	__("graduation-cap", "js_composer") => "graduation-cap", 
	__("book", "js_composer") => "book", 
	__("newspaper", "js_composer") => "newspaper", 
	__("bag", "js_composer") => "bag", 
	__("airplane", "js_composer") => "airplane", 
	__("lifebuoy", "js_composer") => "lifebuoy", 
	__("eye", "js_composer") => "eye", 
	__("clock", "js_composer") => "clock", 
	__("mic", "js_composer") => "mic", 
	__("calendar", "js_composer") => "calendar", 
	__("flash", "js_composer") => "flash", 
	__("thunder-cloud", "js_composer") => "thunder-cloud", 
	__("droplet", "js_composer") => "droplet", 
	__("cd", "js_composer") => "cd", 
	__("briefcase", "js_composer") => "briefcase", 
	__("air", "js_composer") => "air", 
	__("hourglass", "js_composer") => "hourglass", 
	__("gauge", "js_composer") => "gauge", 
	__("language", "js_composer") => "language", 
	__("network", "js_composer") => "network", 
	__("key", "js_composer") => "key", 
	__("battery", "js_composer") => "battery", 
	__("bucket", "js_composer") => "bucket", 
	__("magnet", "js_composer") => "magnet", 
	__("drive", "js_composer") => "drive", 
	__("cup", "js_composer") => "cup", 
	__("rocket", "js_composer") => "rocket", 
	__("brush", "js_composer") => "brush", 
	__("suitcase", "js_composer") => "suitcase", 
	__("traffic-cone", "js_composer") => "traffic-cone", 
	__("globe", "js_composer") => "globe", 
	__("keyboard", "js_composer") => "keyboard", 
	__("browser", "js_composer") => "browser", 
	__("publish", "js_composer") => "publish", 
	__("progress-3", "js_composer") => "progress-3", 
	__("progress-2", "js_composer") => "progress-2", 
	__("progress-1", "js_composer") => "progress-1", 
	__("progress-0", "js_composer") => "progress-0", 
	__("light-down", "js_composer") => "light-down", 
	__("light-up", "js_composer") => "light-up", 
	__("adjust", "js_composer") => "adjust", 
	__("code", "js_composer") => "code", 
	__("monitor", "js_composer") => "monitor", 
	__("infinity", "js_composer") => "infinity", 
	__("light-bulb", "js_composer") => "light-bulb", 
	__("credit-card", "js_composer") => "credit-card", 
	__("database", "js_composer") => "database", 
	__("voicemail", "js_composer") => "voicemail", 
	__("clipboard", "js_composer") => "clipboard", 
	__("cart", "js_composer") => "cart", 
	__("box", "js_composer") => "box", 
	__("ticket", "js_composer") => "ticket", 
	__("rss", "js_composer") => "rss", 
	__("signal", "js_composer") => "signal", 
	__("thermometer", "js_composer") => "thermometer", 
	__("water", "js_composer") => "water", 
	__("sweden", "js_composer") => "sweden", 
	__("line-graph", "js_composer") => "line-graph", 
	__("pie-chart", "js_composer") => "pie-chart", 
	__("bar-graph", "js_composer") => "bar-graph", 
	__("area-graph", "js_composer") => "area-graph", 
	__("lock", "js_composer") => "lock", 
	__("lock-open", "js_composer") => "lock-open", 
	__("logout", "js_composer") => "logout", 
	__("login", "js_composer") => "login", 
	__("check", "js_composer") => "check", 
	__("cross", "js_composer") => "cross", 
	__("squared-minus", "js_composer") => "squared-minus", 
	__("squared-plus", "js_composer") => "squared-plus", 
	__("squared-cross", "js_composer") => "squared-cross", 
	__("circled-minus", "js_composer") => "circled-minus", 
	__("circled-plus", "js_composer") => "circled-plus", 
	__("circled-cross", "js_composer") => "circled-cross", 
	__("minus", "js_composer") => "minus", 
	__("plus", "js_composer") => "plus", 
	__("erase", "js_composer") => "erase", 
	__("block", "js_composer") => "block", 
	__("info", "js_composer") => "info", 
	__("circled-info", "js_composer") => "circled-info", 
	__("help", "js_composer") => "help", 
	__("circled-help", "js_composer") => "circled-help", 
	__("warning", "js_composer") => "warning", 
	__("cycle", "js_composer") => "cycle", 
	__("cw", "js_composer") => "cw", 
	__("ccw", "js_composer") => "ccw", 
	__("shuffle", "js_composer") => "shuffle", 
	__("back", "js_composer") => "back", 
	__("level-down", "js_composer") => "level-down", 
	__("retweet", "js_composer") => "retweet", 
	__("loop", "js_composer") => "loop", 
	__("back-in-time", "js_composer") => "back-in-time", 
	__("level-up", "js_composer") => "level-up", 
	__("switch", "js_composer") => "switch", 
	__("numbered-list", "js_composer") => "numbered-list", 
	__("add-to-list", "js_composer") => "add-to-list", 
	__("layout", "js_composer") => "layout", 
	__("list", "js_composer") => "list", 
	__("text-doc", "js_composer") => "text-doc", 
	__("text-doc-inverted", "js_composer") => "text-doc-inverted", 
	__("doc", "js_composer") => "doc", 
	__("docs", "js_composer") => "docs", 
	__("landscape-doc", "js_composer") => "landscape-doc", 
	__("picture", "js_composer") => "picture", 
	__("video", "js_composer") => "video", 
	__("music", "js_composer") => "music", 
	__("folder", "js_composer") => "folder", 
	__("archive", "js_composer") => "archive", 
	__("trash", "js_composer") => "trash", 
	__("upload", "js_composer") => "upload", 
	__("download", "js_composer") => "download", 
	__("save", "js_composer") => "save", 
	__("install", "js_composer") => "install", 
	__("cloud", "js_composer") => "cloud", 
	__("upload-cloud", "js_composer") => "upload-cloud", 
	__("bookmark", "js_composer") => "bookmark", 
	__("bookmarks", "js_composer") => "bookmarks", 
	__("open-book", "js_composer") => "open-book", 
	__("play", "js_composer") => "play", 
	__("paus", "js_composer") => "paus", 
	__("record", "js_composer") => "record", 
	__("stop", "js_composer") => "stop", 
	__("ff", "js_composer") => "ff", 
	__("fb", "js_composer") => "fb", 
	__("to-start", "js_composer") => "to-start", 
	__("to-end", "js_composer") => "to-end", 
	__("resize-full", "js_composer") => "resize-full", 
	__("resize-small", "js_composer") => "resize-small", 
	__("volume", "js_composer") => "volume", 
	__("sound", "js_composer") => "sound", 
	__("mute", "js_composer") => "mute", 
	__("flow-cascade", "js_composer") => "flow-cascade", 
	__("flow-branch", "js_composer") => "flow-branch", 
	__("flow-tree", "js_composer") => "flow-tree", 
	__("flow-line", "js_composer") => "flow-line", 
	__("flow-parallel", "js_composer") => "flow-parallel", 
	__("left-bold", "js_composer") => "left-bold", 
	__("down-bold", "js_composer") => "down-bold", 
	__("up-bold", "js_composer") => "up-bold", 
	__("right-bold", "js_composer") => "right-bold", 
	__("left", "js_composer") => "left", 
	__("down", "js_composer") => "down", 
	__("up", "js_composer") => "up", 
	__("right", "js_composer") => "right", 
	__("circled-left", "js_composer") => "circled-left", 
	__("circled-down", "js_composer") => "circled-down", 
	__("circled-up", "js_composer") => "circled-up", 
	__("circled-right", "js_composer") => "circled-right", 
	__("triangle-left", "js_composer") => "triangle-left", 
	__("triangle-down", "js_composer") => "triangle-down", 
	__("triangle-up", "js_composer") => "triangle-up", 
	__("triangle-right", "js_composer") => "triangle-right", 
	__("chevron-left", "js_composer") => "chevron-left", 
	__("chevron-down", "js_composer") => "chevron-down", 
	__("chevron-up", "js_composer") => "chevron-up", 
	__("chevron-right", "js_composer") => "chevron-right", 
	__("chevron-small-left", "js_composer") => "chevron-small-left", 
	__("chevron-small-down", "js_composer") => "chevron-small-down", 
	__("chevron-small-up", "js_composer") => "chevron-small-up", 
	__("chevron-small-right", "js_composer") => "chevron-small-right", 
	__("chevron-thin-left", "js_composer") => "chevron-thin-left", 
	__("chevron-thin-down", "js_composer") => "chevron-thin-down", 
	__("chevron-thin-up", "js_composer") => "chevron-thin-up", 
	__("chevron-thin-right", "js_composer") => "chevron-thin-right", 
	__("left-thin", "js_composer") => "left-thin", 
	__("down-thin", "js_composer") => "down-thin", 
	__("up-thin", "js_composer") => "up-thin", 
	__("right-thin", "js_composer") => "right-thin", 
	__("arrow-combo", "js_composer") => "arrow-combo", 
	__("three-dots", "js_composer") => "three-dots", 
	__("two-dots", "js_composer") => "two-dots", 
	__("dot", "js_composer") => "dot", 
	__("cc", "js_composer") => "cc", 
	__("cc-by", "js_composer") => "cc-by", 
	__("cc-nc", "js_composer") => "cc-nc", 
	__("cc-nc-eu", "js_composer") => "cc-nc-eu", 
	__("cc-nc-jp", "js_composer") => "cc-nc-jp", 
	__("cc-sa", "js_composer") => "cc-sa", 
	__("cc-nd", "js_composer") => "cc-nd", 
	__("cc-pd", "js_composer") => "cc-pd", 
	__("cc-zero", "js_composer") => "cc-zero", 
	__("cc-share", "js_composer") => "cc-share", 
	__("cc-remix", "js_composer") => "cc-remix", 
	__("db-logo", "js_composer") => "db-logo", 
	__("db-shape", "js_composer") => "db-shape"
);

add_filter('wpb_widget_title', 'override_widget_title', 10, 2);
function override_widget_title($output = '', $params = array('')) {
  $extraclass = (isset($params['extraclass'])) ? " ".$params['extraclass'] : "";
  return '<h4 class="entry-title'.$extraclass.'">'.$params['title'].'</h4>';
}



if ( function_exists( 'vc_map' ) ) :

	
	
	
	
	/*
	function vc_theme_vc_column_text($atts, $content = null) {
		$output = $el_class = $animation = '';

		extract(shortcode_atts(array(
		    'el_class' => '',
		    'animation' => '',
		    'color' => ''
		), $atts));

		$animated = ($animation == '' ? '' : 'animated');

		$el_class = WPBakeryShortCode_VC_Column_text::getExtraClass($el_class);

		$css_class = 'wpb_text_column wpb_content_element '.$el_class;
		$css_class .= ' '.$animated;
		$output .= "\n\t".'<div class="'.$css_class.'" data-type="'.$animation.'">';
		$output .= "\n\t\t".'<div class="wpb_wrapper">';
		$output .= "\n\t\t\t".wpb_js_remove_wpautop($content, true);
		$output .= "\n\t\t".'</div> ' . WPBakeryShortCode_VC_Column_text::endBlockComment('.wpb_wrapper');
		$output .= "\n\t".'</div> ' . WPBakeryShortCode_VC_Column_text::endBlockComment('.wpb_text_column');


		return $output;
	}
	*/

	function rowBuildStyle($bg_image = '', $bg_color = '', $bg_image_repeat = '', $font_color = '', $padding = '', $margin_bottom = '') {
        $has_image = false;
        $style = '';
        if((int)$bg_image > 0 && ($image_url = wp_get_attachment_url( $bg_image, 'large' )) !== false) {
            $has_image = true;
            $style .= "background-image: url(".$image_url.");";
        }
        if(!empty($bg_color)) {
            $style .= vc_get_css_color('background-color', $bg_color);
        }
        if(!empty($bg_image_repeat) && $has_image) {
            if($bg_image_repeat === 'cover') {
                $style .= "background-repeat:no-repeat;background-size: cover;";
            } elseif($bg_image_repeat === 'contain') {
                $style .= "background-repeat:no-repeat;background-size: contain;";
            } elseif($bg_image_repeat === 'no-repeat') {
                $style .= 'background-repeat: no-repeat;';
            }
        }
        if( !empty($font_color) ) {
            $style .= vc_get_css_color('color', $font_color); // 'color: '.$font_color.';';
        }
        if( $padding != '' ) {
            $style .= 'padding: '.(preg_match('/(px|em|\%|pt|cm)$/', $padding) ? $padding : $padding.'px').';';
        }
        if( $margin_bottom != '' ) {
            $style .= 'margin-bottom: '.(preg_match('/(px|em|\%|pt|cm)$/', $margin_bottom) ? $margin_bottom : $margin_bottom.'px').';';
        }
        return empty($style) ? $style : ' style="'.$style.'"';
    }

	function vc_theme_vc_row($atts, $content = null) {
	   /*
	   $output = $el_class = $full_width = $style = '';
		extract(shortcode_atts(array(
		    'el_class' => '',
		    'full_width' => '',
		    'style' => ''
		), $atts));

		wp_enqueue_style( 'js_composer_front' );
		wp_enqueue_script( 'wpb_composer_front_js' );
		wp_enqueue_style('js_composer_custom_css');

		//$el_class = $this->getExtraClass($el_class);

		if($full_width != 'true') {
			$css_class = 'row '.get_row_css_class().$el_class.' '.$style;
			$container_start = '<div class="container">';
			if($style == 'big-unit-box')
				$container_start .= '<div class="big-unit-box-inside">';
			$container_end = '</div>';
			$container_inner_start = $container_inner_end = '';
		}
		else {
			$css_class = 'full-width-row '.$el_class.' '.$style;
			$container_start = '';
			$container_end = '';
			$container_inner_start = $container_inner_end = '';
			if($style == 'big-unit-box') {
				$container_inner_start = '<div class="container"><div class="big-unit-box-inside">';
				$container_inner_end = '</div></div>';
			}

		}

		

		
		$output .= $container_start;
	
		
		$output .= '<div class="'.$css_class.'">';

		$output .= $container_inner_start;
		$output .= wpb_js_remove_wpautop($content);
		$output .= $container_inner_end;
		$output .= '</div>';
		$output .= $container_end;
		*/
	
		$output = $el_class = $bg_image = $bg_color = $bg_image_repeat = $font_color = $padding = $margin_bottom = $css = $id = '';
		extract(shortcode_atts(array(
		    'el_class'        => '',
		    'bg_image'        => '',
		    'bg_color'        => '',
		    'bg_image_repeat' => '',
		    'font_color'      => '',
		    'padding'         => '',
		    'margin_bottom'   => '',
		    'full_width'	  => '',
		    'id'			  => '',
		    'css' => ''
		), $atts));

		wp_enqueue_style( 'js_composer_front' );
		wp_enqueue_script( 'wpb_composer_front_js' );
		wp_enqueue_style('js_composer_custom_css');

		//$el_class = WPBakeryShortCode_VC_Row::getExtraClass($el_class);

		if($full_width != 'true') { 
			$css_class =  apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'wpb_row '.get_row_css_class().' '.$el_class.vc_shortcode_custom_css_class($css, ' '), 'vc_row');
			$container_start = '';
			$container_end = '';
			$first_container_start = '<div class="container relative">';
			$first_container_end = '</div>';
		}
		else {
			
			$css_class =  apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'full-width-row '.get_row_css_class().' '.$el_class.vc_shortcode_custom_css_class($css, ' '), 'vc_row');
			$container_start = '';
			$container_end = '';
			$first_container_start = '';
			$first_container_end = '';
		}

		//$style = WPBakeryShortCode_VC_Row::buildStyle($bg_image, $bg_color, $bg_image_repeat, $font_color, $padding, $margin_bottom);
		$style = rowBuildStyle($bg_image, $bg_color, $bg_image_repeat, $font_color, $padding, $margin_bottom);

		$output .= $first_container_start;
		//$output .= '<div class="'.$css_class.'"'.$style.' id="'.$id.'">';
		$output .= '<div class="'.$css_class.'" style="'.$style.'" id="'.$id.'">';
		$output .= $container_start;
		$output .= wpb_js_remove_wpautop($content);
		$output .= $container_end;
		$output .= '</div>';
		$output .= $first_container_end;

		return $output;

	}

	function vc_theme_vc_button($atts, $content = null) {
		$output = $color = $size = $icon = $target = $href = $el_class = $title = $position = $style = '';
		extract(shortcode_atts(array(
		    'color' => 'wpb_button',
		    'size' => '',
		    'icon' => 'none',
		    'target' => '_self',
		    'href' => '',
		    'el_class' => '',
		    'title' => __('Text on the button', "js_composer"),
		    'position' => '',
		    'style' => ''
		), $atts));
		$a_class = '';

		/*
		if ( $el_class != '' ) {
		    $tmp_class = explode(" ", strtolower($el_class));
		    $tmp_class = str_replace(".", "", $tmp_class);
		    if ( in_array("prettyphoto", $tmp_class) ) {
		        wp_enqueue_script( 'prettyphoto' );
		        wp_enqueue_style( 'prettyphoto' );
		        $a_class .= ' prettyphoto';
		        $el_class = str_ireplace("prettyphoto", "", $el_class);
		    }
		    if ( in_array("pull-right", $tmp_class) && $href != '' ) { $a_class .= ' pull-right'; $el_class = str_ireplace("pull-right", "", $el_class); }
		    if ( in_array("pull-left", $tmp_class) && $href != '' ) { $a_class .= ' pull-left'; $el_class = str_ireplace("pull-left", "", $el_class); }
		}
		*/
		if ( $target == 'same' || $target == '_self' ) { $target = ''; }
		$target = ( $target != '' ) ? ' target="'.$target.'"' : '';

		$color = ( $color != '' ) ? ' wpb_'.$color : '';
		$size = ( $size != '' && $size != 'wpb_regularsize' ) ? ' '.$size : ' '.$size;
		$icon = ( $icon != '' && $icon != 'none' ) ? ' '.$icon : '';
		$i_icon = ( $icon != '' ) ? ' <i class="fa '.$icon.'"></i>' : '';
		$position = ( $position != '' ) ? ' '.$position.'-button-position' : '';
		

		$css_class = apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, ''.$color.$size.' '.$el_class.' '.$position, 'vc_button');

		if ( $href != '' ) {
		    $output = '<a class="btn '.$css_class.' '.$style.'" title="'.$title.'" href="'.$href.'"'.$target.'>' . $i_icon.$title . '</a>';
		} else {
		    $output .= '<button class="btn '.$css_class.' '.$style.'">'.$i_icon.$title.'</button>';
		}

		return $output;
	}

	function vc_icon_box($atts, $content = null) {

		$output = $fa_icon = $heading = $link = $style = '';
		extract(shortcode_atts(array(
		    'fa_icon' => '',
		    'heading' => '',
		    'link' => '',
		    'style' => '',
		), $atts));
		ob_start(); 
		if($style == 'style1') {
		?> 
			<div class="services-1">
	         	
	         	<div class="services-1-icon">
	              <i class="fa <?php echo $fa_icon; ?>"></i>
	            </div>
	          	<div class="services-1-title"><a href="<?php echo $link; ?>"><?php echo $heading; ?></a></div>
	       		<p><?php echo wpb_js_remove_wpautop($content); ?></p>
	        </div>
		<?php
		}
		if($style == 'style2') { ?>
			<div class="services-2">
	            <div class="services-2-icon">
	              <i class="fa <?php echo $fa_icon; ?>"></i>
	              <h4><a href="<?php echo $link; ?>"><?php echo $heading; ?></a></h4>
	            </div>
	            <div class="services-2-text">              
	              <p><?php echo wpb_js_remove_wpautop($content); ?></p>
	            </div>
	        </div>
	    <?php
		}
		$output = ob_get_contents();
		ob_end_clean();
		return $output;
	}

	add_shortcode('vc_icon_box', 'vc_icon_box');


	function vc_spacer($atts, $content = null) {
		$output = $height = '';
		extract(shortcode_atts(array(
		    'height' => ''
		), $atts));

		$output = '<div class="spacer'.$height.'"></div>';

		return $output;
	}
	
	add_shortcode('vc_spacer', 'vc_spacer');

	function vc_theme_vc_progress_bar($atts, $content = null) {
		$output = $title = $values = $units = $bgcolor = $custombgcolor = $options = $el_class = '';
		extract( shortcode_atts( array(
		    'title' => '',
		    'values' => '',
		    'units' => '',
		    'el_class' => '',
		), $atts ) );

		$bar_options = '';
		$options = explode(",", $options);
		if (in_array("animated", $options)) $bar_options .= " animated";
		if (in_array("striped", $options)) $bar_options .= " striped";

		if ($bgcolor=="custom" && $custombgcolor!='') { $custombgcolor = ' style="background-color: '.$custombgcolor.';"'; $bgcolor=""; }
		if ($bgcolor!="") $bgcolor = " ".$bgcolor;

		$css_class = apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'skill-shortcode '.$el_class, 'vc_progress_bar');
		$output = '<div class="'.$css_class.'">';
		if($title != '') {
			$output .= '<h4>'.$title.'</h4>';
		}

		$graph_lines = explode(",", $values);
		$max_value = 0.0;
		$graph_lines_data = array();
		foreach ($graph_lines as $line) {
		    $new_line = array();
		    $color_index = 2;
		    $data = explode("|", $line);
		    $new_line['value'] = isset($data[0]) ? $data[0] : 0;
		    $new_line['percentage_value'] = isset($data[1]) && preg_match('/^\d{1,2}\%$/', $data[1]) ? (float)str_replace('%', '', $data[1]) : false;
		    if($new_line['percentage_value']!=false) {
		        $color_index+=1;
		        $new_line['label'] = isset($data[2]) ? $data[2] : '';
		    } else {
		        $new_line['label'] = isset($data[1]) ? $data[1] : '';
		    }
		    $new_line['bgcolor'] = (isset($data[$color_index])) ? ' style="background-color: '.$data[$color_index].';"' : $custombgcolor;

		    if($new_line['percentage_value']===false && $max_value < (float)$new_line['value']) {
		        $max_value = $new_line['value'];
		    }

		    $graph_lines_data[] = $new_line;
		}

		foreach($graph_lines_data as $line) {
			if($line['percentage_value']!==false) {
		        $percentage_value = $line['percentage_value'];
		    } elseif($max_value > 100.00) {
		        $percentage_value = (float)$line['value'] > 0 && $max_value > 100.00 ? round((float)$line['value']/$max_value*100, 4) : 0;
		    } else {
		        $percentage_value = $line['value'];
		    }
		    $unit = ($units!='') ? '' .  $line['value'] . $units . '' : $line['value'];
			
			$output .= '<div class="skill">';
			$output .= '<p>'.$line['label'].'</p>';		    
		    $output .= '<div class="progress">';
		    $output .= '<div class="progress-bar" role="progressbar" data-percentage="'.($percentage_value).'">';
		    $output .= '<span class="progress-bar-span" >'.$unit.'</span>';
		    $output .= '<span class="sr-only">'.$unit.' Complete</span>';
		    $output .= '</div>';
		    $output .= '</div>';
		    $output .= '</div>';
			    
			
		}

		$output .= '</div>';

		return $output . "\n";
	}

	function vc_theme_vc_wp_posts($atts, $content = null) {
		$output = $title = $number = $el_class = $style = '';
		extract( shortcode_atts( array(
		    'title' => __('Recent Posts', 'ovni'),
		    'number' => 5,
		    'el_class' => '',
		    'style' => ''
		), $atts ) );


		$output = '<div class="vc_wp_posts wpb_content_element '.$el_class.'">';
		$args = array();

		ob_start();
		if($title != '') {
			$output .= '<h4>'.$title.'</h4>';
		}
		if ($style == 'carousel' || $style == '') {
		?>
			<div class="owl-carousel carousel-top-navigation">           
	          <?php
	          $loop = new WP_Query( array( 'post_type' => 'post', 'posts_per_page' => $number ) );
			    while ( $loop->have_posts() ) : $loop->the_post();
			        $id_post = $loop->post->ID; 
			        $url = wp_get_attachment_url( get_post_thumbnail_id($id_post) );
			        $post_categories = wp_get_post_categories( $id_post );	
			        $categories = '';
			        $i = 0;
					foreach($post_categories as $c){
						$i++;
						$cat = get_category( $c );
						if($i != count($post_categories)) {
							$categories .= '<a href="'.get_category_link( $c ).'">'.$cat->name.'</a> / ';
						}
						else {
							$categories .= '<a href="'.get_category_link( $c ).'">'.$cat->name.'</a>';
						}
					}
			        ?>
			        <div class="item"> 
			            <div class="project-item">
			              <div class="project-image">
			                <a href="<?php the_permalink(); ?>" class="zoom" title="">                  
			                  <?php the_post_thumbnail('recent-news'); ?>
			                </a>                     
			              </div>
			              <div class="project-text">
			              <h5><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
			              <h6><?php echo $categories; ?></h6>
			              <?php the_excerpt(); ?>
			              </div>              
			            </div>              
			         </div>    
			        <?php
			          
			    endwhile;
			    wp_reset_query();
	          ?>
	               
	 
	        </div>    
		<?php
		} else { ?>
			<ul class="recent-posts">
			<?php
			$loop = new WP_Query( array( 'post_type' => 'post', 'posts_per_page' => $number ) );
			while ( $loop->have_posts() ) : $loop->the_post();
				?>
				<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>			
				<?php
			endwhile;
			wp_reset_query();
			?>
			</ul>
			<?php
		}
		$output .= ob_get_clean();

		$output .= '</div>' . "\n";

		return $output;
	}

	function vc_testimonial($atts, $content = full) {
		$output = $type = $name = $job = $image = $animation = $el_class = '';
		extract( shortcode_atts( array(
		    'name' => '',
		    'type' => 'type1',
		    'job' => '',
		    'image' => $image,
		    'animation' => '',
		    'el_class' => ''
		), $atts ) );
		ob_start();
		$animated = ($animation == '' ? '' : 'animated animation');
		$image_url = wp_get_attachment_image_src( $image, 'thumbnail' );
		if($type == 'type1') {
		?>	
			<div class="say-about-us <?php echo $animated; ?> <?php echo $el_class; ?>" data-type="<?php echo $animation; ?>">
		      <div class="row">
		        <div class="col-md-6 client-img">
		          <img src="<?php echo $image_url[0]; ?>" alt=""> 
		        </div>
		        <div class="col-md-6 client-data">
		          <h5><?php echo $name; ?></h5>
		          <h6 class="function"><?php echo $job; ?></h6>
		        </div>
		      </div>
		      <div class="row">
		        <div class="col-md-12">
		          <p>
		            <?php echo wpb_js_remove_wpautop($content); ?>
		          </p>
		        </div> 
		      </div> 
		    </div>   
			
		<?php
		} else {
			?>
			<?php if($title != '') { ?>
		        <h4><?php echo $title; ?></h4>
		    <?php } ?>
		    <div class="<?php echo $animated; ?> <?php echo $el_class; ?>" data-type="<?php echo $animation; ?>">         
			    <div class="client-says">            
		          “<?php echo wpb_js_remove_wpautop($content); ?>”
		        </div>
		        <div class="client-says-2">
		          <div class="client-name">
		            <i class="fa fa-user"></i><?php echo $name; ?>, <?php echo $job; ?>.
		          </div>  
		        </div>
		    </div> 
			<?php
		}
		$output .= ob_get_clean();
		return $output;
	}

	add_shortcode('vc_testimonial', 'vc_testimonial');

	function vc_theme_vc_posts_slider($atts, $content = null) {
		$output = $type = $count = $interval = $transition = $image = $image_size = '';
		$custom_links  = $categories = $layout = '';
		$orderby = $order = $el_class = '';
		extract(shortcode_atts(array(
		    'type' => 'flexslider',
		    'layout' => 'normal',
		    'image' => $image,
		    'image_size' => 'slider',
		    'count' => 3,
		    'interval' => 3,
		    'transition' => 'slide',
		    'categories' => '',
		    'orderby' => NULL,
		    'order' => 'DESC',
		    'el_class' => ''
		), $atts));
		ob_start();

		
		$slideshow = ($interval == 0 ? 'false' : 'true');
		if($slideshow == 'false') $interval = 100000;
		if($count == '' || $count == 'All') $count = -1;

		if ($type == 'flexslider') {

			if($layout == 'thumbnails') {
			?>
			
				<div class="thumbnail-slider" data-interval="<?php echo $interval; ?>" data-transition="<?php echo $transition; ?>" data-slideshow="<?php echo $slideshow; ?>">
					<ul class="slides">
					<?php
					$loop = new WP_Query( array( 'post_type' => 'slider', 'posts_per_page' => $count, 'order_by' => $orderby, 'order' => $order, 'category_name' => $categories ) );
					while ( $loop->have_posts() ) : $loop->the_post();
				        $id_post = $loop->post->ID; 
				        $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($id_post, $image_size ));
				        ?>
				        <li data-thumb="<?php echo $thumb[0]; ?>">
				        	<?php //echo $cont_wrapper_start; ?>
				        	<?php the_post_thumbnail($image_size); ?>
				        	<?php //echo $cont_wrapper_end; ?>
				        </li>
				        <?php
				    endwhile;
				    wp_reset_query();
				    ?>
					</ul>
				</div>
			
		    <?php
			}
			elseif($layout == 'text') {
				?>
				<div class="testimonal-slider"> 
					<ul class="slides">
						<?php
						$loop = new WP_Query( array( 'post_type' => 'slider', 'posts_per_page' => $count, 'order_by' => $orderby, 'order' => $order, 'category_name' => $categories ) );
						while ( $loop->have_posts() ) : $loop->the_post();
					        $id_post = $loop->post->ID; 
					        $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), $image_size );
					        ?>
					        <li>
					        	<div class="testimonial">
					        		<?php the_content(); ?>
					        	</div>
					        </li>
					        <?php
					    endwhile;
					    wp_reset_query();
					    ?>
					</ul>
				</div>
				<?php
			}
			elseif($layout == 'laptop') {
				?>
				<div class="laptop">
		          <img src="<?php echo get_template_directory_uri(); ?>/img/laptop.png" alt="">
		          <div class="laptop-slider">            
		            <ul class="slides">                     
		              <?php
						$loop = new WP_Query( array( 'post_type' => 'slider', 'posts_per_page' => $count, 'order_by' => $orderby, 'order' => $order, 'category_name' => $categories ) );
						while ( $loop->have_posts() ) : $loop->the_post();
					        $id_post = $loop->post->ID; 
					        $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), $image_size );
					        ?>
					        <li>
					        	
					        	<?php the_post_thumbnail('recent-news'); ?>
					        	
					        </li>
					        <?php
					    endwhile;
					    wp_reset_query();
					    ?>         
		            </ul>
		          </div>
		        </div>
				<?php
			}
			else {

			}
		}
		elseif($type == 'slyslider') {
			?>
			<div class="wrap">
				<div class="frame effects" id="effects">
					<ul class="clearfix">
						<?php
						$loop = new WP_Query( array( 'post_type' => 'slider', 'posts_per_page' => $count, 'order_by' => $orderby, 'order' => $order, 'category_name' => $categories ) );
						while ( $loop->have_posts() ) : $loop->the_post();
				        	$id_post = $loop->post->ID; 
				        	?>
							<li><?php the_post_thumbnail('team'); ?></li>
				         	<?php
				        endwhile;
				        wp_reset_query();
				        ?>
					</ul>
				</div>
				<div class="controls center">
				    <button class="btn prev"><i class="fa fa-chevron-left"></i> <?php _e('prev', 'reno'); ?></button>
					<button class="btn next"><?php _e('next', 'reno'); ?> <i class="fa fa-chevron-right"></i></button>
				</div>
			</div>	
			<?php
		}
		elseif($type == 'refineslider') {
			?>
			<ul class="main-slider-direct-nav">
				<?php
					$loop = new WP_Query( array( 'post_type' => 'slider', 'posts_per_page' => $count, 'order_by' => $orderby, 'order' => $order, 'category_name' => $categories ) );
					while ( $loop->have_posts() ) : $loop->the_post();
			        	$id_post = $loop->post->ID; 
	        			$full = wp_get_attachment_image_src( get_post_thumbnail_id($id_post), 'slider' );
			        	?>
					    <li>				                
				            <img src="<?php echo $full[0]; ?>" alt="">
						</li>
				    	<?php
				    endwhile;
				    wp_reset_query();
				    ?>
			  </ul>
			<?php
		}
		elseif($type == 'parallaxslider') {
			$image_url = wp_get_attachment_image_src( $image, 'full' );
			?>
			<div id="parallax-one" class="parallax parallax-slider" style="background-image:url(<?php echo $image_url[0]; ?>);">          
			    <div class="container">
			      <div class="row">   
			        <div class="col-md-12">
			          <div class="flex-1" data-interval="<?php echo $interval; ?>" data-transition="<?php echo $transition; ?>" data-slideshow="<?php echo $slideshow; ?>">
			            <ul class="slides">
			            	<?php          
			                $loop = new WP_Query( array( 'post_type' => 'slider', 'posts_per_page' => $count, 'order_by' => $orderby, 'order' => $order, 'category_name' => $categories ) );
							while ( $loop->have_posts() ) : $loop->the_post();
						        $id_post = $loop->post->ID; 
						        ?>
						        <li>
						        	<div class="parallax-text-container">
                  						<div class="parallax-text-item">
						        			<?php the_content(); ?>
						        		</div>
						        	</div>
						        </li>
						        <?php
						    endwhile;
						    wp_reset_query();
						    ?>
			            </ul>
			          </div>          
			        </div>         
			      </div>
			    </div>
			  </div>

			<?php
		}
	    $output .= ob_get_clean();
				

		return $output;
	}

	function vc_4_image_boxes($atts, $content = null) {
		$output = $box1link = $box2link = $box3link = $box4link = '';
		$image1 = $image2 = $image3 = $image4 = '';
		extract(shortcode_atts(array(
		    'box1link' => '',
		    'box2link' => '',
		    'box3link' => '',
		    'box4link' => '',
		    'image1' => $image1,
		    'image2' => $image2,
		    'image3' => $image3,
		    'image4' => $image4
		), $atts));

		$image1_url = wp_get_attachment_image_src( $image1, 'lightbox' );
		$image1_img = wp_get_attachment_image_src( $image1, 'medium' );
		$image2_url = wp_get_attachment_image_src( $image2, 'lightbox' );
		$image2_img = wp_get_attachment_image_src( $image2, 'medium' );
		$image3_url = wp_get_attachment_image_src( $image3, 'lightbox' );
		$image3_img = wp_get_attachment_image_src( $image3, 'medium' );
		$image4_url = wp_get_attachment_image_src( $image4, 'lightbox' );
		$image4_img = wp_get_attachment_image_src( $image4, 'medium' );
		ob_start(); ?>
		<div class="img-slider">
	        <div class="media-content-2">
	          <div class="media-box">
	            <img src="<?php echo $image1_img[0]; ?>">
	            <div class="overlay"><span class="divider"></span>
	              <a href="<?php echo $image1_url[0]; ?>" class="media-link-1 popup-link"  title=""><?php _e('SHOW IT', 'reno'); ?></a>
	              <?php if($box1link != '') { ?>
	              		<a class="media-link-2" href="<?php echo $box1link; ?>"><?php _e('OPEN PAGE', 'reno'); ?></a>
	              <?php } ?>            
	            </div>
	          </div>
	          <div class="media-box">
	            <img src="<?php echo $image2_img[0]; ?>" alt="sdf">
	            <div class="overlay"><span class="divider"></span>
	              <a href="<?php echo $image2_url[0]; ?>" class="media-link-1 popup-link" title="">SHOW IT</a>
	              <?php if($box2link != '') { ?>
	              		<a class="media-link-2" href="<?php echo $box2link; ?>"><?php _e('OPEN PAGE', 'reno'); ?></a>
	              <?php } ?>    
	            </div>
	          </div>
	          <div class="media-box">
	            <img src="<?php echo $image3_img[0]; ?>" alt="sdf">
	            <div class="overlay"><span class="divider"></span>
	              <a href="<?php echo $image3_url[0]; ?>" class="media-link-1 popup-link" title="">SHOW IT</a>
	              <?php if($box3link != '') { ?>
	              		<a class="media-link-2" href="<?php echo $box3link; ?>"><?php _e('OPEN PAGE', 'reno'); ?></a>
	              <?php } ?>               
	            </div>
	          </div>
	          <div class="media-box">
	            <img src="<?php echo $image4_img[0]; ?>" alt="sdf">
	            <div class="overlay"><span class="divider"></span>
	              <a href="<?php echo $image4_url[0]; ?>" class="media-link-1 popup-link" data-rel="prettyPhoto" title="">SHOW IT</a>
	              <?php if($box4link != '') { ?>
	              		<a class="media-link-2" href="<?php echo $box4link; ?>"><?php _e('OPEN PAGE', 'reno'); ?></a>
	              <?php } ?>                
	            </div>
	          </div>                              
	        </div>
	     </div>
	    <?php
	    $output .= ob_get_clean();

	    return $output;

	}
	add_shortcode('vc_4_image_boxes', 'vc_4_image_boxes');

	

	function vc_hero_unit($atts, $content = null) {
		$output = $fa_icon = $entypo_icon = $text = $animation = $icon_source = $text_align = $color = '';
		extract(shortcode_atts(array(
		    'icon_source' => 'fa',
		    'fa_icon' => '',
		    'entypo_icon' => '',
		    'text' => '',
		    'text_align' => '',
		    'animation' => '',
		    'color' => ''
		), $atts));
		$animated = ($animation == '' ? '' : 'animated animation' );
		
		ob_start(); ?>
		<div class="promo-box-container" style="background-color: <?php echo $color; ?>">
			<span class="triangle" style="border-color: <?php echo $color; ?> transparent"></span>
		    <div class="container">  
		      <div class="row">
		        <div class="col-md-12 <?php echo $text_align; ?>">
		          <div class="promo-box <?php echo $animated; ?>" data-type="<?php echo $animation; ?>">

		            <div class="text <?php if($fa_icon == '') echo 'no-margin'; ?>"><?php echo wpb_js_remove_wpautop($content); ?></div>
		            <?php 
		            if($icon_source == 'fa')
		            	$icon = $fa_icon;
		            else 
		            	$icon = $entypo_icon;
		            ?>
		            <i class="<?php echo $icon_source; ?> <?php echo $icon; ?>"></i>
		          </div>  
		        </div>
		      </div>    
		    </div>   
		</div> 
		<?php
		$output .= ob_get_clean();

	    return $output;
	}

	add_shortcode('vc_hero_unit', 'vc_hero_unit');

	function vc_team_member($atts, $content = null) {
		$output = $team_member = '';
		extract(shortcode_atts(array(
		    'team_member' => '',
		), $atts));

		ob_start();


		$loop = new WP_Query( array( 'post_type' => 'team', 'p' => $team_member ) );
		while ( $loop->have_posts() ) : $loop->the_post();
	        $id_post = $loop->post->ID; 
	        $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($id_post), 'team' );
	        $full = wp_get_attachment_image_src( get_post_thumbnail_id($id_post), 'listing' );
	        ?>
	        <div class="team">
	          <div class="team-img">
	          	<img src="<?php echo $thumb[0]; ?>" alt="">
	          	<a href="mailto:<?php echo get_post_meta($id_post, 'member_email', true ); ?>" class="team-email"><i class="fa fa-envelope"></i></a>
	          	<div class="overlay"></div>	
	          </div>
	        
	                    
	            <div class="team-socials">
	              <?php if(get_post_meta($id_post, 'member_adn', true )) { ?>	
	              	<a href="<?php echo get_post_meta($id_post, 'member_adn', true ); ?>"><i class="fa fa-adn"></i></a>
	              <?php } ?>
	              <?php if(get_post_meta($id_post, 'member_android', true )) { ?>	
	              	<a href="<?php echo get_post_meta($id_post, 'member_android', true ); ?>"><i class="fa fa-android"></i></a>
	              <?php } ?>
	              <?php if(get_post_meta($id_post, 'member_apple', true )) { ?>	
	              	<a href="<?php echo get_post_meta($id_post, 'member_apple', true ); ?>"><i class="fa fa-apple"></i></a>
	              <?php } ?>
	              <?php if(get_post_meta($id_post, 'member_bitbucket', true )) { ?>	
	              	<a href="<?php echo get_post_meta($id_post, 'member_bitbucket', true ); ?>"><i class="fa fa-bitbucket"></i></a>
	              <?php } ?>
	              <?php if(get_post_meta($id_post, 'member_css3', true )) { ?>	
	              	<a href="<?php echo get_post_meta($id_post, 'member_css3', true ); ?>"><i class="fa fa-css3"></i></a>
	              <?php } ?>
	              <?php if(get_post_meta($id_post, 'member_dribbble', true )) { ?>	
	              	<a href="<?php echo get_post_meta($id_post, 'member_dribbble', true ); ?>"><i class="fa fa-dribbble"></i></a>
	              <?php } ?>
	              <?php if(get_post_meta($id_post, 'member_dropbox', true )) { ?>	
	              	<a href="<?php echo get_post_meta($id_post, 'member_dropbox', true ); ?>"><i class="fa fa-dropbox"></i></a>
	              <?php } ?>
	              <?php if(get_post_meta($id_post, 'member_foursquare', true )) { ?>	
	              	<a href="<?php echo get_post_meta($id_post, 'member_foursquare', true ); ?>"><i class="fa fa-foursquare"></i></a>
	              <?php } ?>
	              <?php if(get_post_meta($id_post, 'member_github', true )) { ?>	
	              	<a href="<?php echo get_post_meta($id_post, 'member_github', true ); ?>"><i class="fa fa-github"></i></a>
	              <?php } ?>
	              <?php if(get_post_meta($id_post, 'member_html5', true )) { ?>	
	              	<a href="<?php echo get_post_meta($id_post, 'member_html5', true ); ?>"><i class="fa fa-html5"></i></a>
	              <?php } ?>
	              <?php if(get_post_meta($id_post, 'member_instagram', true )) { ?>	
	              	<a href="<?php echo get_post_meta($id_post, 'member_instagram', true ); ?>"><i class="fa fa-instagram"></i></a>
	              <?php } ?>
	              <?php if(get_post_meta($id_post, 'member_facebook', true )) { ?>	
	              	<a href="<?php echo get_post_meta($id_post, 'member_facebook', true ); ?>"><i class="fa fa-facebook"></i></a>
	              <?php } ?>
	              <?php if(get_post_meta($id_post, 'member_google', true )) { ?>	
	              	<a href="<?php echo get_post_meta($id_post, 'member_google', true ); ?>"><i class="fa fa-google-plus"></i></a>
	              <?php } ?>
	              <?php if(get_post_meta($id_post, 'member_skype', true )) { ?>	
	              	<a href="<?php echo get_post_meta($id_post, 'member_skype', true ); ?>"><i class="fa fa-skype"></i></a>
	              <?php } ?>
	              <?php if(get_post_meta($id_post, 'member_twitter', true )) { ?>	
	              	<a href="<?php echo get_post_meta($id_post, 'member_twitter', true ); ?>"><i class="fa fa-twitter"></i></a>
	              <?php } ?>
	              <?php if(get_post_meta($id_post, 'member_linkedin', true )) { ?>	
	              	<a href="<?php echo get_post_meta($id_post, 'member_linkedin', true ); ?>"><i class="fa fa-linkedin"></i></a>
	              <?php } ?>
	              <?php if(get_post_meta($id_post, 'member_pinterest', true )) { ?>	
	              	<a href="<?php echo get_post_meta($id_post, 'member_pinterest', true ); ?>"><i class="fa fa-pinterest"></i></a>
	              <?php } ?>
	              <?php if(get_post_meta($id_post, 'member_flickr', true )) { ?>	
	              	<a href="<?php echo get_post_meta($id_post, 'member_flickr', true ); ?>"><i class="fa fa-flickr"></i></a>
	              <?php } ?>
	            </div>
	            <div class="team-name"><h3><?php the_title(); ?></h3></div>
	            <div class="team-position"><?php echo get_post_meta($id_post, 'member_job', true ); ?></div>   
	         
	        </div> 
	        <?php
	    endwhile;
	    wp_reset_query();

	    $output .= ob_get_clean();

	    return $output;

	}

	add_shortcode('vc_team_member', 'vc_team_member');


	function vc_clients($atts, $content = null) {
		$output = $widget_title = '';
		extract(shortcode_atts(array(
		    'widget_title' => '',
		), $atts));

		ob_start();
		?>
		<?php if($widget_title != '') { ?>
			<h4><?php echo $widget_title; ?></h4>
		<?php } ?>             
        <div class="logos">       
            
            <?php 
          	$loop = new WP_Query( array( 'post_type' => 'client', 'posts_per_page' => -1 ) );
		  	while ( $loop->have_posts() ) : $loop->the_post();
	          	$id_post = $loop->post->ID;
	          	?>
	          	<span class="logo-cell">
	                <a href="<?php echo get_post_meta($id_post, 'ovni_client_link', true); ?>"><?php the_post_thumbnail( 'client', array('class' => 'client1') ); ?></a>                 
	            </span>   
	          	<?php
	        endwhile; 
	        wp_reset_query();
          	?>                                                                                                                                                                   
        </div>
		<?php
		$output .= ob_get_clean();

	    return $output;
	}
	add_shortcode('vc_clients', 'vc_clients');

	function vc_last_portfolio($atts, $content = null) {
		$output = $count = $style = '';
		extract(shortcode_atts(array(
		    'count' => '',
		    'style' => ''
		), $atts));

		ob_start();
		if ($style == 'style1') {
		?>           
	        <div class="row wpb_row">
	        	<?php
	        	$loop = new WP_Query( array( 'post_type' => 'portfolio', 'posts_per_page' => $count ) );
			  	while ( $loop->have_posts() ) : $loop->the_post();
		          	$id_post = $loop->post->ID;
		          	$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($id_post), 'recent-news' );
		    		$full = wp_get_attachment_image_src( get_post_thumbnail_id($id_post), 'lightbox' );
		          	?>
		          	<div class="col-sm-3 col-md-3">
		          		<div class="project-boxed">
					        <!-- Projects -->
					        
					        <div class="media-content">	
					            <img src="<?php echo $thumb[0]; ?>" alt="">
					        </div>
				            <div class="media-text">
				              <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
				              <h5><?php echo ovni_show_portfolio_categories($id_post); ?></h5>
				            </div>
					                           
					          
					        
					        <!-- Projects End -->
					    </div>
				    </div>   
			    <?php
			    endwhile;
			    wp_reset_query();
			    ?>
	        </div>
		<?php
		}
		if ($style == 'style2') { ?>
			
				
			<div class="projects-slider">
				<div class="row">
	    			<ul class="slides">
	    				<li> 
	    				<?php
	    				$i = 0;
	    				$loop = new WP_Query( array( 'post_type' => 'portfolio', 'posts_per_page' => $count ) );
					  	while ( $loop->have_posts() ) : $loop->the_post();
				          	$id_post = $loop->post->ID;
				          	$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($id_post), 'recent-news' );
				    		$full = wp_get_attachment_image_src( get_post_thumbnail_id($id_post), 'lightbox' );
				    		$count = $loop->post_count;
				    		$i++;
				          	?>

					            <div class="col-xs-4 col-sm-4 col-md-4 slider-content">  
					                <a href="<?php the_permalink(); ?>">

						              	<img src="<?php echo $thumb[0]; ?>" alt="">
						              
							            <div class="slider-text">
							                <h3><?php the_title(); ?></h3>
							                <p><?php echo get_post_meta($id_post, 'ovni_portfolio_excerpt', true); ?></p>
							            </div>  
					                </a>                       
					            </div>  
					            <?php
					            if (($i % 3) == 0 && $i != $count) { ?>
						    		</li><li>
						    	<?php
						    	} 
					            ?>

					    <?php
					    endwhile;
					    wp_reset_query();
					    ?>
					    </li> 
	    			</ul>
	    		</div>
    		</div>
        	<div class="spacer40"></div>
			
		<?php
		}
		$output .= ob_get_clean();

		return $output;
	}
	add_shortcode('vc_last_portfolio', 'vc_last_portfolio');

	function vc_theme_vc_accordion_tab($atts, $content = null) {
		$output = $title = '';

		extract(shortcode_atts(array(
			'title' => __("Section", "js_composer")
		), $atts));

		$css_class = apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'panel panel-default', 'vc_accordion_tab');
		$output .= "\n\t\t\t" . '<div class="'.$css_class.'">';
			$output .= "\n\t\t\t\t" . '<div class="panel-heading">';
		    $output .= "\n\t\t\t\t\t" . '<h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#'.sanitize_title($title).'">'.$title.'</a></h4>';
		    $output .= "\n\t\t\t\t" . '</div>';
		    $output .= "\n\t\t\t\t" . '<div id="'.sanitize_title($title).'" class="panel-collapse collapse"><div class="panel-body">';
		        $output .= ($content=='' || $content==' ') ? __("Empty section. Edit page to add content here.", "js_composer") : "\n\t\t\t\t" . wpb_js_remove_wpautop($content);
		    $output .= "\n\t\t\t\t" . '</div></div>';
		    $output .= "\n\t\t\t" . '</div> ';

		return $output;
	}

	function vc_theme_vc_accordion($atts, $content = null) {
		$output = $title = $interval = $el_class = $collapsible = $active_tab = '';
		//
		extract(shortcode_atts(array(
		    'title' => '',
		    'interval' => 0,
		    'el_class' => '',
		    'collapsible' => 'no',
		    'active_tab' => '1'
		), $atts));

		$css_class = apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'panel-group '.$el_class, 'vc_accordion');

		if($title != '') { 
			$output .= '<h4>'.$title.'</h4>';
		}
		$output .= "\n\t".'<div class="'.$css_class.'" id="accordion">'; //data-interval="'.$interval.'"
		$output .= "\n\t\t\t".wpb_js_remove_wpautop($content);
		$output .= "\n\t".'</div> ';

		return $output;
	}

	function reno_list_portfolio_categories($cat_id) {

		global $entiri_opt;

		$idObj = get_category_by_slug($cat_id); 
  		$id = $idObj->term_id;

		$html = '<ul id="portfolio-filter">
	        		<li class="act"><a href="#" class="filter" data-filter="*">All</a></li>';
	    $cat_id = $entiri_opt['portfolio-category'];
		$categories = get_categories( array('hide_empty' => 1, 'child_of' => $id) );
		foreach ($categories as $category) { 
			$html .= '<li><a href="#" class="filter" data-filter=".'.$category->slug.'">'.$category->name.'</a></li>';
		}
		$html .= '</ul>';

		return $html;
	}

	function reno_list_post_categories($id_post) {
		$post_categories = wp_get_post_categories( $id_post );
		$cats = array();
		$numItems = count($post_categories);	
		$html = '';
		$i = 0;
		foreach($post_categories as $c){
			$cat = get_category( $c );
			$html .= $cat->slug;
			if(++$i !== $numItems) {
			    $html .= ',';
			}
		}
		return $html;
	}

	function vc_portfolio_display($atts) {
		global $entiri_opt;
		$html = $category = $rows = $show_filter = $style = $isotope_layout = $filter_align = '';

		extract(shortcode_atts(array(
			'category' => '',
		    'show_filter' => 'true',
		    'filter_align' => '',
		    'rows' => 'all',
		    'style' => '',
		    'isotope_layout' => 'fitRows'
		), $atts));

		if($category == '') {
			$cat_obj = get_category( $entiri_opt['portfolio-category'], false );
			$cat_slug = $cat_obj->slug;
		}
		else {
			$cat_slug = $category;
		}

		if($show_filter == 'true') { 
			$html = '<div class="filter-wrapper '.$filter_align.'">';
			$html .= reno_list_portfolio_categories($cat_slug);
			$html .= '</div>';
		}
		else {
			$html = '';
		}
		
		$cat_id = $entiri_opt['portfolio-category'];
		if ($rows == '4') $class="col-md-3 col-sm-6";
		if ($rows == '3') $class="col-md-4 col-sm-6";
		if ($rows == '2') $class="col-md-6 col-sm-6";

		$size = 'slider-small';
		$show_desc = true;
		$i = 1;
		$html .= '<section id="portfolio-items" data-layout="'.$isotope_layout.'">
					<ul class="portfolio row">';
		$loop = new WP_Query( array( 'post_type' => 'portfolio', 'category_name' => $cat_slug, 'posts_per_page' => -1 ) );
	    while ( $loop->have_posts() ) : $loop->the_post(); 
	    	$id_post = $loop->post->ID;
	    	$url = wp_get_attachment_url( get_post_thumbnail_id($id_post) );
	    	if($rows == 'all') {
	    		$size = 'slider-small';
	    		if($i%9 > 0 && $i%9 < 5) {
	    			$class = 'col-md-3';
	    			$show_desc = true;
	    		}
	    		if($i%9 > 4 && $i%9 < 8) {
	    			$class = 'col-md-4';
	    			$show_desc = false;
	    		}
	    		if($i%9 > 7 && $i%9 < 10) {
	    			$class = 'col-md-6';
	    			$show_desc = true;
	    		}
	    		

	    	}
	    	
    		if($isotope_layout == 'masonry') {
    			$size = 'portfolio-masonry';
    		}
	    	$html .= '<li><article class="'.$class.' project" data-tags="'.reno_list_post_categories($id_post).'">';
	    	$html .= '
	    				
	                   
	                    <div class="media-container"><div class="media-file"><a class="open-popup-link" href="#project-detail-'.$id_post.'">';
	    	$html .= get_the_post_thumbnail($id_post,$size);

			$html .= '</a></div></div>';
			
			
	    	$html .= '</article>
	    	
	    	  <section class="project-detail mfp-hide" id="project-detail-'.$id_post.'">
			    <div class="container">
			        
			        
			          <div class="image">
			            '.get_the_post_thumbnail($id_post,'project-detail', array('class' => 'image-bottom')).'
			          </div>
			        <div class="row">
			          <div class="col-sm-12">
			          	<div class="text">
			            <h2 class="text-left">'.get_the_title($id_post).'</h2>
			            '.get_the_content();

			            if(get_post_meta($id_post, 'ovni_portfolio_url', true)) {
			          		$html .= '
			          		<p><a class="btn btn-primary project-link" href="'.get_post_meta($id_post, 'ovni_portfolio_url', true).'">Project page</a></p>';
			            }
			          $img_url = $thumb['0']; 
			          $title=urlencode(get_the_title());
					  $url=urlencode(get_permalink());
					  $summary=urlencode(get_the_content());
					  $image=urlencode($img_url);
					  ob_start();
					  ?>
					  <p>
					  <a onClick="window.open('http://www.facebook.com/sharer.php?s=100&amp;p[title]=<?php echo $title;?>&amp;p[summary]=<?php echo $summary;?>&amp;p[url]=<?php echo $url; ?>&amp;p[images][0]=<?php echo $image;?>','sharer','toolbar=0,status=0,width=548,height=325');" href="javascript: void(0)" class="portfolio-share-fb"><i class="fa fa-facebook" title="facebook"></i></a>
					  <a onClick="window.open('https://plus.google.com/share?url=<?php echo $url; ?>','sharer','toolbar=0,status=0,width=548,height=325');" href="javascript: void(0)" class="portfolio-share-google"><i class="fa fa-google-plus a-bounce" title="Google plus"></i></a>
					  <a onClick="window.open('http://twitter.com/home?status=<?php echo $title; ?> <?php echo $url; ?>','sharer','toolbar=0,status=0,width=548,height=325');" href="javascript: void(0)" class="portfolio-share-twitter"><i class="fa fa-twitter a-bounce" title="twitter"></i></a>
			          </p>
			          <?php
			          $html .= ob_get_contents();
					  ob_end_clean();
			          $html .= '
			             </div>
			          </div>
			        </div>


			    </div>     
			  </section>
			 
	    	</li>';
	    	$i++;
	    endwhile;
		$html .= '	</ul>
				  </section>';

		wp_reset_query();
		return $html;
	}

	add_shortcode('vc_portfolio_display', 'vc_portfolio_display');

	function vc_pricing_table($atts,$content = null) {
		$output = $row1 = $row2 = $row3 = $row4 = $row5 = $row6 = $row7 = $row8 = $row9 = $row10 = '';
		$plan_title = $plan_price = $price_text = $footer_text = '';
		extract(shortcode_atts(array(
		    'row1' => '',
		    'row2' => '',
		    'row3' => '',
		    'row4' => '',
		    'row5' => '',
		    'row6' => '',
		    'row7' => '',
		    'row8' => '',
		    'row9' => '',
		    'row10' => '',
		    'plan_title' => '',
		    'plan_price' => '',
		    'price_text' => '',
		    'footer_text' => ''
		), $atts));
		ob_start(); ?> 
	 	<table class="pricing-table">
          <thead>
          	<tr>
          		<th>
          			<div class="plan-title"><?php echo $plan_title; ?></div>
          			<div class="price"><?php echo $plan_price; ?>
		              <span class="price2"><?php echo $price_text; ?></span>
		            </div> 
          		</th>
          	</tr>
            
             
          </thead>
          <tbody>
	          <?php if($row1 != '') { ?>
	          	  <tr><td><?php echo $row1; ?></td></tr>
	          <?php } ?>
	          <?php if($row2 != '') { ?>
	          	  <tr><td><?php echo $row2; ?></td></tr>
	          <?php } ?>
	          <?php if($row3 != '') { ?>
	          	  <tr><td><?php echo $row3; ?></td></tr>
	          <?php } ?>
	          <?php if($row4 != '') { ?>
	          	  <tr><td><?php echo $row4; ?></td></tr>
	          <?php } ?>
	          <?php if($row5 != '') { ?>
	          	  <tr><td><?php echo $row5; ?></td></tr>
	          <?php } ?>
	          <?php if($row6 != '') { ?>
	          	  <tr><td><?php echo $row6; ?></td></tr>
	          <?php } ?>
	          <?php if($row7 != '') { ?>
	          	  <tr><td><?php echo $row7; ?></td></tr>
	          <?php } ?>
	          <?php if($row8 != '') { ?>
	          	  <tr><td><?php echo $row8; ?></td></tr>
	          <?php } ?>
	          <?php if($row9 != '') { ?>
	          	  <tr><td><?php echo $row9; ?></td></tr>
	          <?php } ?>
	          <?php if($row10 != '') { ?>
	          	  <tr><td><?php echo $row10; ?></td></tr>
	          <?php } ?>
	          <tr>
	          	  <td class="table-footer">
	                <?php echo $footer_text; ?>
	          	  </td>
	      	  </tr>
      	  </tbody>
        </table>
	  		
		<?php
		$output = ob_get_contents();
		ob_end_clean();
		return $output;
	}

	add_shortcode('vc_pricing_table', 'vc_pricing_table' );

	function vc_theme_vc_message($atts, $content = null) {

		$output = $color = $el_class = $css_animation = '';
		extract(shortcode_atts(array(
		    'color' => 'alert-info',
		    'el_class' => '',
		    'css_animation' => ''
		), $atts));

		if ($color == "alert-block") $color = "";
		$color = ( $color != '' ) ? ' '.$color : '';
		$css_class = apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'alert wpb_content_element'.$color.$el_class, 'vc_message');

		$output .= '<div class="'.$css_class.'"><button type="button" class="close" data-dismiss="alert">&times;</button><div class="messagebox_text clearfix">'.wpb_js_remove_wpautop($content, true).'</div></div>';

		return $output;

	}

	function vc_social_icons($atts, $content=null) {
		$output = $style = $title = '';
		$adn = $android = $apple = $bitbucket = $css3 = $dribbble = $dropbox = $facebook = '';
		$flickr = $foursquare = $github = $google_plus = $html5 = $instagram = $linkedin = '';
		$renren = $skype = $stackexchange = $trello = $tumblr = $twitter = $vk = $weibo = $windows = '';
		$xing = $youtube = '';
		extract(shortcode_atts(array(
		    'title' => '',
		    'style' => '',
		    'adn' => '',
		    'android' => '',
		    'apple' => '',
		    'bitbucket' => '',
		    'css3' => '',
		    'dribbble' => '',
		    'dropbox' => '',
		    'facebook' => '',
		    'flickr' => '',
		    'foursquare' => '',
		    'github' => '',
		    'google_plus' => '',
		    'html5' => '',
		    'instagram' => '',
		    'linkedin' => '',
		    'renren' => '',
		    'skype' => '',
		    'stackexchange' => '',
		    'trello' => '',
		    'tumblr' => '',
			'twitter' => '',
			'vk' => '',
			'weibo' => '',
			'windows' => '',
			'xing' => '',
			'youtube' => ''
		), $atts));
		ob_start(); ?> 
		<div class="<?php echo $style; ?>">
			<?php if($title != '') { ?>
				<h4><?php echo $title; ?></h4>
			<?php } ?>
			<?php if($adn != '') { ?>
				<a href="<?php echo $adn; ?>"><i class="fa fa-adn"></i></a>
			<?php } ?>
			<?php if($android != '') { ?>
				<a href="<?php echo $android; ?>"><i class="fa fa-android"></i></a>
			<?php } ?>
			<?php if($apple != '') { ?>
				<a href="<?php echo $apple; ?>"><i class="fa fa-apple"></i></a>
			<?php } ?>
			<?php if($bitbucket != '') { ?>
				<a href="<?php echo $bitbucket; ?>"><i class="fa fa-bitbucket"></i></a>
			<?php } ?>
			<?php if($css3 != '') { ?>
				<a href="<?php echo $css3; ?>"><i class="fa fa-css3"></i></a>
			<?php } ?>
			<?php if($dribbble != '') { ?>
				<a href="<?php echo $dribbble; ?>"><i class="fa fa-dribbble"></i></a>
			<?php } ?>
			<?php if($dropbox != '') { ?>
				<a href="<?php echo $dropbox; ?>"><i class="fa fa-dropbox"></i></a>
			<?php } ?>
			<?php if($facebook != '') { ?>
				<a href="<?php echo $facebook; ?>"><i class="fa fa-facebook"></i></a>
			<?php } ?>
			<?php if($flickr != '') { ?>
				<a href="<?php echo $flickr; ?>"><i class="fa fa-flickr"></i></a>
			<?php } ?>
			<?php if($foursquare != '') { ?>
				<a href="<?php echo $foursquare; ?>"><i class="fa fa-foursquare"></i></a>
			<?php } ?>
			<?php if($github != '') { ?>
				<a href="<?php echo $github; ?>"><i class="fa fa-github"></i></a>
			<?php } ?>
			<?php if($google_plus != '') { ?>
				<a href="<?php echo $google_plus; ?>"><i class="fa fa-google-plus"></i></a>
			<?php } ?>
			<?php if($html5 != '') { ?>
				<a href="<?php echo $html5; ?>"><i class="fa fa-html5"></i></a>
			<?php } ?>
			<?php if($instagram != '') { ?>
				<a href="<?php echo $instagram; ?>"><i class="fa fa-instagram"></i></a>
			<?php } ?>
			<?php if($linkedin != '') { ?>
				<a href="<?php echo $linkedin; ?>"><i class="fa fa-linkedin"></i></a>
			<?php } ?>
			<?php if($renren != '') { ?>
				<a href="<?php echo $renren; ?>"><i class="fa fa-renren"></i></a>
			<?php } ?>
			<?php if($skype != '') { ?>
				<a href="<?php echo $skype; ?>"><i class="fa fa-skype"></i></a>
			<?php } ?>
			<?php if($stackexchange != '') { ?>
				<a href="<?php echo $stackexchange; ?>"><i class="fa fa-stack-exchange"></i></a>
			<?php } ?>
			<?php if($trello != '') { ?>
				<a href="<?php echo $trello; ?>"><i class="fa fa-trello"></i></a>
			<?php } ?>
			<?php if($tumblr != '') { ?>
				<a href="<?php echo $tumblr; ?>"><i class="fa fa-tumblr"></i></a>
			<?php } ?>
			<?php if($twitter != '') { ?>
				<a href="<?php echo $twitter; ?>"><i class="fa fa-twitter"></i></a>
			<?php } ?>
			<?php if($vk != '') { ?>
				<a href="<?php echo $vk; ?>"><i class="fa fa-vk"></i></a>
			<?php } ?>
			<?php if($weibo != '') { ?>
				<a href="<?php echo $weibo; ?>"><i class="fa fa-weibo"></i></a>
			<?php } ?>
			<?php if($windows != '') { ?>
				<a href="<?php echo $windows; ?>"><i class="fa fa-windows"></i></a>
			<?php } ?>
			<?php if($xing != '') { ?>
				<a href="<?php echo $xing; ?>"><i class="fa fa-xing"></i></a>
			<?php } ?>
			<?php if($youtube != '') { ?>
				<a href="<?php echo $youtube; ?>"><i class="fa fa-youtube"></i></a>
			<?php } ?>
		</div>
		<?php
		$output = ob_get_contents();
		ob_end_clean();
		return $output;
	}

	add_shortcode('vc_social_icons', 'vc_social_icons');
	/*
	function vc_theme_vc_single_image($atts, $content = null) {
		$output = $el_class = $image = $img_size = $img_link = $img_link_target = $img_link_large = $title = $css_animation = '';

		extract(shortcode_atts(array(
		    'title' => '',
		    'image' => $image,
		    'img_size'  => 'thumbnail',
		    'img_link_large' => false,
		    'img_link' => '',
		    'img_link_target' => '_self',
		    'el_class' => '',
		    'css_animation' => ''
		), $atts));

		$img_id = preg_replace('/[^\d]/', '', $image);
		$img = wpb_getImageBySize(array( 'attach_id' => $img_id, 'thumb_size' => $img_size ));
		if ( $img == NULL ) $img['thumbnail'] = '<img src="http://placekitten.com/g/400/300" /> <small>'.__('This is image placeholder, edit your page to replace it.', 'js_composer').'</small>';

		$a_class = ' class="popup-link"';
		if ( $el_class != '' ) {
		    $tmp_class = explode(" ", strtolower($el_class));
		    $tmp_class = str_replace(".", "", $tmp_class);
		    if ( in_array("prettyphoto", $tmp_class) ) {
		        wp_enqueue_script( 'prettyphoto' );
		        wp_enqueue_style( 'prettyphoto' );
		        $a_class = ' class="prettyphoto"';
		        $el_class = str_ireplace(" prettyphoto", "", $el_class);
		    }
		}

		$link_to = '';
		if ($img_link_large==true) {
		    $link_to = wp_get_attachment_image_src( $img_id, 'large');
		    $link_to = $link_to[0];
		}
		else if (!empty($img_link)) {
		    $link_to = $img_link;
		}
		if(!preg_match('/^(https?\:\/\/|\/\/)/', $link_to) && $link_to!='') $link_to = 'http://'.$link_to;
		$image_string = !empty($link_to) ? '<a'.$a_class.' href="'.$link_to.'"'.($img_link_target!='_self' ? ' target="'.$img_link_target.'"' : '').'>'.$img['thumbnail'].'</a>' : $img['thumbnail'];

		$css_class =  apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'wpb_single_image wpb_content_element'.$el_class, 'vc_single_image');
		

		$output .= "\n\t".'<div class="'.$css_class.'">';
		$output .= "\n\t\t".'<div class="wpb_wrapper">';
		$output .= "\n\t\t\t".wpb_widget_title(array('title' => $title, 'extraclass' => 'wpb_singleimage_heading'));
		$output .= "\n\t\t\t".$image_string;
		$output .= "\n\t\t".'</div> ';
		$output .= "\n\t".'</div> ';

		return $output;
	}
	*/
	function vc_back_to_top($atts, $content=null) {
		ob_start();
		?>
		<button class="btn-small bc-2 back-to-top back-tt"><i class="fa fa-angle-up"></i></button>
		<?php
		$output = ob_get_contents();
		ob_end_clean();
		return $output;
	}
	add_shortcode('vc_back_to_top', 'vc_back_to_top');

	function vc_address($atts, $content=null) {
		$output = $fa_icon = $entypo_icon = $icon_source = '';
		extract(shortcode_atts(array(
		    'icon' => '',
		    'icon_source' => 'fa',
		    'fa_icon' => '',
		    'entypo_icon' => ''
		), $atts));
		ob_start();
		?>
		<div class="footer-column">
			

			<?php
			if($icon_source == 'fa') 
				$icon = $fa_icon;
			else 
				$icon = $entypo_icon;
			?>
       		<i class="<?php echo $icon_source; ?> <?php echo $icon; ?>"></i>
        	<p><?php echo $content; ?></p>
      	</div>
		<?php
		$output = ob_get_contents();
		ob_end_clean();
		return $output;
	}

	add_shortcode('vc_address', 'vc_address');

	/*
	function vc_theme_vc_text_separator($atts, $content=null) {
		$output = $title = $el_class = '';
		extract(shortcode_atts(array(
		    'title' => '',
		    'el_class' => ''
		), $atts));

	
		$output .= '<div class="h2-container '.$style.' '.$el_class.'"> ';
		if($title != '') {
			$output .= '<h2>'.$title.'</h2>';
		} 

	    $output .= '
	          <hr class="small-line">
	        </div>';

		return $output;
	}
	*/

	vc_map( array(
	  "name" => __("Accordion", "js_composer"),
	  "base" => "vc_accordion",
	  "show_settings_on_create" => false,
	  "is_container" => true,
	  "icon" => "icon-wpb-ui-accordion",
	  "category" => __('Content', 'js_composer'),
	  "params" => array(
	    array(
	      "type" => "textfield",
	      "heading" => __("Widget title", "js_composer"),
	      "param_name" => "title",
	      "description" => __("What text use as a widget title. Leave blank if no title is needed.", "js_composer")
	    ),
	    array(
	      "type" => "textfield",
	      "heading" => __("Extra class name", "js_composer"),
	      "param_name" => "el_class",
	      "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "js_composer")
	    )
	  ),
	  "custom_markup" => '
	  <div class="wpb_accordion_holder wpb_holder clearfix vc_container_for_children">
	  %content%
	  </div>
	  <div class="tab_controls">
	  <button class="add_tab" title="'.__("Add accordion section", "js_composer").'">'.__("Add accordion section", "js_composer").'</button>
	  </div>
	  ',
	  'default_content' => '
	  [vc_accordion_tab title="'.__('Section 1', "js_composer").'"][/vc_accordion_tab]
	  [vc_accordion_tab title="'.__('Section 2', "js_composer").'"][/vc_accordion_tab]
	  ',
	  'js_view' => 'VcAccordionView'
	) );

	function list_display($atts, $content = null) {
		$output = '';
		$output = '<ul class="styled-list">'.do_shortcode($content).'</ul>';
		return $output;
	}
	add_shortcode('list', 'list_display');

	function list_item($atts, $content = null) {
		$output = $style = '';
		extract(shortcode_atts(array(
		    'style' => '1'
		), $atts));
		if($style == 1) $style = 'list-a';
		if($style == 2) $style = 'list-b';
		if($style == 3) $style = 'list-c';
		$output = '<li class="'.$style.'">';
		
		$output .= do_shortcode($content);
		$output .= '</li>';
		return $output;
	}
	add_shortcode('list_item', 'list_item');

	function fa_icon($atts) {
		$output = $name = '';
		extract(shortcode_atts(array(
		    'name' => ''
		), $atts));
		$output = '<i class="fa '.$name.'"></i>';
		return $output;
	}
	add_shortcode('icon', 'fa_icon');
	/*
	vc_map( array(
	  "name" => __("Row", "js_composer"),
	  "base" => "vc_row",
	  "is_container" => true,
	  "icon" => "icon-wpb-row",
	  "show_settings_on_create" => false,
	  "category" => __('Content', 'js_composer'),
	  "params" => array(
	    array(
	      "type" => "textfield",
	      "heading" => __("Extra class name", "js_composer"),
	      "param_name" => "el_class",
	      "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "js_composer")
	    ),
	    array(
	      "type" => "dropdown",
	      "heading" => __("Full width", "js_composer"),
	      "param_name" => "full_width",
	      "value" => array(__('No', "js_composer") => "false",__('Yes', "js_composer") => "true" ),
	      "description" => __("Do you want to display this row in full width?", "js_composer")
	    ),
	    array(
	      "type" => "dropdown",
	      "heading" => __("Style", "js_composer"),
	      "param_name" => "style",
	      "value" => array(__('Default', "js_composer") => "",__('Big unit box', "js_composer") => "big-unit-box" ),
	    ),
	  ),
	  "js_view" => 'VcRowView'
	) );
	*/

	function countdown($atts) {
		$output = $month = $day = $year = $hour = $minute = '';
		extract(shortcode_atts(array(
		    'month' => '',
		    'day' => '',
		    'year' => '',
		    'hour' => '',
		    'minute' => ''
		), $atts));

		$date = $month . ' ' . $day .', '. $year . ' '. $hour .
		   ':'. $minute . ':00';

		ob_start(); 
		?>
		<div class="counter-container">
			<div class="counter-box">
		       <!-- Countdown -->
		       <div class="countdown" data-date="<?php echo $date; ?>"></div>

		    </div> 
	    </div> 
		<?php
		$output = ob_get_contents();
		ob_end_clean();
		return $output;
	}

	add_shortcode('countdown', 'countdown');

	function event_timeline($atts) {
		$output = $icon1 = $icon2 = $icon3 = $final_icon = $title1 = $title2 = $title3 = '';
		$content1 = $content2 = $content3 = '';
		extract(shortcode_atts(array(
		    'icon1' => '',
		    'icon2' => '',
		    'icon3' => '',
		    'final_icon' => '',
		    'title1' => '',
		    'title2' => '',
		    'title3' => '',
		    'content1' => '',
		    'content2' => '',
		    'content3' => ''
		), $atts));
		ob_start(); 
		?>
		<div class="event">
		<?php
		if ($icon1 != 'none') {
		?>
			<div class="row">   
		        <div class="col-md-6 col-md-offset-6 col-sm-6 col-sm-offset-6 col-xs-6 col-xs-offset-6 e-right"> 
		          <!-- Title -->  
		          <h4><?php echo $title1; ?></h4>
		          <!-- Icon --> 
		          <i class="fa <?php echo $icon1; ?>"></i>
		        </div>                  
		    </div>         

	      	<div class="row info-row">   
	        	<div class="col-md-6 col-md-offset-6 col-sm-6 col-sm-offset-6 col-xs-6 col-xs-offset-6 content-right"> 
		          <!-- Content -->
		          <p>   
		            <?php echo $content1; ?> 
		          </p>
		          <!-- Content End -->
		       </div>
	      	</div>  
	        <!-- Event Item Right End -->
	    <?php
		}
		if ($icon2 != 'none') {
	    ?>
	      <!-- Event Item Left -->
	      <div class="row">   
	        <div class="col-md-6 col-sm-6 col-xs-6 e-left"> 
	          <!-- Title -->  
	          <h4><?php echo $title2; ?></h4>
	          <!-- Icon --> 
	          <i class="fa <?php echo $icon2; ?>"></i>
	        </div>                  
	      </div>         
	      
	      <div class="row info-row">   
	        <div class="col-md-5 col-md-offset-1 col-sm-5 col-sm-offset-1 col-xs-5 col-xs-offset-1 content-left">
	          <!-- Content -->
	          <p>
	            <?php echo $content2; ?> 
	          </p>
	          <!-- Content End -->
	        </div>
	      </div>  
	      <!-- Event Item Left End -->
	    <?php
		}
		if ($icon3 != 'none') {
		?>
	      <!-- Event Item Right -->
	      <div class="row">   
	        <div class="col-md-6 col-md-offset-6 col-sm-6 col-sm-offset-6 col-xs-6 col-xs-offset-6 e-right"> 
	          <!-- Title -->  
	          <h4><?php echo $title3; ?></h4>
	          <!-- Icon --> 
	          <i class="fa <?php echo $icon3; ?>"></i>
	        </div>                  
	      </div>         
	      
	      <div class="row info-row">   
	        <div class="col-md-6 col-md-offset-6 col-sm-6 col-sm-offset-6 col-xs-6 col-xs-offset-6 content-right"> 
	          <!-- Content -->
	          <p>   
	            <?php echo $content3; ?>
	          </p>
	          <!-- Content End -->
	        </div>
	      </div>  
	      <!-- Event Item Right End -->
	    <?php
		}
		if ($final_icon != 'none') {
		?>
	      <div class="row">   
	        <div class="col-md-12">
	          <div class="circle">
	            <i class="fa <?php echo $final_icon; ?>"></i>
	          </div>
	        </div>
	      </div>      
		<?php
		}
		?>
		</div>
		<?php
		$output = ob_get_contents();
		ob_end_clean();
		return $output;
	}

	add_shortcode('event_timeline', 'event_timeline');

	function blog_display($atts) {
		$output = '';
		ob_start(); 
		?>
		<div class="row blog-container">  
        	<div class="col-md-8">
        		<?php
        		$loop = new WP_Query( array( 'post_type' => 'post', 'posts_per_page' => -1 ) );
				while ( $loop->have_posts() ) : $loop->the_post(); 
					$id_post = $loop->post->ID;
					global $more;
					$more = 0;
					?>
					<div class="article">
					<h3 class="dark"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
					<ul class="infoline upper">
		              <li><span class="gray"><?php _e('By', 'ovni'); ?></span> <?php echo get_the_author(); ?></li> 
		              <li><span class="gray"><?php _e('Published', 'ovni'); ?></span> <?php echo get_the_date(); ?></li> 
		              <li><span class="gray"><?php _e('Tagged', 'ovni'); ?></span> <?php the_tags('',', ',''); ?></li>
		            </ul>
		           
		            <?php if(get_post_format($id_post) == 'video') { ?>
		            	<?php echo ovni_video(); ?>
		           	<?php } else { ?>
						<a href="<?php the_permalink(); ?>" class="thumb"><?php the_post_thumbnail('listing'); ?></a>
					<?php } ?>
					<div class="entry-content">
					<div class="space25"></div>
						<?php 
				    	the_content( 'Continue reading...' );
				    	?>
					</div>
				    <div class="space30"></div>
				    </div> 
				    <?php
				endwhile;
				wp_reset_query();
        		?>
        	</div>
        	<div class="col-md-4">
        		<div class="blog-right-sidebar">   
        			<?php get_sidebar(); ?>
        		</div>
        	</div>
        </div>
		<?php
		$output = ob_get_contents();
		ob_end_clean();
		return $output;
	}
	add_shortcode('blog_shortcode', 'blog_display');

	function section_title($atts) {
		$output = $title = $style = '';
		extract(shortcode_atts(array(
		    'title' => '',
		    'style' => ''
		), $atts));
		ob_start(); 
		?>
		<h2 class="<?php echo $style; ?>"><?php echo $title; ?></h2>
		<?php
		$output = ob_get_contents();
		ob_end_clean();
		return $output;
	}
	add_shortcode('section_title', 'section_title');

	add_action('admin_init','shortcodes_maps_init');

	function shortcodes_maps_init() {

		global $all_categories;
		global $animate_css;
		global $team_members;
		global $font_awesome_icons;
		global $entypo_icons;
		global $team_members;
		global $animate_css;
		global $font_awesome_icons;
		global $entypo_icons;
		global $all_categories;
		global $days;
		global $years;
		global $hours;
		global $minutes;
		
		vc_add_param('vc_row',array(
		      "type" => "dropdown",
		      "heading" => __("Full width", "js_composer"),
		      "param_name" => "full_width",
		      "value" => array(__('No', "js_composer") => "false",__('Yes', "js_composer") => "true" ),
		      "description" => __("Do you want to display this row in full width?", "js_composer")
		    )
		);

		vc_add_param('vc_row',array(
		      "type" => "textfield",
		      "heading" => __("ID", "js_composer"),
		      "param_name" => "id",
		      "description" => __("Enter unique ID of the section. You will use this ID to add menu item.", "js_composer")
		    )
		);
		
		vc_remove_element("vc_teaser_grid");
		vc_remove_element("vc_posts_grid");

		vc_remove_element("vc_pie");
		vc_remove_element("vc_tour");
		vc_remove_element("vc_cta_button");
		vc_remove_element("vc_carousel");
		vc_remove_element("vc_text_separator");
		
		/*
		vc_map( array(
		  "name" => __("Text Block", "js_composer"),
		  "base" => "vc_column_text",
		  "icon" => "icon-wpb-layer-shape-text",
		  "wrapper_class" => "clearfix",
		  "category" => __('Content', 'js_composer'),
		  "params" => array(
		    array(
		      "type" => "textarea_html",
		      "holder" => "div",
		      "heading" => __("Text", "js_composer"),
		      "param_name" => "content",
		      "value" => __("<p>I am text block. Click edit button to change this text.</p>", "js_composer")
		    ),
		    
		    array(
		      "type" => "dropdown",
		      "heading" => __("CSS Animation", "js_composer"),
		      "param_name" => "animation",
		      "value" => $animate_css   
		    ),
		    
		    array(
		      "type" => "textfield",
		      "heading" => __("Extra class name", "js_composer"),
		      "param_name" => "el_class",
		      "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "js_composer")
		    ),
		  )
		) );
		*/
		vc_map( array(
		  "name" => __("Section title", "js_composer"),
		  "base" => "section_title",
		  "icon" => "icon-wpb-row",
		  "show_settings_on_create" => true,
		  "category" => __('Content', 'js_composer'),
		  "params" => array(
		    array(
		      "type" => "textfield",
		      "heading" => __("Title", "js_composer"),
		      "param_name" => "title",
		      "value" => __("Title", "js_composer"),
		      "description" => __("Title of the section.", "js_composer")
		    ),
		  	array(
		      "type" => "dropdown",
		      "heading" => __("Style", "js_composer"),
		      "param_name" => "style",
		      "value" => array(__('Dark', "js_composer") => "section-title", __('White', "js_composer") => "section-title-white", __('Gray', "js_composer") => "section-title-gray" ),
		      "description" => __("Button size.", "js_composer")
		    ),
		  ),
		) );
		

		/* Button
		---------------------------------------------------------- */

		vc_map( array(
		  "name" => __("Button", "js_composer"),
		  "base" => "vc_button",
		  "icon" => "icon-wpb-ui-button",
		  "category" => __('Content', 'js_composer'),
		  "params" => array(
		    array(
		      "type" => "textfield",
		      "heading" => __("Text on the button", "js_composer"),
		      "holder" => "button",
		      "class" => "wpb_button",
		      "param_name" => "title",
		      "value" => __("Text on the button", "js_composer"),
		      "description" => __("Text on the button.", "js_composer")
		    ),
		    array(
		      "type" => "textfield",
		      "heading" => __("URL (Link)", "js_composer"),
		      "param_name" => "href",
		      "description" => __("Button link.", "js_composer")
		    ),
		    array(
		      "type" => "dropdown",
		      "heading" => __("Icon", "js_composer"),
		      "param_name" => "icon",
		      "value" => $font_awesome_icons,
		      "description" => __("Button icon.", "js_composer")
		    ),
		    array(
		      "type" => "dropdown",
		      "heading" => __("Size", "js_composer"),
		      "param_name" => "size",
		      "value" => array(__('Default', "js_composer") => "", __('Large', "js_composer") => "btn-lg", __('Small', "js_composer") => "btn-small", __('Mini', "js_composer") => "btn-mini"),
		      "description" => __("Button size.", "js_composer")
		    ),
		    array(
		      "type" => "textfield",
		      "heading" => __("Extra class name", "js_composer"),
		      "param_name" => "el_class",
		      "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "js_composer")
		    ),
		    array(
		      "type" => "dropdown",
		      "heading" => __("Style", "js_composer"),
		      "param_name" => "style",
		      "value" => array(__('Default', "js_composer") => "hero-unit-button", __('Primary', "js_composer") => "btn-primary" )
		    ),
		  ),
		  "js_view" => 'VcButtonView'
		) );

		vc_map( array(
		  "name" => __("Icon box", "js_composer"),
		  "base" => "vc_icon_box",
		  "show_settings_on_create" => true,
		  "category" => __('Content', 'js_composer'),
		  "params" => array(
		   
		  	array(
		      "type" => "dropdown",
		      "heading" => __("Icon", "js_composer"),
		      "param_name" => "fa_icon",
		      "value" => $font_awesome_icons,
		   
		    ),
		    array(
		      "type" => "dropdown",
		      "heading" => __("Style", "js_composer"),
		      "param_name" => "style",
		      "value" => array(__('Icon on top', "js_composer") => "style1", __('Icon on left', "js_composer") => "style2" )
		    ),
		    
		    array(
		      "type" => "textfield",
		      "heading" => __("Heading", "js_composer"),
		      "param_name" => "heading"
		    ),
		    array(
		      "type" => "textfield",
		      "heading" => __("Link", "js_composer"),
		      "param_name" => "link"
		    ),
		    array(
		      "type" => "textarea_html",
		      "holder" => "div",
		      "heading" => __("Text", "js_composer"),
		      "param_name" => "content",
		      "value" => __("<p>I am text block. Click edit button to change this text.</p>", "js_composer")
		    ),
		  ),
		  "js_view" => 'VcRowView'
		) );

		vc_map( array(
		  "name" => __("Spacer", "js_composer"),
		  "base" => "vc_spacer",
		  "show_settings_on_create" => true,
		  "category" => __('Content', 'js_composer'),
		  "params" => array(
		    array(
		      "type" => "dropdown",
		      "heading" => __("Height", "js_composer"),
		      "param_name" => "height",
		      "value" => array('20px' => '20', '1px' => '1','5px' => '5','10px' => '10','15px' => '15', '25px' => '25','30px' => '30',
		      	'35px' => '35', '40px' => '40', '45px' => '45', '50px' => '50','55px' => '55','60px' => '60', '65px' => '65', '70px' => '70',
		      	'75px' => '75', '80px' => '80', '85px' => '85', '90px' => '90', '95px' => '95', '100px' => '100', '125px' => '125'),
		      "description" => __("Choose icon", "js_composer")
		    )
		   
		  ),
		  "js_view" => 'VcRowView'
		) );

		vc_map( array(
		  "name" => __("Countdown", "js_composer"),
		  "base" => "countdown",
		  "show_settings_on_create" => true,
		  "icon" => "icon-wpb-row",
		  "category" => __('Content', 'js_composer'),
		  "params" => array(
		    array(
		      "type" => "dropdown",
		      "heading" => __("Month", "js_composer"),
		      "param_name" => "month",
		      "value" => array('January' => 'january', 'February' => 'february','March' => 'march','April' => 'april','May' => 'may', 'June' => 'june','July' => 'july',
		      	'August' => 'august', 'September' => 'september', 'October' => 'october', 'November' => 'november','December' => 'december'),
		      "description" => __("Choose month", "js_composer")
		    ),
		    array(
		      "type" => "dropdown",
		      "heading" => __("Day", "js_composer"),
		      "param_name" => "day",
		      "value" => $days,
		      "description" => __("Choose day", "js_composer")
		    ),
		    array(
		      "type" => "dropdown",
		      "heading" => __("Year", "js_composer"),
		      "param_name" => "year",
		      "value" => $years,
		      "description" => __("Choose year", "js_composer")
		    ),
		    array(
		      "type" => "dropdown",
		      "heading" => __("Hour", "js_composer"),
		      "param_name" => "hour",
		      "value" => $hours,
		      "description" => __("Choose hour", "js_composer")
		    ),
		    array(
		      "type" => "dropdown",
		      "heading" => __("Minute", "js_composer"),
		      "param_name" => "minute",
		      "value" => $minutes,
		      "description" => __("Choose minute", "js_composer")
		    )
		   
		  ),
		  "js_view" => 'VcRowView'
		) );

		vc_map( array(
		  "name" => __("Blog", "js_composer"),
		  "base" => "blog_shortcode",
		  "show_settings_on_create" => false,
		  "icon" => "icon-wpb-row",
		  "category" => __('Content', 'js_composer'),
		  "js_view" => 'VcRowView'
		) );

		vc_map( array(
		  "name" => __("Progress Bar", "js_composer"),
		  "base" => "vc_progress_bar",
		  "icon" => "icon-wpb-graph",
		  "category" => __('Content', 'js_composer'),
		  "params" => array(
		    array(
		      "type" => "textfield",
		      "heading" => __("Widget title", "js_composer"),
		      "param_name" => "title",
		      "description" => __("What text use as a widget title. Leave blank if no title is needed.", "js_composer")
		    ),
		    
		    array(
		      "type" => "exploded_textarea",
		      "heading" => __("Graphic values", "js_composer"),
		      "param_name" => "values",
		      "description" => __('Input graph values here. Divide values with linebreaks (Enter). Example: 90|Development', 'js_composer'),
		      "value" => "90|Development,80|Design,70|Marketing"
		    ),
		    array(
		      "type" => "textfield",
		      "heading" => __("Units", "js_composer"),
		      "param_name" => "units",
		      "description" => __("Enter measurement units (if needed) Eg. %, px, points, etc. Graph value and unit will be appended to the graph title.", "js_composer")
		    ),

		    array(
		      "type" => "textfield",
		      "heading" => __("Extra class name", "js_composer"),
		      "param_name" => "el_class",
		      "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "js_composer")
		    )
		  )
		) );

		vc_map( array(
		  "name" => 'WP ' . __("Recent Posts", 'ovni'),
		  "base" => "vc_wp_posts",
		  "icon" => "icon-wpb-wp",
		  "category" => __("WordPress Widgets", "js_composer"),
		  "class" => "wpb_vc_wp_widget",
		  "params" => array(
		    array(
		      "type" => "textfield",
		      "heading" => __("Widget title", "js_composer"),
		      "param_name" => "title",
		      "description" => __("What text use as a widget title. Leave blank to use default widget title.", "js_composer")
		    ),
		    array(
		      "type" => "dropdown",
		      "heading" => __("Style", "js_composer"),
		      "param_name" => "style",
		      "value" => array(__('Carousel', 'js_composer') => 'carousel', __('List', 'js_composer') => 'list' )
		    ),
		    array(
		      "type" => "textfield",
		      "heading" => __("Number of posts to show", "js_composer"),
		      "param_name" => "number",
		      "admin_label" => true
		    ),
		    array(
		      "type" => "textfield",
		      "heading" => __("Extra class name", "js_composer"),
		      "param_name" => "el_class",
		      "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "js_composer")
		    )
		  )
		) );

		

		vc_map( array(
		  "name" => __("Testimonial", "js_composer"),
		  "base" => "vc_testimonial",
		  "icon" => "icon-wpb-graph",
		  "category" => __('Content', 'js_composer'),
		  "params" => array(
		    array(
		      "type" => "dropdown",
		      "heading" => __("Style", "js_composer"),
		      "param_name" => "type",
		      "value" => array(__('With image', 'js_composer') => 'type1', __('Without image', 'js_composer') => 'type2' )
		    ),
		    array(
		      "type" => "textfield",
		      "heading" => __("Name", "js_composer"),
		      "param_name" => "name",
		      "description" => __("Enter name of the customer", "js_composer")
		    ),
		    array(
		      "type" => "textfield",
		      "heading" => __("Job", "js_composer"),
		      "param_name" => "job",
		      "description" => __("Enter job of the customer", "js_composer")
		    ),
		    array(
		      "type" => "textarea_html",
		      "holder" => "div",
		      "heading" => __("Text", "js_composer"),
		      "param_name" => "content",
		      "value" => __("<p>I am text block. Click edit button to change this text.</p>", "js_composer")
		    ),
		    array(
		      "type" => "attach_image",
		      "heading" => __("Image", "js_composer"),
		      "param_name" => "image",
		      "dependency" => Array('element' => 'type', 'value' => array('type1')),
		      "value" => "",
		      "description" => __("Select image from media library.", "js_composer")
		    ),
		    array(
		      "type" => "dropdown",
		      "heading" => __("CSS Animation", "js_composer"),
		      "param_name" => "animation",
		      "value" => $animate_css   
		    ),
		   
		    array(
		      "type" => "textfield",
		      "heading" => __("Extra class name", "js_composer"),
		      "param_name" => "el_class",
		      "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "js_composer")
		    )
		  )
		) );

		vc_map( array(
		  "name" => __("Posts Slider", "js_composer"),
		  "base" => "vc_posts_slider",
		  "icon" => "icon-wpb-slideshow",
		  "category" => __('Content', 'js_composer'),
		  "params" => array(
		    array(
		      "type" => "dropdown",
		      "heading" => __("Slider type", "js_composer"),
		      "param_name" => "type",
		      "admin_label" => true,
		      "value" => array(__("Flexslider", "js_composer") => "flexslider", __("Sly slider", "js_composer") => "slyslider", __("Refine slider", "js_composer") => "refineslider"),
		      "description" => __("Select slider type.", "js_composer")
		    ),
		    array(
		      "type" => "dropdown",
		      "heading" => __("Layout", "js_composer"),
		      "param_name" => "layout",
		      "admin_label" => false,
		      "dependency" => Array('element' => "type", 'value' => array('flexslider')),
		      "value" => array(__("Normal", "js_composer") => "normal", __("With thumbnails", "js_composer") => "thumbnails", __("Text", "js_composer") => "text", __("Laptop", "js_composer") => "laptop"),
		      "description" => __("Layout of slider", "js_composer")
		    ),
		    array(
		      "type" => "attach_image",
		      "heading" => __("Parallax image", "js_composer"),
		      "param_name" => "image",
		      "admin_label" => true,
		      "dependency" => Array('element' => "type", 'value' => array('parallaxslider')),
		      "value" => "",
		      "description" => __("Select image from media library.", "js_composer")
		    ),
		    array(
		      "type" => "dropdown",
		      "heading" => __("Transition", "js_composer"),
		      "param_name" => "transition",
		      "admin_label" => true,
		      "dependency" => Array('element' => "type", 'value' => array('flexslider', 'parallaxslider')),
		      "value" => array(__("Fade", "js_composer") => "fade",__("Slide", "js_composer") => "slide" ),
		      "description" => __("Select transition type.", "js_composer")
		    ),
		    array(
		      "type" => "dropdown",
		      "heading" => __("Image size", "js_composer"),
		      "param_name" => "image_size",
		      "dependency" => Array('element' => "type", 'value' => array('flexslider')),
		      "value" => array(__("Slider (wide)", "js_composer") => "slider",__("Slider (medium)", "js_composer") => "slider-medium", __("Slider (small)", "js_composer") => "slider-small" ),
		      "description" => __("Select image size.", "js_composer")
		    ),
		    
		    array(
		      "type" => "textfield",
		      "heading" => __("Slides count", "js_composer"),
		      "param_name" => "count",
		      "description" => __('How many slides to show? Enter number or word "All".', "js_composer")
		    ),
		    array(
		      "type" => "dropdown",
		      "heading" => __("Auto rotate slides", "js_composer"),
		      "param_name" => "interval",
		      "dependency" => Array('element' => "type", 'value' => array('flexslider', 'parallaxslider')),
		      "value" => array(3000, 5000, 10000, 15000, __("Disable", "js_composer") => 0),
		      "description" => __("Auto rotate slides each X milliseconds.", "js_composer")
		    ),
		    /*
		    array(
		      "type" => "exploded_textarea",
		      "heading" => __("Categories", "js_composer"),
		      "param_name" => "categories",
		      "description" => __("If you want to narrow output, enter category slugs here. Note: Only listed categories will be included. Divide categories with commas (,).", "js_composer")
		    ),
		    */
			array(
		      "type" => "dropdown",
		      "heading" => __("Category", "js_composer"),
		      "param_name" => "categories",
		      "value" => $all_categories,
		      "description" => __("Select category of posts you want to display", "js_composer")
		    ),
		    array(
		      "type" => "dropdown",
		      "heading" => __("Order by", "js_composer"),
		      "param_name" => "orderby",
		      "value" => array( "", __("Date", "js_composer") => "date", __("ID", "js_composer") => "ID", __("Author", "js_composer") => "author", __("Title", "js_composer") => "title", __("Modified", "js_composer") => "modified", __("Random", "js_composer") => "rand", __("Comment count", "js_composer") => "comment_count", __("Menu order", "js_composer") => "menu_order" ),
		      "description" => sprintf(__('Select how to sort retrieved posts. More at %s.', 'js_composer'), '<a href="http://codex.wordpress.org/Class_Reference/WP_Query#Order_.26_Orderby_Parameters" target="_blank">WordPress codex page</a>')
		    ),
		    array(
		      "type" => "dropdown",
		      "heading" => __("Order", "js_composer"),
		      "param_name" => "order",
		      "value" => array( __("Descending", "js_composer") => "DESC", __("Ascending", "js_composer") => "ASC" ),
		      "description" => sprintf(__('Designates the ascending or descending order. More at %s.', 'js_composer'), '<a href="http://codex.wordpress.org/Class_Reference/WP_Query#Order_.26_Orderby_Parameters" target="_blank">WordPress codex page</a>')
		    ),
		    array(
		      "type" => "textfield",
		      "heading" => __("Extra class name", "js_composer"),
		      "param_name" => "el_class",
		      "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "js_composer")
		    )
		  )
		) );
		
		
		vc_map( array(
		  "name" => __("Hero unit", "js_composer"),
		  "base" => "vc_hero_unit",
		  "icon" => "icon-wpb-row",
		  "show_settings_on_create" => true,
		  "category" => __('Content', 'js_composer'),
		  "params" => array(
		    array(
		      "type" => "dropdown",
		      "heading" => __("Icon source", "js_composer"),
		      "param_name" => "icon_source",
		      "value" => array(__('Font Awesome', 'reno') => 'fa',__('Entypo', 'reno') => 'entypo'),
		    ),
		  	array(
		      "type" => "dropdown",
		      "heading" => __("Icon", "js_composer"),
		      "param_name" => "fa_icon",
		      "value" => $font_awesome_icons,
		      "dependency" => Array('element' => 'icon_source', 'value' => array('fa')),
		    ),
		    array(
		      "type" => "dropdown",
		      "heading" => __("Icon", "js_composer"),
		      "param_name" => "entypo_icon",
		      "value" => $entypo_icons,
		      "dependency" => Array('element' => 'icon_source', 'value' => array('entypo')),
		    ),
		    array(
		      "type" => "textarea_html",
		      "holder" => "div",
		      "heading" => __("Text", "js_composer"),
		      "param_name" => "content",
		      "value" => __("<h4>Heading text</h4>", "js_composer")
		    ),
		    array(
		      "type" => "dropdown",
		      "heading" => __("Text align", "js_composer"),
		      "param_name" => "text_align",
		      "value" => array(__('Left', 'decoy') => 'align-left',__('Center', 'decoy') => 'align-center',__('Right', 'decoy') => 'align-right'),
		    ),
		    array(
				"type" => "colorpicker",
				"class" => "",
				"heading" => __("Background color", "decoy"),
				"param_name" => "color",
				"value" => "#54cde7",
				"description" => __("Give it a nice paint!", "smile"),				
			),
		    array(
		      "type" => "dropdown",
		      "heading" => __("CSS animation", "js_composer"),
		      "param_name" => "animation",
		      "value" => $animate_css,
		      "description" => __("Animation of left side.", "js_composer")
		    ),
		  ),
		) );
		vc_map( array(
		  "name" => __("Team member", "js_composer"),
		  "base" => "vc_team_member",
		  "icon" => "icon-wpb-row",
		  "show_settings_on_create" => true,
		  "category" => __('Content', 'js_composer'),
		  "params" => array(
		    array(
		      "type" => "dropdown",
		      "heading" => __("Team member", "js_composer"),
		      "param_name" => "team_member",
		      "admin_label" => true,
		      "value" => $team_members,
		      "description" => __("Select team member", "js_composer")
		    ),
		  ),
		) );

		vc_map( array(
		  "name" => __("Clients", "js_composer"),
		  "base" => "vc_clients",
		  "icon" => "icon-wpb-row",
		  "show_settings_on_create" => true,
		  "category" => __('Content', 'js_composer'),
		  "params" => array(
		    array(
		      "type" => "textfield",
		      "heading" => __("Widget title", "js_composer"),
		      "param_name" => "widget_title",
		    ),
		  ),
		) );
		vc_map( array(
		  "name" => __("Last portfolio posts", "js_composer"),
		  "base" => "vc_last_portfolio",
		  "icon" => "icon-wpb-row",
		  "show_settings_on_create" => true,
		  "category" => __('Content', 'js_composer'),
		  "params" => array(
		  	array(
		      "type" => "dropdown",
		      "heading" => __("Style", "js_composer"),
		      "param_name" => "style",
		      "value" => array(__('Style 1 - boxed', 'reno') => 'style1', __('Style 2 - slider', 'reno') => 'style2'),
		    ),
		    array(
		      "type" => "textfield",
		      "heading" => __("Number of posts", "js_composer"),
		      "param_name" => "count",
		    ),
		  ),
		) );
		vc_map( array(
		  "name" => __("Tab", "js_composer"),
		  "base" => "vc_tab",
		  "allowed_container_element" => 'vc_row',
		  "is_container" => true,
		  "content_element" => false,
		  "params" => array(
		    array(
		      "type" => "textfield",
		      "heading" => __("Title", "js_composer"),
		      "param_name" => "title",
		      "description" => __("Tab title.", "js_composer")
		    ),
		    array(
		      "type" => "tab_id",
		      "heading" => __("Tab ID", "js_composer"),
		      "param_name" => "tab_id"
		    ),
		  ),
		  'js_view' => 'VcTabView'
		) );

		vc_map( array(
		  "name" => __("Portfolio", "js_composer"),
		  "base" => "vc_portfolio_display",
		  "icon" => "icon-wpb-row",
		  "show_settings_on_create" => true,
		  "category" => __('Content', 'js_composer'),
		  "params" => array(
		  	 array(
		      "type" => "textfield",
		      "heading" => __("Category", "js_composer"),
		      "param_name" => "category",
		      "description" => __("Insert slug of portfolio category, you want to display. Leave blank for default category.", "js_composer")
		    ),
		  	array(
		      "type" => "dropdown",
		      "heading" => __("Show filter", "js_composer"),
		      "param_name" => "show_filter",
		      "value" => array(__('Yes', 'reno') => 'true', __('No', 'reno') => 'false'),
		    ),
		    array(
		      "type" => "dropdown",
		      "heading" => __("Filter align", "js_composer"),
		      "param_name" => "filter_align",
		      "value" => array(__('Left', 'reno') => 'align-left', __('Center', 'reno') => 'align-center', __('Right', 'reno') => 'align-right'),
		    ),
		    array(
		      "type" => "dropdown",
		      "heading" => __("Columns", "js_composer"),
		      "param_name" => "rows",
		      "value" => array(__('2 Columns', 'reno') => '2', __('3 Columns', 'reno') => '3', __('4 Columns', 'reno') => '4'),
		      "description" => __("Display portfolio in X columns", "js_composer")
		    ),
		    
		    array(
		      "type" => "dropdown",
		      "heading" => __("Isotope layout", "js_composer"),
		      "param_name" => "isotope_layout",
		      "value" => array(__('fitRows', 'reno') => 'fitRows',__('masonry', 'reno') => 'masonry',  __('straightDown', 'reno') => 'straightDown'),
		      "description" => __("", "js_composer")
		    ),
		  ),
		) );

		vc_map( array(
		  "name" => __("Social icons", "js_composer"),
		  "base" => "vc_social_icons",
		  "icon" => "icon-wpb-row",
		  "show_settings_on_create" => true,
		  "category" => __('Content', 'js_composer'),
		  "params" => array(
		  	array(
		      "type" => "textfield",
		      "heading" => __("Widget title", "js_composer"),
		      "param_name" => "title",
		    ),
		    array(
		      "type" => "dropdown",
		      "heading" => __("Style", "js_composer"),
		      "param_name" => "style",
		      "value" => array(__('Style 1', 'reno') => 'social', __('Style 2', 'reno') => 'social-2', __('Style 3', 'reno') => 'social-3', __('Style 4', 'reno') => 'social-4'),
		    ),
		  	array(
		      "type" => "textfield",
		      "heading" => __("Adn", "js_composer"),
		      "param_name" => "adn",
		    ),
		    array(
		      "type" => "textfield",
		      "heading" => __("Android", "js_composer"),
		      "param_name" => "android",
		    ),
		    array(
		      "type" => "textfield",
		      "heading" => __("Apple", "js_composer"),
		      "param_name" => "apple",
		    ),
		    array(
		      "type" => "textfield",
		      "heading" => __("Bitbucket", "js_composer"),
		      "param_name" => "bitbucket",
		    ),
		    array(
		      "type" => "textfield",
		      "heading" => __("Css3", "js_composer"),
		      "param_name" => "css3",
		    ),
		    array(
		      "type" => "textfield",
		      "heading" => __("Dribbble", "js_composer"),
		      "param_name" => "dribbble",
		    ),
		    array(
		      "type" => "textfield",
		      "heading" => __("Dropbox", "js_composer"),
		      "param_name" => "dropbox",
		    ),
		    array(
		      "type" => "textfield",
		      "heading" => __("Facebook", "js_composer"),
		      "param_name" => "facebook",
		    ),
		    array(
		      "type" => "textfield",
		      "heading" => __("Flickr", "js_composer"),
		      "param_name" => "flickr",
		    ),
		    array(
		      "type" => "textfield",
		      "heading" => __("Foursquare", "js_composer"),
		      "param_name" => "foursquare",
		    ),
		    array(
		      "type" => "textfield",
		      "heading" => __("Github", "js_composer"),
		      "param_name" => "github",
		    ),
		    array(
		      "type" => "textfield",
		      "heading" => __("Google plus", "js_composer"),
		      "param_name" => "google_plus",
		    ),
		    array(
		      "type" => "textfield",
		      "heading" => __("html5", "js_composer"),
		      "param_name" => "html5",
		    ),
		    array(
		      "type" => "textfield",
		      "heading" => __("Instagram", "js_composer"),
		      "param_name" => "instagram",
		    ),
		    array(
		      "type" => "textfield",
		      "heading" => __("Linkedin", "js_composer"),
		      "param_name" => "linkedin",
		    ),
		    array(
		      "type" => "textfield",
		      "heading" => __("Renren", "js_composer"),
		      "param_name" => "renren",
		    ),
		    array(
		      "type" => "textfield",
		      "heading" => __("Skype", "js_composer"),
		      "param_name" => "skype",
		    ),
		    array(
		      "type" => "textfield",
		      "heading" => __("Stackexchange", "js_composer"),
		      "param_name" => "stackexchange",
		    ),
		    array(
		      "type" => "textfield",
		      "heading" => __("Trello", "js_composer"),
		      "param_name" => "trello",
		    ),
		    array(
		      "type" => "textfield",
		      "heading" => __("Tumblr", "js_composer"),
		      "param_name" => "tumblr",
		    ),
		    array(
		      "type" => "textfield",
		      "heading" => __("Twitter", "js_composer"),
		      "param_name" => "twitter",
		    ),
		    array(
		      "type" => "textfield",
		      "heading" => __("Vk", "js_composer"),
		      "param_name" => "vk",
		    ),
		    array(
		      "type" => "textfield",
		      "heading" => __("Weibo", "js_composer"),
		      "param_name" => "weibo",
		    ),
		    array(
		      "type" => "textfield",
		      "heading" => __("Windows", "js_composer"),
		      "param_name" => "windows",
		    ),
		    array(
		      "type" => "textfield",
		      "heading" => __("Xing", "js_composer"),
		      "param_name" => "xing",
		    ),
		    array(
		      "type" => "textfield",
		      "heading" => __("Youtube", "js_composer"),
		      "param_name" => "youtube",
		    ),
		  ),
		) );

		vc_map( array(
		  "name" => __("Pricing table", "js_composer"),
		  "base" => "vc_pricing_table",
		  "icon" => "icon-wpb-row",
		  "show_settings_on_create" => true,
		  "category" => __('Content', 'js_composer'),
		  "params" => array(
		  	array(
		      "type" => "textfield",
		      "heading" => __("Plan title", "js_composer"),
		      "param_name" => "plan_title",
		    ),
		    array(
		      "type" => "textfield",
		      "heading" => __("Plan price", "js_composer"),
		      "param_name" => "plan_price",
		    ),
		    array(
		      "type" => "textfield",
		      "heading" => __("Price text", "js_composer"),
		      "param_name" => "price_text",
		    ),
		    array(
		      "type" => "textfield",
		      "heading" => __("Footer text", "js_composer"),
		      "param_name" => "footer_text",
		    ),
		    array(
		      "type" => "textfield",
		      "heading" => __("Row 1", "js_composer"),
		      "param_name" => "row1",
		    ),
		    array(
		      "type" => 'checkbox',
		      "heading" => __("Show row 2", "js_composer"),
		      "param_name" => "row2_enable",
		      "description" => __("Do you want to display 2nd row?", "js_composer"),
		      "value" => Array(__("Yes, please", "js_composer") => 'yes')
		    ),
		    array(
		      "type" => "textfield",
		      "heading" => __("Row 2", "js_composer"),
		      "param_name" => "row2",
		      "dependency" => Array('element' => 'row2_enable', 'value' => array('yes')),
		    ),
		    array(
		      "type" => 'checkbox',
		      "heading" => __("Show row 3", "js_composer"),
		      "param_name" => "row3_enable",
		      "description" => __("Do you want to display 3rd row?", "js_composer"),
		      "value" => Array(__("Yes, please", "js_composer") => 'yes'),
		    ),
		    array(
		      "type" => "textfield",
		      "heading" => __("Row 3", "js_composer"),
		      "param_name" => "row3",
		      "dependency" => Array('element' => 'row3_enable', 'value' => array('yes')),
		    ),
		    array(
		      "type" => 'checkbox',
		      "heading" => __("Show row 4", "js_composer"),
		      "param_name" => "row4_enable",
		      "description" => __("Do you want to display 4th row?", "js_composer"),
		      "value" => Array(__("Yes, please", "js_composer") => 'yes'),
		    ),
		    array(
		      "type" => "textfield",
		      "heading" => __("Row 4", "js_composer"),
		      "param_name" => "row4",
		      "dependency" => Array('element' => 'row4_enable', 'value' => array('yes')),
		    ),
		    array(
		      "type" => 'checkbox',
		      "heading" => __("Show row 5", "js_composer"),
		      "param_name" => "row5_enable",
		      "description" => __("Do you want to display 5th row?", "js_composer"),
		      "value" => Array(__("Yes, please", "js_composer") => 'yes'),
		    ),
		    array(
		      "type" => "textfield",
		      "heading" => __("Row 5", "js_composer"),
		      "param_name" => "row5",
		      "dependency" => Array('element' => 'row5_enable', 'value' => array('yes')),
		    ),
		    array(
		      "type" => 'checkbox',
		      "heading" => __("Show row 6", "js_composer"),
		      "param_name" => "row6_enable",
		      "description" => __("Do you want to display 6th row?", "js_composer"),
		      "value" => Array(__("Yes, please", "js_composer") => 'yes'),
		    ),
		    array(
		      "type" => "textfield",
		      "heading" => __("Row 6", "js_composer"),
		      "param_name" => "row6",
		      "dependency" => Array('element' => 'row6_enable', 'value' => array('yes')),
		    ),
		    array(
		      "type" => 'checkbox',
		      "heading" => __("Show row 7", "js_composer"),
		      "param_name" => "row7_enable",
		      "description" => __("Do you want to display 7th row?", "js_composer"),
		      "value" => Array(__("Yes, please", "js_composer") => 'yes'),
		    ),
		    array(
		      "type" => "textfield",
		      "heading" => __("Row 7", "js_composer"),
		      "param_name" => "row7",
		      "dependency" => Array('element' => 'row7_enable', 'value' => array('yes')),
		    ),
		    array(
		      "type" => 'checkbox',
		      "heading" => __("Show row 8", "js_composer"),
		      "param_name" => "row8_enable",
		      "description" => __("Do you want to display 8th row?", "js_composer"),
		      "value" => Array(__("Yes, please", "js_composer") => 'yes'),
		    ),
		    array(
		      "type" => "textfield",
		      "heading" => __("Row 8", "js_composer"),
		      "param_name" => "row8",
		      "dependency" => Array('element' => 'row8_enable', 'value' => array('yes')),
		    ),
		    array(
		      "type" => 'checkbox',
		      "heading" => __("Show row 9", "js_composer"),
		      "param_name" => "row9_enable",
		      "description" => __("Do you want to display 9th row?", "js_composer"),
		      "value" => Array(__("Yes, please", "js_composer") => 'yes'),
		    ),
		    array(
		      "type" => "textfield",
		      "heading" => __("Row 9", "js_composer"),
		      "param_name" => "row9",
		      "dependency" => Array('element' => 'row9_enable', 'value' => array('yes')),
		    ),
		    array(
		      "type" => 'checkbox',
		      "heading" => __("Show row 10", "js_composer"),
		      "param_name" => "row10_enable",
		      "description" => __("Do you want to display 10th row?", "js_composer"),
		      "value" => Array(__("Yes, please", "js_composer") => 'yes'),
		    ),
		    array(
		      "type" => "textfield",
		      "heading" => __("Row 10", "js_composer"),
		      "param_name" => "row10",
		      "dependency" => Array('element' => 'row10_enable', 'value' => array('yes')),
		    ),
		  ),
		) );
		/*
		vc_map( array(
		  "name" => __("Single Image", "js_composer"),
		  "base" => "vc_single_image",
		  "icon" => "icon-wpb-single-image",
		  "category" => __('Content', 'js_composer'),
		  "params" => array(
		    array(
		      "type" => "textfield",
		      "heading" => __("Widget title", "js_composer"),
		      "param_name" => "title",
		      "description" => __("What text use as a widget title. Leave blank if no title is needed.", "js_composer")
		    ),
		    array(
		      "type" => "attach_image",
		      "heading" => __("Image", "js_composer"),
		      "param_name" => "image",
		      "value" => "",
		      "description" => __("Select image from media library.", "js_composer")
		    ),
		    $add_css_animation,
		    array(
		      "type" => "textfield",
		      "heading" => __("Image size", "js_composer"),
		      "param_name" => "img_size",
		      "description" => __("Enter image size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height). Leave empty to use 'thumbnail' size.", "js_composer")
		    ),
		    array(
		      "type" => 'checkbox',
		      "heading" => __("Link to large image?", "js_composer"),
		      "param_name" => "img_link_large",
		      "description" => __("If selected, image will be linked to the bigger image.", "js_composer"),
		      "value" => Array(__("Yes, please", "js_composer") => 'yes')
		    ),
		    array(
		      "type" => "textfield",
		      "heading" => __("Image link", "js_composer"),
		      "param_name" => "img_link",
		      "description" => __("Enter url if you want this image to have link.", "js_composer"),
		      "dependency" => Array('element' => "img_link_large", 'is_empty' => true, 'callback' => 'wpb_single_image_img_link_dependency_callback')
		    ),
		    array(
		      "type" => "dropdown",
		      "heading" => __("Link Target", "js_composer"),
		      "param_name" => "img_link_target",
		      "value" => $target_arr,
		      "dependency" => Array('element' => "img_link", 'not_empty' => true)
		    ),
		    array(
		      "type" => "textfield",
		      "heading" => __("Extra class name", "js_composer"),
		      "param_name" => "el_class",
		      "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "js_composer")
		    )
		  )
		));
		*/
		vc_map( array(
		  "name" => __("Back to top button", "js_composer"),
		  "base" => "vc_back_to_top",
		  "icon" => "icon-wpb-row",
		  "show_settings_on_create" => false,
		  "category" => __('Content', 'js_composer'),
		) );

		vc_map( array(
		  "name" => __("Address with icon", "js_composer"),
		  "base" => "vc_address",
		  "icon" => "icon-wpb-row",
		  "show_settings_on_create" => true,
		  "category" => __('Content', 'js_composer'),
		  "params" => array(
		  	array(
		      "type" => "dropdown",
		      "heading" => __("Icon source", "js_composer"),
		      "param_name" => "icon_source",
		      "value" => array(__('Font Awesome', 'reno') => 'fa',__('Entypo', 'reno') => 'entypo'),
		    ),
		  	array(
		      "type" => "dropdown",
		      "heading" => __("Icon", "js_composer"),
		      "param_name" => "fa_icon",
		      "value" => $font_awesome_icons,
		      "dependency" => Array('element' => 'icon_source', 'value' => array('fa')),
		    ),
		    array(
		      "type" => "dropdown",
		      "heading" => __("Icon", "js_composer"),
		      "param_name" => "entypo_icon",
		      "value" => $entypo_icons,
		      "dependency" => Array('element' => 'icon_source', 'value' => array('entypo')),
		    ),
		    array(
		      "type" => "textarea_html",
		      "holder" => "div",
		      "heading" => __("Text", "js_composer"),
		      "param_name" => "content",
		      "value" => __("<p>I am text block. Click edit button to change this text.</p>", "js_composer")
		    ),
		  )
		) );
		/*
		vc_map( array(
		  "name" => __("Separator with Text", "js_composer"),
		  "base" => "vc_text_separator",
		  "icon" => "icon-wpb-ui-separator-label",
		  "category" => __('Content', 'js_composer'),
		  "params" => array(
		    array(
		      "type" => "textfield",
		      "heading" => __("Title", "js_composer"),
		      "param_name" => "title",
		      "holder" => "div",
		      "value" => __("Title", "js_composer"),
		      "description" => __("Separator title.", "js_composer")
		    ),
		    array(
		      "type" => "textfield",
		      "heading" => __("Extra class name", "js_composer"),
		      "param_name" => "el_class",
		      "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "js_composer")
		    )
		  ),
		  "js_view" => 'VcTextSeparatorView'
		) );
		*/
	}

endif;


?>