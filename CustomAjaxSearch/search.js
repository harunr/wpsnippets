jQuery(document).ready( function() {
    
    // Search Button jQuery AJAX
    jQuery('input#custom-search-btn').click( function() {
        jQuery('#search-loader').show();
        jQuery('.search_box').hide();
        
        
        var search_text = jQuery("input#custom-search").val(); 
        jQuery.ajax({
            'type' : 'POST',
            'url' : SearchAjax.ajaxurl, 
            'data' : {
                'action' : "show_search_results",
                'search_text' : search_text
            },
            'success' : function(data) {
                jQuery("#custom-search-results").html(data);
                jQuery('#search-loader').hide();
                jQuery('.search_box').show();

            },
            error: function(errorThrown){
                alert(errorThrown);
            }
        }); // End AJAX parameters
            
        return false;
    });  // End of Search Button jQuery AJAX
    
    
    // Next Button AJAX
    jQuery(document).on('click', '#next-page-hrr', function() {
        jQuery('#search-loader').show();
        
        
        var search_text = jQuery("#hrr-search-text").val(); 
        var page_num = jQuery("#next-page-num-hrr").val(); 
        jQuery.ajax({
            'type' : 'POST',
            'url' : SearchAjax.ajaxurl, 
            'data' : {
                'action' : "show_search_results_next",
                'search_text' : search_text,
                'page_num' : page_num
            },
            'success' : function(data) {
                jQuery("#custom-search-results").html(data);
                jQuery('#search-loader').hide();
                jQuery('.search_box').show();

            }
        }); // End AJAX parameters
            
         return false;
    });  // End of Next Button AJAX
    
    
    
    // Previous Button AJAX
    jQuery(document).on('click', '#previous-page-hrr', function() {
                       
        var search_text = jQuery("#hrr-search-text").val(); 
        var page_num = jQuery("#previous-page-num-hrr").val(); 
        // alert(search_text + page_num); 
        
        jQuery('#search-loader').show();
        jQuery.ajax({
            'type' : 'POST',
            'url' : SearchAjax.ajaxurl, 
            'data' : {
                'action' : "show_search_results_previous",
                'search_text' : search_text,
                'page_num' : page_num
            },
            'success' : function(data) {
                jQuery("#custom-search-results").html(data);
                jQuery('#search-loader').hide();

            }
        }); // End AJAX parameters
            
         return false;
    });  // End of Next Button AJAX
    
    
});