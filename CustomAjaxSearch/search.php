<?php

function show_search_results() {
    if(isset($_POST['search_text'])){
        
        $search_text = $_POST['search_text'];
        echo '<div class="search_box">';
        if(!empty($search_text)){
            $search_query = new WP_Query(array(
                'post_type' => array( 'post', 'page' ),
                'posts_per_page' => 10,
                's' => $search_text 
            ));
            
            
            if($search_query->have_posts()) { 
                $total_results = $search_query->found_posts;
                $total_pages = $search_query->max_num_pages;
                // echo '<p class="total_posts">Total Results: '.$total_results.'. Total Pages: '.$total_pages.'</p>';
                while ($search_query->have_posts()) {
                    $search_query->the_post();
                    
                    echo '<div class="single_result">';
                    echo '<h4><a href="'.get_the_permalink().'">'.get_the_title().'</a></h4>';
                    echo '<p class="content">'.wp_trim_words( get_the_content(), 20, '' ) .'</p>';
                    echo '<a href="'.get_the_permalink().'" class="more">+ '.utf8_encode('Läs mer').'...</a>';
                    echo '</div>';
                    
                } // end while
                echo '<input type="hidden" value="'.$search_text.'" id="hrr-search-text"name="hrr_search_text"/>';
                if($total_results > 10){
                    echo '<input type="hidden" value="2" id="next-page-num-hrr" name="next_page_num_hrr"/>';
                    echo '<input type="button" value="'. utf8_encode('Nästa sida').'" name="next_page_hrr" id="next-page-hrr"/>';
                }
            } else {
                echo  '<p>No result found on <strong>'. $search_text.'</strong>. Please try another keyword</p>';
            } // endif
            
            
        } else {
            echo 'Please enter a Keyword!';
        } // endif
        
        echo '</div>';
    } // endif
die();
return true;
}


add_action('wp_ajax_show_search_results', 'show_search_results');
add_action('wp_ajax_nopriv_show_search_results', 'show_search_results');

// show_search_results_next

function show_search_results_next() {
 if(isset($_POST['search_text']) && isset($_POST['page_num'])){
        
        $search_text = $_POST['search_text'];
        $page_num = $_POST['page_num'];
        echo '<div class="search_box">';
        if(!empty($search_text)){
            $search_query = new WP_Query(array(
                'post_type' => array( 'post', 'page' ),
                'posts_per_page' => 10,
                'paged' => $page_num,
                's' => $search_text 
            ));
            
            
            if($search_query->have_posts()) { 
                $total_results = $search_query->found_posts;
                $total_pages = $search_query->max_num_pages;
               //  echo '<p class="total_posts">Total Results: '.$total_results.'. Total Pages: '.$total_pages.'</p>';
                while ($search_query->have_posts()) {
                    $search_query->the_post();
                    
                    echo '<div class="single_result">';
                    echo '<h4><a href="'.get_the_permalink().'">'.get_the_title().'</a></h4>';
                    echo '<p class="content">'.wp_trim_words( get_the_content(), 20, '' ) .'</p>';
                    echo '<a href="'.get_the_permalink().'" class="more">+ '.utf8_encode('Läs mer').'...</a>';
                    echo '</div>';
                    
                } // end while
                if($page_num != 1){
                    echo '<input type="hidden" value="'.($page_num - 1).'" id="previous-page-num-hrr" name="previous_page_num_hrr"/>';
                    echo '<input type="button" value="'. utf8_encode('Tillbaka').'" name="previous_page_hrr" id="previous-page-hrr"/>';
                }
                
                echo '<input type="hidden" value="'.$search_text.'" id="hrr-search-text" name="hrr_search_text"/>';
                if($page_num < $total_pages){
                    echo '<input type="hidden" value="'. ($page_num + 1).'"  id="next-page-num-hrr" name="next_page_num_hrr"/>';
                    echo '<input type="button" value="'. utf8_encode('Nästa sida').'" name="next_page_hrr" id="next-page-hrr"/>';
                }
            } else {
                echo  '<p>No result found on <strong>'. $search_text.'</strong>. Please try another keyword</p>';
            } // endif
            
            
        } else {
            echo 'Please enter a Keyword!';
        } // endif
        
        echo '</div>';
    } // endif
die();
return true;
}


add_action('wp_ajax_show_search_results_next', 'show_search_results_next');
add_action('wp_ajax_nopriv_show_search_results_next', 'show_search_results_next');


// show_search_results_previous

function show_search_results_previous() {
 if(isset($_POST['search_text']) && isset($_POST['page_num'])){
        
        $search_text = $_POST['search_text'];
        $page_num = $_POST['page_num'];
        echo '<div class="search_box">';
        if(!empty($search_text)){
            $search_query = new WP_Query(array(
                'post_type' => array( 'post', 'page' ),
                'posts_per_page' => 10,
                'paged' => $page_num,
                's' => $search_text 
            ));
            
            
            if($search_query->have_posts()) { 
                $total_results = $search_query->found_posts;
                $total_pages = $search_query->max_num_pages;
                // echo '<p class="total_posts">Total Results: '.$total_results.'. Total Pages: '.$total_pages.'</p>';
                while ($search_query->have_posts()) {
                    $search_query->the_post();
                    
                    echo '<div class="single_result">';
                    echo '<h4><a href="'.get_the_permalink().'">'.get_the_title().'</a></h4>';
                    echo '<p class="content">'.wp_trim_words( get_the_content(), 20, '' ) .'</p>';
                    echo '<a href="'.get_the_permalink().'" class="more">+ '.utf8_encode('Läs mer').'...</a>';
                    echo '</div>';
                    
                } // end while
                if($page_num != 1){
                    echo '<input type="hidden" value="'.($page_num - 1).'" id="previous-page-num-hrr" name="previous_page_num_hrr"/>';
                    echo '<input type="button" value="'. utf8_encode('Tillbaka').'" name="previous_page_hrr" id="previous-page-hrr"/>';
                }
                
                echo '<input type="hidden" id="hrr-search-text" value="'.$search_text.'" name="hrr_search_text"/>';
                if($page_num < $total_pages){
                    echo '<input type="hidden" value="'. ($page_num + 1).'"  id="next-page-num-hrr" name="next_page_num_hrr"/>';
                    echo '<input type="button" value="'. utf8_encode('Nästa sida').'" name="next_page_hrr" id="next-page-hrr"/>';
                }
            } else {
                echo  '<p>No result found on <strong>'. $search_text.'</strong>. Please try another keyword</p>';
            } // endif
            
            
        } else {
            echo 'Please enter a Keyword!';
        } // endif
        
        echo '</div>';
    } // endif
die();
return true;
}


add_action('wp_ajax_show_search_results_previous', 'show_search_results_previous');
add_action('wp_ajax_nopriv_show_search_results_previous', 'show_search_results_previous');