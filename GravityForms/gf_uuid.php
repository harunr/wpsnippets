<?php

/* -------------------------------------------------------------------------------------
  UUID for forms
  ----------------------------------------------------------------------------------- */
add_filter("gform_field_value_uuid", "get_unique");
function get_unique(){
	$num = 0;
    $prefix = intval( date("Ymd") ); // update the prefix here
	$uniques = sprintf("%04d", $num);
    $unique = $prefix . $uniques;
    do {
		$unique += 1;
    } while (!check_unique($unique));
    return $unique;
}
function check_unique($unique) {
    global $wpdb;

	
	$table = $wpdb->prefix . 'rg_lead_detail';
    $form_id2 = 2; // update to the form ID your unique id field belongs to
    $field_id2 = 93; // update to the field ID your unique id is being prepopulated in
    $result2 = $wpdb->get_var("SELECT value FROM $table WHERE form_id = '$form_id2' AND field_number = '$field_id2' AND value = '$unique'");

    if(empty($result2)){
		return true;
	} else {
		return false;
	}  
    
}