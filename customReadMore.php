<?php
// Custom ReadMore function for WordPress

function hrrReadMore($wordCount){
    $post_content = explode(" ", get_the_content());
    $slice_word = array_slice($post_content, 0, $wordCount);
    $get_sliced_content = implode(" ", $slice_word);

    return $get_sliced_content;
}



