<?php

global $wpdb;
$table_name = $wpdb->prefix.'TABLE-NAME-HERE';
if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
     //table not in database. Create new table
     $charset_collate = $wpdb->get_charset_collate();
 
     $sql = "CREATE TABLE $table_name (
          id mediumint(9) NOT NULL AUTO_INCREMENT,
          field_x text NOT NULL,
          field_y text NOT NULL,
          UNIQUE KEY id (id)
     ) $charset_collate;";
     require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
     dbDelta( $sql );
}
else{
}


/*  
 * 
 * In plugin activation Hooks
 * 
 * 
 */

//action hook for plugin activation
register_activation_hook( __FILE__, 'callback_plugin' );
//callback function
function callback_plugin(){
    global $wpdb;
    $table_name = $wpdb->prefix . "your-table-name";
    if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
        $sql = "CREATE TABLE $table_name (
                id int NOT NULL AUTO_INCREMENT,
                name tinytext NOT NULL
                );";
        //reference to upgrade.php file
        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $sql );
    }
}


