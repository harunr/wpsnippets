jQuery(document).ready(function($) {

	// Light box persguide 
	jQuery("a#persguide_opener").on('click', function() {
		jQuery('#persguide').addClass('open');
			return false;
	});
	jQuery("a#persguide-closer").on('click', function() {
		jQuery('#persguide').removeClass('open');
			return false;
		
	});
	jQuery('#persguide').on('click', function(e) {
		
      		jQuery('#persguide').removeClass('open');
		return false;
	});

	jQuery('#persguide .interface-personalization').click(function(event){
		    event.stopPropagation();
	}); //Light box persguide End



	// Light box Font 
	jQuery("a.embroideryfonts-opener").on('click', function() {
		jQuery('#embroideryfonts').addClass('open');
			return false;
	});
	jQuery("a#embroideryfonts-close").on('click', function() {
		jQuery('#embroideryfonts').removeClass('open');
			return false;
		
	});
	jQuery('#embroideryfonts').on('click', function(e) {
		
      		jQuery('#embroideryfonts').removeClass('open');
		return false;
	});

	jQuery('#embroideryfonts .interface-modal').click(function(event){
		    event.stopPropagation();
	}); //Light box Font End

	// Tabs
	jQuery('.interface-fonts .fonts h4').on('click', function(event) {
		event.preventDefault();
		jQuery(this).toggleClass('opened');
		jQuery(this).next('ul.content').toggle();
	}); // End Tabs



	jQuery(".gform_wrapper ul.gform_fields li.pro_single_latter .ginput_container input").attr('maxlength','1');
	jQuery(".gform_wrapper ul.gform_fields li.pro_single_latter .ginput_container input").on('input', function(event) {
		var single_value = jQuery(this).val().toUpperCase();
		jQuery(this).val(single_value);
	});

	jQuery(".gform_wrapper ul.gform_fields li.pro_female_latters .ginput_container input").attr('maxlength','1');
	jQuery(".gform_wrapper ul.gform_fields li.pro_female_latters .ginput_container input").on('input', function(event) {
		var single_value = jQuery(this).val().toUpperCase();
		jQuery(this).val(single_value);

	});

	jQuery(".gform_wrapper ul.gform_fields li.pro_female_latters .ginput_container input").keyup(function () {
	    if (this.value.length == this.maxLength) {
	      jQuery(this).parent('span').next('span').children('input').focus();
	    }
	});

	jQuery(".gform_wrapper ul.gform_fields li.pro_male_initials .ginput_container input").attr('maxlength','1');
	jQuery(".gform_wrapper ul.gform_fields li.pro_male_initials .ginput_container input").on('input', function(event) {
		var single_value = jQuery(this).val().toUpperCase();
		jQuery(this).val(single_value);
	});

	jQuery(".gform_wrapper ul.gform_fields li.pro_male_initials .ginput_container input").keyup(function () {
	    if (this.value.length == this.maxLength) {
	      jQuery(this).parent('span').next('span').children('input').focus();
	    }
	});

	jQuery(".gform_wrapper ul.gform_fields li.pro_full_name .ginput_container input").attr('maxlength','12');


	// Light box Color Swatches 
	jQuery("a.thread_color_open").on('click', function() {
		jQuery('#threadColor01').addClass('open');
			return false;
	});
	jQuery("a.thread_color_close").on('click', function() {
		jQuery('#threadColor01').removeClass('open');
			return false;
		
	});
	jQuery('#threadColor01').on('click', function(e) {
		
      		jQuery('#threadColor01').removeClass('open');
		return false;
	});

	jQuery('#threadColor01 .interface-thread-colors').click(function(event){
		    event.stopPropagation();
	}); //Light box Color Swatches End

});