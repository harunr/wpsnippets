<?php

/**
 *
 * => Template for Form 3 and 7 <=
 * 
 * Don't give direct access to the template
 */ 
if(!class_exists("RGForms")){
	return;
}

/**
 * Load the form data to pass to our PDF generating function 
 */
$form = RGFormsModel::get_form_meta($form_id);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>	

    <title>Contract</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <style type="text/css">
        body{
            font-family: Verdana, OpenSymbol;
            font-size: 12px;
        }
        p{
            font-size: 12px;
            margin-bottom: 5px;
        }
        .page{
            padding: 20px 15px;
        }
    </style>
</head>
	<body>
       
        <?php	

        foreach($lead_ids as $lead_id) {

            $lead = RGFormsModel::get_lead($lead_id);
            $form_data = GFPDFEntryDetail::lead_detail_grid_array($form, $lead);
			
			/*
			 * Add &data=1 when viewing the PDF via the admin area to view the $form_data array
			 */
			PDF_Common::view_data($form_data);				
						
			/*
			 * Store your form fields from the $form_data array into variables here
			 * To see your entire $form_data array, view your PDF via the admin area and add &data=1 to the url
			 * 
			 * For an example of accessing $form_data fields see https://developer.gravitypdf.com/documentation/custom-templates-introduction/
			 *
			 * Alternatively, as of v3.4.0 you can use merge tags (except {allfields}) in your templates. 
			 * Just add merge tags to your HTML and they'll be parsed before generating the PDF.	
			 * 		 
			 */				 					
							
			?>            
          
     <div class="page page_one">
        <p>{Uw Bedrijfsnaam:13} <br />
        T.a.v. { (Prefix):1.2} { (First):1.3} { (Middle):1.4} { (Last):1.6} <br />
        { (Street Address):3.1} { (Address Line 2):3.2}<br />
        { (ZIP / Postal Code):3.5} { (City):3.3}</p>
        
        <p>{ (City):3.3}, {date_dmy}</p>
        
        <p>OVEREENKOMST VAN AANNEMING</p>
        
        <p>A. Partijen <br />
        Ondergetekenden:</p>
        
        <p>1. <br />
        naam: {Bedrijfsnaam opdrachtgever / client:9}<br />
        straat/postbus: { (Street Address):4.1} { (Address Line 2):4.2}<br />
        postcode: { (ZIP / Postal Code):4.5}<br />
        plaats: { (City):4.3}<br />
        e-mailadres: {Email client:5}<br />
        KvK-nummer: {Kamer van Koophandel client:31}<br />
        BTW-nummer: {Uw BTW nummer:16}</p>
        
        <p>hierna te noemen: de opdrachtgever,<br />
        en</p>
        
        <p>2.<br />
        {Uw Bedrijfsnaam:13}<br />
        naam: { (First):1.3} { (Middle):1.4} { (Last):1.6} namens {Uw Bedrijfsnaam:13}<br />
        straat/postbus: { (Street Address):3.1} { (Address Line 2):3.2}<br />
        postcode: { (ZIP / Postal Code):3.5}<br />
        plaats: { (City):3.3}<br />
        KvK-nummer: {Kamer van Koophandel:30}<br />
        BTW-nummer: {Uw BTW nummer:15}</p>
        
        <p>hierna te noemen: de opdrachtnemer</p>
        
        <p>In aanmerking nemende dat:<br />
        - partijen met deze overeenkomst geen arbeidsovereenkomst beogen af te sluiten, maar een<br />
        overeenkomst van aanneming van werk.<br />
        - Partijen zullen de uitvoering van deze overeenkomst in overeenstemming laten zijn met de wettelijke<br />
        regels bij het zelfstandig uitvoeren van een overeenkomst van aanneming in de zin van artikel 7:750 BW.<br />
        - Deze overeenkomst heeft nadrukkelijk betrekking op het aannemen van werk.<br />
        - Partijen deze overeenkomst onder de navolgende voorwaarden en bepalingen wensen aan te gaan:</p>
        
        <p>verklaren het volgende te zijn overeengekomen:</p>
        
        <p>B. Het werk<br />
        Deze overeenkomst heeft betrekking op het volgende werk. De opdrachtgever heeft opgedragen aan de<br />
        opdrachtnemer, die deze opdracht heeft aanvaard, het volgende werk:</p>
        
        <p>{Omschrijving van het werk:19}</p>
        
        <p>De opdrachtnemer verplicht zich, in relatie tot de aanvaarde opdracht, het volgende resultaat op te<br />
        leveren:<br />
        Plaats van het werk: {Locatie van het werk:21}<br />
        Benaming van het werk: Project {Naam van het werk:20}</p>
        
        <p>De opdrachtgever is verplicht de opdrachtnemer in het bezit te stellen van de stukken welke betrekking<br />
        hebben op de uitvoering van de opdracht. Deze stukken maken deel uit van deze overeenkomst van<br />
        aanneming.<br />
        De opdrachtnemer verplicht zich tot nakoming van de opdracht door het werk uit te voeren naar de eisen<br />
        van goed en deugdelijk werk en naar de bepalingen van deze overeenkomst.</p>
    </div>
    <pagebreak sheet-size="A4" />
    <div class="page page_two">
        <p>C. Onafhankelijkheid <br />
        > De opdrachtnemer voert de opdracht uit in volledige onafhankelijkheid en bepaalt zelf onder welke<br />
        omstandigheden en op welke wijze de opdracht wordt uitgevoerd<br />
        > Opdrachtnemer is vrij in het indelen van zijn werkzaamheden binnen de context van de opdracht en<br />
        de overeengekomen resultaatsverplichting.<br />
        > Opdrachtgever kan de opdrachtnemer richtlijnen geven over de afstemming van het overeengekomen<br />
        werk in relatie tot andere werkzaamheden die hiermede in relatie staan, zonder in te grijpen in de wijze<br />
        van uitvoering van de overeenkomst, aangezien de opdrachtnemer daarover bij uitsluiting beslist.<br />
        > De opdrachtnemer is volledig vrij in het aannemen van opdrachten van derden.<br />
        > De opdrachtnemer zal zodanig gekleed zijn tijdens de uitvoering van het werk, zodat voldoende<br />
        herkenbaar is dat hij zijn eigen bedrijf vertegenwoordigt.<br />
        > De opdrachtnemer zal gebruik maken van vervoersmiddelen die hem in eigendom toebehoren dan wel<br />
        door hem geleased of gehuurd zijn .</p>
        
        <p>Voor wat betreft horizontale of verticale transportmiddelen op de bouwplaats kan de opdrachtgever deze<br />
        aan de opdrachtnemer ter beschikking stellen.<br />
        > De opdrachtnemer is gerechtigd anderen in te schakelen bij de uitvoering van de overeengekomen<br />
        werkzaamheden dan wel de werkzaamheden door anderen te laten uitvoeren ter vervanging van zijn<br />
        eigen arbeid.<br />
        > De opdrachtnemer is aansprakelijk voor nakoming van het overeengekomen resultaat.<br />
        > De opdracht zal worden uitgevoerd, met inachtneming van wettelijke voorschriften, die betrekking<br />
        hebben op arbeidsomstandigheden en veiligheid.<br />
        > Indien de opdracht wordt uitgevoerd op een bouwplaats waar ook werknemers werkzaam zijn, is de<br />
        opdrachtgever verantwoordelijk voor de naleving van de sectorale arbocatalogus. De opdrachtgever dient<br />
        hiervoor de noodzakelijke voorzieningen te treffen.<br />
        > De opdrachtnemer zal bij de uitvoering van zijn werkzaamheden gebruik maken van eigen<br />
        gereedschap en materieel.<br />
        > De opdrachtnemer zal bij de uitvoering van zijn werkzaamheden gebruik maken van eigen materialen.<br />
        Slechts in het geval de opdrachtgever een belangrijk inkoopvoordeel kan behalen bij de<br />
        aanschaf van materiaal kan het materiaal door de opdrachtgever ter beschikking worden gesteld aan<br />
        opdrachtnemer, of door uitdrukkelijke voorgeschrevenkwaliteits-eis van opdrachtgever of fysieke<br />
        onmogelijkheid.<br />
        > De opdrachtnemer verklaart meerdere opdrachtgevers per kalenderjaar te bedienen met zijn diensten.<br />
        Tevens verklaart hij dit met ondertekening van deze overeenkomst richting opdrachtgever en overige<br />
        betrokken partijen. Mocht hierdoor enige schade uit voortvloeien dan zijn de (financiele) gevolgen voor<br />
        opdrachtnemer.</p>
        
        <p>D. Aanvang van het werk; {Startdatum van het werk:23} <br />
        uitvoeringsduur; {Duur van het project:24} <br />
        Met betrekking tot het tijdstip van oplevering geldt het volgende: {Opleverdatum:25} <br />
        Het werk dient te zijn opgeleverd: voor {Opleverdatum:25} of zoveel eerder dat de hoofdaannemer dat<br />
        wenselijk acht.</p>
        
        <p>Na de oplevering geldt geen onderhoudstermijn.<br />
        Indien opdrachtnemer wegens ziekte niet in staat is om de werkzaamheden uit te voeren, zal<br />
        opdrachtnemer opdrachtgever onverwijld in kennis stellen. Opdrachtnemer zal zich zoveel mogelijk<br />
        inspannen om eventuele vertraging in de uitvoering van de werkzaamheden te voorkomen. Ziekte van de<br />
        opdrachtnemer wordt wel beschouwd als overmacht.</p>
        
        <p>E. De Tariefvorming<br />
        > De aannemingssom van het aan de opdrachtnemer opgedragen werk beloopt<br />
        {Prijs van het werk:29} <br />
        Overige werkzaamheden en bovenstaande werkzaamheden worden afgerekend tegen meer en<br />
        minderwerk.<br />
        Prijzen zijn exclusief BTW.</p>
        
        <p>Opdrachtgever verklaart een zodanig tarief te betalen aan opdrachtnemer die naast een marktconforme<br />
        vergoeding voor de overeengekomen arbeid ook voldoende is om de kosten te kunnen dekken van<br />
        voorzieningen tegen ziekte, langdurige arbeidsongeschiktheid, pensioen en scholing.</p>
        
    </div>
    <pagebreak sheet-size="A4" />
    <div class="page page_three">
        <p>F. Wijziging van kosten en prijzen<br />
        Wijzigingen van kosten en prijzen worden niet verrekend.</p>
        
        <p>G. Inrichting werkterrein; aan opdrachtnemer ter beschikking te stellen zaken<br />
        Met het oog op de uitvoering van het werk worden door de opdrachtgever aan de opdrachtnemer<br />
        de volgende zaken ter beschikking gesteld:<br />
        > opslagterrein/afsluitbare opslagruimte(n):<br />
        > hulpmiddelen:<br />
        > aansluitpunten gas, water, elektriciteit.</p>
        
        <p>Dit alles onverminderd datgene dat conform deze overeenkomst door de opdrachtnemer zelf wordt<br />
        geleverd. Uiterlijk 5 dagen voor de aanvang van het werk plegen partijen overleg over de locatie en de<br />
        capaciteit van de aan de opdrachtnemer ter beschikking te stellen zaken, alsmede over de periode waarin<br />
        die zaken gebruikt kunnen worden.<br />
        De container(s), bestemd voor de inzameling van het afval dat ontstaat bij de uitvoering van het<br />
        aan de opdrachtnemer opgedragen werk, wordt (worden) ter beschikking gesteld door opdrachtgever.<br />
        De kosten van het van het werkterrein verwijderen van het afval dat ontstaat bij de uitvoering van<br />
        het aan de opdrachtnemer opgedragen werk, zijn voor rekening van opdrachtgever.</p>
        
        <p>H. Hulpwerkzaamheden<br />
        De opdrachtgever verricht geen werkzaamheden ten behoeve van de uitvoering van het aan de<br />
        opdrachtnemer opgedragen werk. Dit met inachtneming van het overeengekomene onder I en J.</p>
        
        <p>I. Werktekeningen<br />
        De opdrachtnemer vervaardigt geen tekeningen met betrekking tot het aan hem opgedragen werk.</p>
        
        <p> J. Maatvoering<br />
        Door de opdrachtnemer wordt geen maatvoering c.q. stelwerkzaamheden verricht.</p>
        
        <p>K. Weekrapporten<br />
        De opdrachtnemer zorgt wel voor het opmaken van weekrapporten.</p>
        
        <p>L. Betaling<br />
        Het aan de opdrachtnemer toekomende zal door hem kunnen worden gefactureerd in<br />
        termijnen en telkens na het verschijnen van de desbetreffende termijn. De termijnen verschijnen na<br />
        goedkeuring van de termijnbon van opdrachtgever inzake het aantal gerealiseerde vierkante meters. De<br />
        opdrachtgever zal in alle gevallen rechtstreeks betalen aan de opdrachtnemer.</p>
        
        <p>M. Betalingstermijn<br />
        Met betrekking tot de termijn waarbinnen betaling dient plaats te vinden van door de opdrachtnemer<br />
        ingediende facturen geldt het volgende:<br />
        Betaling van een door de opdrachtnemer opgestelde factuur vindt in alle gevallen plaats uiterlijk binnen 4<br />
        weken nadat die factuur in goede orde bij de opdrachtgever is ingekomen.</p>
        
        <p>N. BTW - verleggingsregeling<br />
        Op deze overeenkomst is de verleggingsregeling met betrekking tot de B.T.W. wel van toepassing.</p>
        
        <p>O. Verzekeringen en certificaten<br />
        - Opdrachtnemer dient in het bezit te zijn van een bedrijfsaansprakelijkheidsverzekering (AVB)<br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- Opdrachtnemer beschikt over een geldig VCA VOL certificaat<br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- Opdrachtgever heeft een ongevallenverzekering afgesloten ten behoeve van opdrachtnemer.</p>
        
    </div>
    <pagebreak sheet-size="A4" />
    <div class="page page_four">
        <p>Opmerkingen:</p>
        
        <p>{Opmerkingen:32}</p>
        
        <p>Aldus overeengekomen en ondertekend te</p>
        
        <p>De opdrachtgever</p>
        
        <p>Namens {Bedrijfsnaam opdrachtgever / client:9}</p>
        
        <p>
            <br />
            <br />
            <br />
            <br />
            <br />
            ____________________________________________________</p>
        
        <p>{Contactpersoon opdrachtgever / client (Prefix):7.2} {Contactpersoon opdrachtgever / client (First):7.3} {Contactpersoon opdrachtgever / client (Middle):7.4} {Contactpersoon opdrachtgever / client (Last):7.6}</p>
        
        <p>De opdrachtnemer</p>
        
        <p>Namens {Uw Bedrijfsnaam:13}</p>
                
        <p>
            <br />
            <br />
            <br />
            <br />
            <br />
            ____________________________________________________</p>
        
        <p>{ (Prefix):1.2} { (First):1.3} { (Middle):1.4} { (Last):1.6}</p>
        
        <p>Deze overeenkomst is gelijkluidend aan de door de Belastingdienst op 13 - 10 - 2015 onder nummer <br />
        9051596301 beoordeelde overeenkomst.</p>
    </div>
  
            <?php
        }

        ?>
	</body>
</html>