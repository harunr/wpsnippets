<?php
/**
 * The template for displaying Author pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package klein 
 */
get_header(klein_header()); ?>
<div class="content-heading ">
    <div class="row">
        <div class="col-md-12" id="content-header">
            <div class="row">
		<div class="col-md-12">
		<?php
                the_archive_title( '<h1 class="entry-title '.$bread_mg_class.'">', '</h1>' );
                $curspeaker = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author));
                $author_id = $curspeaker->ID;
                $current_user = wp_get_current_user();
                $current_user_roles = $current_user->roles;
                $current_user_role = array_shift($current_user_roles);
                
                $rating = new hrrUserReviews();
                ?>
               <h2><?php echo $curspeaker->display_name;  ?></h2>
               <?php 
               if(is_user_logged_in() && $current_user->ID != $author_id && ($current_user_role == 'consumer' || $current_user_role == 'professional' ) && $rating->user_role($author_id) == 'professional' ){  
                    // User loggedin and nota same user? Have to add user roles later
                   
                   $is_rated = $rating->check_exist_review($author_id, $current_user->ID);
                   if($is_rated != 0){ // Is rated already
                       ?>
                       <?php $rating->show_detiled_review($author_id); 
                            echo $rating->extra_ratings($author_id);
                            echo $rating->rating_comment($author_id);
                       ?>
                       
                       <p>You Have already rated!</p>
                       <?php
                   } else { // not rated yet ? go on
                        $rating->show_detiled_review($author_id);
                        echo $rating->extra_ratings($author_id);
                        echo $rating->rating_comment($author_id);
                        ?>
                        <a href="#submit_review" class="review_form">Submit Your Review</a>
                        <?php
                         require 'reviews/form.php';
                   }
               } else {
                    $rating->show_detiled_review($author_id);
                    echo $rating->extra_ratings($author_id);
                    echo $rating->rating_comment($author_id);
               }
                ?>
               </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(klein_header()); ?>