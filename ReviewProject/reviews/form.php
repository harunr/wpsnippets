<div id="submit_review" class="white-popup-block mfp-hide">
    <div class="mfp-content">
        <button title="Close (Esc)" type="button" class="mfp-close">X</button>
        <div class="msg"></div>

        <form id="submit_review_form" method="POST">
            <h3> Review Form </h3>

            <div class="single_ratings welcomed">
                <span class="single_rating_title">Made you feel welcomed*:</span>
                <span class="starRating">
                  <input id="welcomed5" type="radio" name="welcomed" value="5">
                  <label for="welcomed5">5</label>
                  <input id="welcomed4" type="radio" name="welcomed" value="4">
                  <label for="welcomed4">4</label>
                  <input id="welcomed3" type="radio" name="welcomed" value="3">
                  <label for="welcomed3">3</label>
                  <input id="welcomed2" type="radio" name="welcomed" value="2">
                  <label for="welcomed2">2</label>
                  <input id="welcomed1" type="radio" name="welcomed" value="1">
                  <label for="welcomed1">1</label>
                </span>
            </div>

            <div class="single_ratings timely_manner">
                <span class="single_rating_title">Service provided in a timely manner*:</span>
                <span class="starRating">
                  <input id="timely_manner5" type="radio" name="timely" value="5">
                  <label for="timely_manner5">5</label>
                  <input id="timely_manner4" type="radio" name="timely" value="4">
                  <label for="timely_manner4">4</label>
                  <input id="timely_manner3" type="radio" name="timely" value="3">
                  <label for="timely_manner3">3</label>
                  <input id="timely_manner2" type="radio" name="timely" value="2">
                  <label for="timely_manner2">2</label>
                  <input id="timely_manner1" type="radio" name="timely" value="1">
                  <label for="timely_manner1">1</label>
                </span>
            </div>

            <div class="single_ratings needs">
                <span class="single_rating_title">Took time to understand and address your needs*:</span>
                <span class="starRating">
                  <input id="needs5" type="radio" name="needs" value="5">
                  <label for="needs5">5</label>
                  <input id="needs4" type="radio" name="needs" value="4">
                  <label for="needs4">4</label>
                  <input id="needs3" type="radio" name="needs" value="3">
                  <label for="needs3">3</label>
                  <input id="needs2" type="radio" name="needs" value="2">
                  <label for="needs2">2</label>
                  <input id="needs1" type="radio" name="needs" value="1">
                  <label for="needs1">1</label>
                </span>
            </div>

            <div class="single_ratings confidence">
                <span class="single_rating_title">Expressed confidence*:</span>
                <span class="starRating">
                  <input id="confidence5" type="radio" name="confidence" value="5">
                  <label for="confidence5">5</label>
                  <input id="confidence4" type="radio" name="confidence" value="4">
                  <label for="confidence4">4</label>
                  <input id="confidence3" type="radio" name="confidence" value="3">
                  <label for="confidence3">3</label>
                  <input id="confidence2" type="radio" name="confidence" value="2">
                  <label for="confidence2">2</label>
                  <input id="confidence1" type="radio" name="confidence" value="1">
                  <label for="confidence1">1</label>
                </span>
            </div>

            <div class="single_ratings knowledgeable">
                <span class="single_rating_title">Was knowledgeable*:</span>
                <span class="starRating">
                  <input id="knowledgeable5" type="radio" name="knowledgeable" value="5">
                  <label for="knowledgeable5">5</label>
                  <input id="knowledgeable4" type="radio" name="knowledgeable" value="4">
                  <label for="knowledgeable4">4</label>
                  <input id="knowledgeable3" type="radio" name="knowledgeable" value="3">
                  <label for="knowledgeable3">3</label>
                  <input id="knowledgeable2" type="radio" name="knowledgeable" value="2">
                  <label for="knowledgeable2">2</label>
                  <input id="knowledgeable1" type="radio" name="knowledgeable" value="1">
                  <label for="knowledgeable1">1</label>
                </span>
            </div>

            <div class="single_ratings who_served">
                <p>Who served you? <br />
                    <input type="text" class="text" name="served" /></p>
            </div>

            <div class="single_ratings comments">
                <p>Additional Comments*: <br />
                    <textarea name="comment" id="cmnt" cols="30" rows="10"></textarea></p>
            </div>

            <input type="submit" value="Submit Review" name="review_form_btn" id="form_submit_btn"/>
            <input type="hidden" name="user_id" value="<?php echo $author_id; ?>" />
            <input type="hidden" name="reviewer_id" value="<?php echo $current_user->ID ?>" />
        </form>

    </div> 
</div> 