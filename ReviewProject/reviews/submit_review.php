<?php                                        
function post_word_count(){
    // Getting Data
    if(isset($_POST['welcomed']) && isset($_POST['timely']) && isset($_POST['needs']) && isset($_POST['confidence']) && isset($_POST['knowledgeable']) && isset($_POST['comment']) && isset($_POST['user_id']) && isset($_POST['reviewer_id'])){
       $welcomed = $_POST['welcomed'];
       $timely = $_POST['timely'];
       $needs = $_POST['needs'];
       $confidence = $_POST['confidence'];
       $knowledgeable = $_POST['knowledgeable'];
       $comment = $_POST['comment'];
       $user_id = $_POST['user_id'];
       $reviewer_id = $_POST['reviewer_id'];
       
       // Check Already or not
       $rating_class = new hrrUserReviews();
       $check_exist = $rating_class->check_exist_review($user_id, $reviewer_id);
       if($check_exist != 0){
           $errMsg ='You have rated already mate, aren\'t you?';
       } else if(isset($_POST['served']) && !empty($_POST['served'])) {  // Server Empty check and post a default data
           $served = $_POST['served'];
       }  else {
           $served = 0;
       }
        
       // Ratings and Comments empty check
        if(empty($welcomed) || empty($timely) || empty($needs) || empty($confidence) || empty($knowledgeable) || $welcomed == 0 || $timely == 0 || $needs == 0 || $confidence == 0 || $knowledgeable == 0 || $welcomed == 0) {
            $errMsg = 'Please select star(s) for all 5 type';
        } else if(empty($comment) || $comment == ''){
            $errMsg ='Please Submit Your Comment';
        } else {
     
            // Nothing empty, now time to insert data
            $avg_ratings = ($welcomed + $timely + $needs + $confidence + $knowledgeable) / 5 ;
            $avg_ratings = round($avg_ratings, 2);

            // Inserting Data
            $inserting_review = new hrrUserReviews();

            $inserting_review->insert_data($user_id, $avg_ratings, $reviewer_id, $welcomed, $timely, $needs, $confidence, $knowledgeable, $served, $comment);

            // Inser avarage rating into user profile
            $inserting_review->insert_avg_rating_user_profile($user_id, $avg_ratings);
            $successMsg = "Your Review Successfully Added";
       
       
        }


    } 



    // Showing Massage
    if(isset($successMsg)){
            echo '<p class="success_massage">'.$successMsg.'</p>';
    } else  if(isset($errMsg)){
        echo '<p class="error_massage">'.$errMsg.'</p>';
    }
// die();
// return true;
}

add_action('wp_ajax_post_word_count', 'post_word_count');
add_action('wp_ajax_nopriv_post_word_count', 'post_word_count');