jQuery(document).ready(function($) {
    // pop up
   jQuery('.review_form').magnificPopup({
     type: 'inline',
     preloader: false,
   });
   
   
   // Form Submit by ajax
   jQuery('#submit_review_form #form_submit_btn').click(function(){
        
       
        var welcomed = "";
        var welSelected = jQuery("input[type='radio'][name='welcomed']:checked");
        if (welSelected.length > 0) {
            welcomed = welSelected.val();
        }
       
        var timely = "";
        var timelySelected = jQuery("input[type='radio'][name='timely']:checked");
        if (timelySelected.length > 0) {
            timely = timelySelected.val();
        } 
       
        var needs = "";
        var needsSelected = jQuery("input[type='radio'][name='needs']:checked");
        if (needsSelected.length > 0) {
            needs = needsSelected.val();
        } 
        
        var confidence = "";
        var confidenceSelected = jQuery("input[type='radio'][name='confidence']:checked");
        if (confidenceSelected.length > 0) {
            confidence = confidenceSelected.val();
        } 
        
        
        var knowledgeable = "";
        var knowledgeableSelected = jQuery("input[type='radio'][name='knowledgeable']:checked");
        if (knowledgeableSelected.length > 0) {
            knowledgeable = knowledgeableSelected.val();
        } 
        
        
       var served = jQuery("input[type='text'][name='served']").val(); 
       var user_id = jQuery("input[type='hidden'][name='user_id']").val(); 
       var reviewer_id = jQuery("input[type='hidden'][name='reviewer_id']").val(); 
       var comment = jQuery("textarea#cmnt").val(); 
       
       jQuery.ajax({
           'type' : 'POST',
           'url' : ReviewAjax.ajaxurl, 
           'data' : {
               'action' : "post_word_count",
               'user_id' : user_id,
               'reviewer_id' : reviewer_id,
               'welcomed' : welcomed,
               'timely' : timely,
               'needs' : needs,
               'confidence' : confidence,
               'knowledgeable' : knowledgeable,
               'served' : served,
               'comment' : comment
           },
           'success' : function(data) {
               jQuery('#submit_review .msg').html(data);
               
           }
       });
              

       
       
       
       
       return false;
   });

 });