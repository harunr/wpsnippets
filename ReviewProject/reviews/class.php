<?php

// Class for User Reviews
class hrrUserReviews{
      // Initializing table
    private function table_init(){
        global $wpdb;
        $table_name = $wpdb->prefix.'hrr_user_ratings';
        return $table_name;
    } // End of table_init()
    
     public function user_role($user_id) {
        $user_info = get_userdata($user_id);
        $user_roles = $user_info->roles;
        $user_role = array_shift($user_roles);
        return $user_role;
    } // End of user_role()
    
     private function profile_avarage_review($user_id){
        if($this->user_role($user_id) == 'professional'){
            
            $user_review = get_user_meta($user_id, 'avarage_review', true); 
            if($user_review == '' || $user_review == 0 || $user_review == NULL || !isset($user_review)){
                    return false;
                } else{
                    return $user_review;
                }
        }
    } // End of profile_avarage_review()
    
    private function single_extra_ratings($user_id, $row_name) {
        global $wpdb;
        $total_extra_reviewer = $wpdb->get_var("SELECT COUNT(*) FROM {$this->table_init()} WHERE user_id = '{$user_id}'");
        $get_extra_rating = $wpdb->get_results( "SELECT {$row_name} FROM {$this->table_init()} WHERE user_id = '{$user_id}'", ARRAY_N );
        $total_ext_review = 0;
        foreach($get_extra_rating as $single_rate){
            $total_ext_review += $single_rate[0];
        }
        $avg_extra_rating = $total_ext_review / $total_extra_reviewer;
        
        $output_extra_result = '<p class="rating_stars">';
        
        if (0.1 <= $avg_extra_rating && $avg_extra_rating <= 1.4) { // One star
                $output_extra_result .=  '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i>';
            } elseif (1.5 <= $avg_extra_rating && $avg_extra_rating <= 1.9) { // One and half star
                $output_extra_result .=  '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-half-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i>';
            } elseif (2.0 <= $avg_extra_rating && $avg_extra_rating <= 2.4) { // Two star
                $output_extra_result .=  '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i>';
            } elseif (2.5 <= $avg_extra_rating && $avg_extra_rating <= 2.9) { // Two and half star
                $output_extra_result .=  '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-half-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i>';
            } elseif (3.0 <= $avg_extra_rating && $avg_extra_rating <= 3.4) { // Three star
                $output_extra_result .=  '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i>';
            } elseif (3.5 <= $avg_extra_rating && $avg_extra_rating <= 3.9) { // Three and half star
                $output_extra_result .=  '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-half-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i>';
            } elseif (4.0 <= $avg_extra_rating && $avg_extra_rating <= 4.4) { // Four star
                $output_extra_result .=  '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i>';
            } elseif (4.5 <= $avg_extra_rating && $avg_extra_rating <= 4.9) { // Four and half star
                $output_extra_result .=  '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star aria-hidden="true"></i><i class="fa fa-star-half-o" aria-hidden="true"></i>';
            } else { // Five star
                $output_extra_result .=  '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i>';
            }
        
        $output_extra_result .= '<span class="rating_nums">'.$avg_extra_rating.'</span></p>';
        return $output_extra_result;
        
    } // End of single_extra_ratings() 
    
    public function insert_data($user_id, $avg_ratings, $reviewer_id, $welcomed, $timely, $needs, $confidence, $knowledgeable, $served, $comment){
          global $wpdb;
         $table = $wpdb->prefix."hrr_user_ratings";
        $wpdb->insert(
            $table,
            array(
                    'user_id' => $user_id,
                    'rating_avg' => $avg_ratings,
                    'reviewer_id' => $reviewer_id,
                    'welcomed' => $welcomed,
                    'timely_manner' => $timely,
                    'needs' => $needs,
                    'confidence' => $confidence,
                    'knowledgeable' => $knowledgeable,
                    'who_served' => $served,
                    'reviewer_comment' => $comment
            )
        );
        
    } // End of insert_data()
    
    public function check_exist_review($user_id, $reviewer_id){
        global $wpdb;
        $review_count = $wpdb->get_var("SELECT COUNT(*) FROM {$this->table_init()} WHERE user_id = '{$user_id}' AND reviewer_id = '{$reviewer_id}'");
        return $review_count;
    } // End of check_exist_review()
    
    public function insert_avg_rating_user_profile($user_id, $avg_rating){
        // Checking user is professional or not 
        if($this->user_role($user_id) == 'professional'){
                $user_review = get_user_meta($user_id, 'avarage_review', true); 
        
                if($user_review == '' || $user_review == 0 || $user_review == NULL || !isset($user_review)){
                    add_user_meta( $user_id, 'avarage_review', $avg_rating );
                } elseif ($user_review != $avg_rating) {
                    global $wpdb;
                    $total_avg_ratings = $wpdb->get_results( "SELECT sum(rating_avg) FROM {$this->table_init()} WHERE user_id = '{$user_id}'", ARRAY_N );
                    $total_reviewer = $wpdb->get_var("SELECT COUNT(*) FROM {$this->table_init()} WHERE user_id = '{$user_id}'");
                    
                    $rating_in_avg = $total_avg_ratings / $total_reviewer;
                    update_user_meta( $user_id, 'avarage_review', $avg_rating);
                }
                
                return true;
        } else {
            return 'Something wrong out there!!! You supposed to don\'t have access in this area';
        }
    
        
    } // End of insert_avg_rating_user_profile()
    
    public function show_avg_review($user_id){
        $avg_review_num = $this->profile_avarage_review($user_id);
        
        echo '<div class="show_avg_rating">';
        if($avg_review_num === false){
            echo 'Not Rated yet';
        } else {
            if (0.1 <= $avg_review_num && $avg_review_num <= 1.4) { // One star
                echo '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i>';
            } elseif (1.5 <= $avg_review_num && $avg_review_num <= 1.9) { // One and half star
                echo '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-half-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i>';
            } elseif (2.0 <= $avg_review_num && $avg_review_num <= 2.4) { // Two star
                echo '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i>';
            } elseif (2.5 <= $avg_review_num && $avg_review_num <= 2.9) { // Two and half star
                echo '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-half-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i>';
            } elseif (3.0 <= $avg_review_num && $avg_review_num <= 3.4) { // Three star
                echo '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i>';
            } elseif (3.5 <= $avg_review_num && $avg_review_num <= 3.9) { // Three and half star
                echo '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-half-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i>';
            } elseif (4.0 <= $avg_review_num && $avg_review_num <= 4.4) { // Four star
                echo '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i>';
            } elseif (4.5 <= $avg_review_num && $avg_review_num <= 4.9) { // Four and half star
                echo '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star aria-hidden="true"></i><i class="fa fa-star-half-o" aria-hidden="true"></i>';
            } else { // Five star
                echo '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i>';
            }
            
        }
        echo '</div>';
    } // End of show_avg_review()
    
        public function show_detiled_review($user_id){
        $avg_review_num = $this->profile_avarage_review($user_id);
        
        if($avg_review_num === false){
            echo '<div class="show_rating"><h6>Not Rated yet</h6></div>';
        } else {
            echo '<div class="show_rating"><h5>Avarage Rating(s): </h5>';
            if (0.1 <= $avg_review_num && $avg_review_num <= 1.4) { // One star
                echo '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i>';
            } elseif (1.5 <= $avg_review_num && $avg_review_num <= 1.9) { // One and half star
                echo '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-half-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i>';
            } elseif (2.0 <= $avg_review_num && $avg_review_num <= 2.4) { // Two star
                echo '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i>';
            } elseif (2.5 <= $avg_review_num && $avg_review_num <= 2.9) { // Two and half star
                echo '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-half-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i>';
            } elseif (3.0 <= $avg_review_num && $avg_review_num <= 3.4) { // Three star
                echo '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i>';
            } elseif (3.5 <= $avg_review_num && $avg_review_num <= 3.9) { // Three and half star
                echo '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-half-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i>';
            } elseif (4.0 <= $avg_review_num && $avg_review_num <= 4.4) { // Four star
                echo '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i>';
            } elseif (4.5 <= $avg_review_num && $avg_review_num <= 4.9) { // Four and half star
                echo '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star aria-hidden="true"></i><i class="fa fa-star-half-o" aria-hidden="true"></i>';
            } else { // Five star
                echo '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i>';
            }
            echo '<h6>'.$avg_review_num.'</h6></div>';
        }
    } // End of show_detiled_review()
    
    
    public function extra_ratings($user_id){
        
        // Checking is user 'professional' or not
        if($this->user_role($user_id) == 'professional'){
            
            // Checking if user have atleast one review or not
            if($this->profile_avarage_review($user_id) != 0){
               
                 $output = '<div class="extra_reviews"><div class="first_three_cols">
                          <div class="single_extra_rating">
                              <p class="rating_title">Made you feel welcomed</p>
                              '.$this->single_extra_ratings($user_id, 'welcomed').'
                           </div>
                          <div class="single_extra_rating">
                              <p class="rating_title">Service provided in a timely manner</p>
                              '.$this->single_extra_ratings($user_id, 'timely_manner').'
                          </div>
                          <div class="single_extra_rating">
                              <p class="rating_title">Took time to understand and address your needs</p>
                              '.$this->single_extra_ratings($user_id, 'needs').'
                          </div>
                      </div>
                      <div class="last_two_cols">
                          <div class="single_extra_rating">
                              <p class="rating_title">Expressed confidence</p>
                              '.$this->single_extra_ratings($user_id, 'confidence').'
                          </div>
                          <div class="single_extra_rating">
                              <p class="rating_title">Was knowledgeable</p>
                              '.$this->single_extra_ratings($user_id, 'knowledgeable').'
                          </div>
                      </div>
                  </div>';
                
            }
            return $output;
        }
        
    } // End of extra_ratings()
    
    
    public function rating_comment($user_id) {
        global $wpdb;
        
        
        $review_cmnt = '<div class="review_comment"><h3 class="recent_rating_comment">Recent Reviews</h3>';
        
         $get_reviw_data = $wpdb->get_results( "SELECT rating_avg, reviewer_comment, reviewer_id FROM {$this->table_init()} WHERE user_id = '{$user_id}' LIMIT 5", ARRAY_A );
         foreach($get_reviw_data as $single_review){
             $rating_avg = $single_review['rating_avg'];
             $reviewer_comment = $single_review['reviewer_comment'];
             $reviewer_id = $single_review['reviewer_id'];
             
             $review_cmnt .= '<div class="single_review_comment">';
             $user_info = get_userdata($reviewer_id);
             $review_cmnt .= '<h4 class="reviewr_name">'.$user_info->first_name.' '.$user_info->last_name.'</h4>';
             
             $review_cmnt .= '<p class="rating_stars">';
                if (0.1 <= $rating_avg && $rating_avg <= 1.4) { // One star
                   $review_cmnt .=  '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i>';
               } elseif (1.5 <= $rating_avg && $rating_avg <= 1.9) { // One and half star
                   $review_cmnt .=  '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-half-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i>';
               } elseif (2.0 <= $rating_avg && $rating_avg <= 2.4) { // Two star
                   $review_cmnt .=  '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i>';
               } elseif (2.5 <= $rating_avg && $rating_avg <= 2.9) { // Two and half star
                   $review_cmnt .=  '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-half-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i>';
               } elseif (3.0 <= $rating_avg && $rating_avg <= 3.4) { // Three star
                   $review_cmnt .=  '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i>';
               } elseif (3.5 <= $rating_avg && $rating_avg <= 3.9) { // Three and half star
                   $review_cmnt .=  '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-half-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i>';
               } elseif (4.0 <= $rating_avg && $rating_avg <= 4.4) { // Four star
                   $review_cmnt .=  '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i>';
               } elseif (4.5 <= $rating_avg && $rating_avg <= 4.9) { // Four and half star
                   $review_cmnt .=  '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star aria-hidden="true"></i><i class="fa fa-star-half-o" aria-hidden="true"></i>';
               } else { // Five star
                   $review_cmnt .=  '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i>';
               }

                $review_cmnt .= '<span class="rating_nums">'.$rating_avg.'</span></p>';
                $review_cmnt .= '</div>';
                
                $review_cmnt .= '<div class="review_comment"><p>'.stripslashes($reviewer_comment).'</p></div>';
                
         }
        
        
        $review_cmnt .= '</div>';
        
        return $review_cmnt;
        
    } // End of rating_comment() 
    
    
}