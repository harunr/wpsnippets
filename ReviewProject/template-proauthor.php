<?php
/**
 * Template Name: Professional Authors
 *
 * @package  Klein
 * @version  3.0
 * @since  3.0
 */

get_header(klein_header());

/**
 * Just allow the content 
 * to be place here..
 */
$result = count_users();
echo 'Total: '.$result['avail_roles']['professional'].' Professionals';


$pro_query = new WP_User_Query( array( 
        'role' => 'professional',
        'orderby' => 'registered', 
        'order' => 'ASC',
        'number' => 10
    ) );

// Get the results
$authors = $pro_query->get_results();

// Check for results
if (!empty($authors)) {
    echo '<div class="pro_profile"';
    // loop trough each author
    foreach ($authors as $author)
    {
        // get all the user's data
        $author_info = get_userdata($author->ID);
// var_dump($author);
        $rating_class = new hrrUserReviews();
        echo '<div class="pro_profile_single"><a href="/author/'.$author_info->user_login.'">'.get_avatar( $author->ID, 32 ).$author_info->first_name.' '.$author_info->last_name.'</a><br />';
        $rating_class->show_avg_review($author->ID);
         echo '</div>';
    }
    echo '</div>';
} else {
    echo 'No professional found';
}

get_footer();
?>