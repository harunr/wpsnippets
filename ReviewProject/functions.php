<?php
/**
 * Enter your custom functions here
 *
 * @package klein
 *
 */

// Code Added By Harun R Rayhan

function hrr_initial_scripts(){
    wp_enqueue_style('magnific-popup-css', get_stylesheet_directory_uri().'/reviews/magnific-popup.css');
    wp_enqueue_script('jquery-magnific-popup', get_stylesheet_directory_uri().'/reviews/jquery.magnific-popup.min.js', array('jquery'));
    wp_enqueue_style('custom-css', get_stylesheet_directory_uri().'/css/custom.css', array('rtmedia-main'));
    wp_enqueue_script('review-js', get_stylesheet_directory_uri().'/reviews/review.js', array('jquery'));
    // including ajax script in the plugin ReviewAjax.ajaxurl
    wp_localize_script( 'review-js', 'ReviewAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php')));
}
add_action('wp_enqueue_scripts', 'hrr_initial_scripts');


// Scripts

function hrr_initial_code(){
    // Adding Consumer user role.
    add_role('consumer', __( 'Consumer' ),array('read' => true,  'edit_posts'   => false,'delete_posts' => false, ));

    // Adding Professional user role.
    add_role('professional',__( 'Professional' ),array('read' => true, 'edit_posts'   => false, 'delete_posts' => false, ));


    global $wpdb;
    $table_name = $wpdb->prefix.'hrr_user_ratings';
    if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
         //table not in database. Create new table
         $charset_collate = $wpdb->get_charset_collate();

         $sql = "CREATE TABLE $table_name (
              id mediumint(9) NOT NULL AUTO_INCREMENT,
              user_id int(11) NOT NULL,
              rating_avg float(11) NOT NULL,
              reviewer_id int(11) NOT NULL,
              welcomed int(11) NOT NULL,
              timely_manner int(11) NOT NULL,
              needs int(11) NOT NULL,
              confidence int(11) NOT NULL,
              knowledgeable int(11) NOT NULL,
              who_served text NOT NULL,
              reviewer_comment text NOT NULL,
              UNIQUE KEY id (id)
         ) $charset_collate;";
         require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
         dbDelta( $sql );
    }
}

add_action("after_setup_theme", "hrr_initial_code");

// Adding Reviews Class
require_once 'reviews/class.php';


// Inserting Databse by Ajax
require_once 'reviews/submit_review.php';